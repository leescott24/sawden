﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Sawden.Application.Checks;
using Sawden.Application.Checks.Interfaces;
using Sawden.Application.Logic;
using Sawden.Application.Logic.Interfaces;
using Sawden.Application.Managers;
using Sawden.Application.Managers.Interfaces;
using Sawden.Application.Mapping;
using Sawden.Application.Mapping.Interfaces;
using Sawden.Application.Persistance;
using Sawden.Application.Persistance.Interfaces;
using Sawden.Application.Setup;
using Sawden.Application.Setup.Interfaces;
using Sawden.Persistance.FrameDesignerDBContext.Models;
using Sawden.Persistance.SawdenDBContext.Models;
using Sawden.Persistance.SawdenOptDBContext.Models;
using Sawden.Shared;
using Sawden.Shared.Interfaces;
using Swashbuckle.AspNetCore.Swagger;

namespace Sawden.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("SawdenRouting",
                    builder =>
                    {
                        builder.AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowAnyOrigin()
                            .AllowCredentials();
                    });
            });
            
            //DBContext configuration & DI
            services.AddDbContext<FrameDesignerContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("FrameDesignerDB"));                
            }, ServiceLifetime.Scoped);

            services.AddDbContext<SawdenContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("SawdenDB"));
            }, ServiceLifetime.Scoped);

            services.AddDbContext<SawdenOptContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("SawdenOptDB"));
            }, ServiceLifetime.Scoped);


            //config
            //services.Configure<int[]>(Configuration.GetSection("AllowedMasterProductTypes"));


            //Setting Up DI for classes

            //persist
            services.AddScoped<IOrderJsonPersistance, OrderJsonPersistance>();
            services.AddScoped<IProfilePersistance, ProfilePersistance>();
            services.AddScoped<IPcodePersistance, PCodePersistance>();

            //managers
            services.AddScoped<IInitialStartManager, InitialStartManager>();
            services.AddScoped<ISecondaryStartManager, SecondaryStartManager>();

            //logic/workers    
            services.AddScoped<IMulTraSplitAndCrucifixCalculation, MulTraSplitAndCrucifixCalculation>();
            services.AddScoped<IPeiceSplitAndOrientationCalculation, PeiceSplitAndOrientationCalculation>();            
            services.AddScoped<IProfileCalculation, ProfileCalculation>();
            services.AddScoped<ISashSlabSizeCalculation, SashSlabSizeCalculation>();
            services.AddScoped<IGlassSizeCalculation, GlassSizeCalculation>();
            services.AddScoped<IColourCalculation, ColourCalculation>();
            services.AddScoped<IPartPositionCalculation, PartPositionCalculation>();
            services.AddScoped<ILetterBoxCalculation, LetterBoxCalculation>();
            services.AddScoped<ITrickleVentCalculation, TrickleVentCalculation>();
            services.AddScoped<IPCodeSizeCalculation, PCodeSizeCalculation>();
            services.AddScoped<IMechanicalJointCalculation, MechanicalJointCalculation>();

            //checks
            services.AddScoped<IInitialChecks, InitialChecks>();
            services.AddScoped<ISecondaryChecks, SecondaryChecks>();

            //response unique for each request
            services.AddScoped<IResponseData, ResponseData>();

            //settings/setup
            services.AddScoped<ISawdenSettings, SawdenSettings>();
            services.AddScoped<ISawdenSettingsSetup, SawdenSettingsSetup>();

            //Setting up mapper for Class mapping            
            services.AddScoped<IMainMapper, MainMapper>();
            services.AddScoped<IOutputMapper, OutputMapper>();

            //automapper
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);            

            //register mvc && set out put for enum to be string not value
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                    //options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                });

            //register swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Sawden API", Version = "V1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors("SawdenRouting");

            app.UseHttpsRedirection();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Sawden API V1"); 
            });

            app.UseMvc();
        }
    }
}
