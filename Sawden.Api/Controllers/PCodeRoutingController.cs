﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Sawden.Application.Persistance.Interfaces;
using Sawden.Domain.Persistance;
using Sawden.Persistance.SawdenDBContext.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sawden.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("SawdenRouting")]
    public class PCodeRoutingController : ControllerBase
    {       

        private readonly IPcodePersistance _pcodePersistance;

        public PCodeRoutingController(IPcodePersistance pcodePersistance)
        {           
            _pcodePersistance = pcodePersistance;
        }

        [HttpGet("GetPcodeMaster")]
        public ActionResult<PCodeMaster> Pcodes()
        {
            try
            {
                var pcodes = _pcodePersistance.GetPcodes();

                return Ok(pcodes);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost("UpdatePcode")]
        public ActionResult<bool> updatePcode([FromBody] Pcodes pcode)
        {
            try
            {
                var result = _pcodePersistance.updatePcode(pcode);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost("AddPcode")]
        public ActionResult<int> addPcode([FromBody] Pcodes pcode)
        {
            try
            {
                var result = _pcodePersistance.addPcode(pcode);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost("DeletePcode")]
        public ActionResult<bool> deletePcode([FromBody] int id)
        {
            try
            {
                var result = _pcodePersistance.deletePcode(id);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost("AddPcodeType")]
        public ActionResult<int> addPcodeType([FromBody] PcodeTypes pcodeType)
        {
            try
            {
                var result = _pcodePersistance.addPcodeType(pcodeType);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost("UpdatePcodeGroup")]
        public ActionResult<bool> updatePcodeGroup([FromBody] PcodeGroupsWithPcodes pcodeGroup)
        {
            try
            {
                var result = _pcodePersistance.updatePcodeGroup(pcodeGroup);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost("AddPcodeGroup")]
        public ActionResult<int> addPcodeGroup([FromBody] PcodeGroupsWithPcodes pcodeGroup)
        {
            try
            {
                var result = _pcodePersistance.addPcodeGroup(pcodeGroup);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost("DeletePcodeGroup")]
        public ActionResult<bool> deletePcodeGroup([FromBody] int id)
        {
            try
            {
                var result = _pcodePersistance.deletePcodeGroup(id);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        
    }
}
