﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Sawden.Application.Persistance.Interfaces;
using Sawden.Domain.Persistance;

namespace Sawden.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("SawdenRouting")]
    public class ProfileRoutingController : ControllerBase
    {
        private readonly IProfilePersistance _profilePersistance;

        public ProfileRoutingController(IProfilePersistance profilePersistance)
        {
            _profilePersistance = profilePersistance;
        }

        [HttpGet("GetProfileMaster")]
        public ActionResult<ProfileMaster> Pcodes()
        {
            try
            {
                var pcodes = _profilePersistance.GetProfiles();

                return Ok(pcodes);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}