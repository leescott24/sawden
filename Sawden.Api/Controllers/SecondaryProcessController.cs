﻿using Microsoft.AspNetCore.Mvc;
using Sawden.Application.Managers.Interfaces;
using Sawden.Shared;
using Sawden.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sawden.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SecondaryProcessController : ControllerBase
    {
        private readonly IResponseData _response;

        private readonly ISecondaryStartManager _secondaryStart;

        public SecondaryProcessController(IResponseData response, ISecondaryStartManager secondaryStart)
        {
            _response = response;

            _secondaryStart = secondaryStart;
        }

        [HttpPost("Process")]
        public ActionResult<IResponseData> Process([FromBody] int orderNumber)
        {
            try
            {
                _secondaryStart.Start(orderNumber);

                if (_response.Continue)
                {
                    return Ok(_response);
                }
                else
                {
                    _response.Data = null;
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                BadResponseGenerator.Exception(_response, ex);

                return BadRequest(_response);
            }
        }
    }
}
