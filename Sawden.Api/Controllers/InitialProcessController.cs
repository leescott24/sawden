﻿using System;
using Microsoft.AspNetCore.Mvc;
using Sawden.Application.Managers.Interfaces;
using Sawden.Domain.Api.InitialProcess.Models;
using Sawden.Domain.Input.Models;
using Sawden.Shared;
using Sawden.Shared.Interfaces;

namespace Sawden.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InitialProcessController : ControllerBase
    {
        private readonly IResponseData _response;

        private readonly IInitialStartManager _initialStart;

        public InitialProcessController(IResponseData response, IInitialStartManager initialStart)
        {
            _response = response;
            _initialStart = initialStart;
        }

        [HttpPost("Process")]
        public ActionResult<IResponseData> Process([FromBody] ClsInputOrder order)
        {
            try
            {
                _initialStart.Start(order);

                if (_response.Continue)
                {
                    return Ok(_response);
                }
                else
                {
                    _response.Data = null;
                    return BadRequest(_response);
                }
            }
            catch (Exception ex)
            {
                BadResponseGenerator.Exception(_response, ex);

                return BadRequest(_response);
            }
        }
    }
}