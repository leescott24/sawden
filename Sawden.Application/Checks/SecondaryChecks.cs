﻿using Sawden.Application.Checks.Interfaces;
using Sawden.Application.Extensions.Main;
using Sawden.Domain.Main.Models.Main;
using Sawden.Shared;
using Sawden.Shared.Interfaces;
using System;
using System.Linq;

namespace Sawden.Application.Checks
{
    public class SecondaryChecks : ISecondaryChecks
    {
        private IResponseData _response;

        private ClsOrder _order;

        public SecondaryChecks(IResponseData response)
        {
            _response = response;
        }

        public void Check(ClsOrder order)
        {
            try
            { 
                _order = order;

                checkFrameProfile();

                checkFrameStormguardSashOffset();

                checkSashProfile();

                checkSashSashOffset();
            }
            catch (Exception ex)
            {
                BadResponseGenerator.Exception(_response, ex);
            }
        }

        private void checkFrameProfile()
        {
            _order.LstFrame.ForEach(frame =>
            {
                frame.LstOuterTop().ForEach(top =>
                {
                    if (!top.IsFrameOuter())
                    {
                        BadResponseGenerator.Add(_response, note: new ResponseNote()
                        {
                            NoteType = EnResponseNote.SawdenError,
                            NoteMessage = $"Frame: {frame.FrameNumber} has an Outer Top not of ProdCategory Outer Frames/1"
                        });
                    }
                });

                var lstBottom = frame.LstOuterBottom();

                lstBottom.ForEach(bottom =>
                {
                    if (!bottom.IsFrameOuter() && !bottom.IsStorm())
                    {
                        BadResponseGenerator.Add(_response, note: new ResponseNote()
                        {
                            NoteType = EnResponseNote.SawdenError,
                            NoteMessage = $"Frame: {frame.FrameNumber} has an Outer Bottom not of ProdCategory Outer Frames/1 OR Threshold/25"
                        });
                    }
                });

                if(lstBottom.Where(w => w.IsStorm()).Count() >= 1 && lstBottom.Count > 1)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.SawdenError,
                        NoteMessage = $"Frame: {frame.FrameNumber} has multiple Outer Bot's of 1 or more is Threshold. This is not possible"
                    });
                }

                frame.LstOuterLeft().ForEach(left =>
                {
                    if (!left.IsFrameOuter())
                    {
                        BadResponseGenerator.Add(_response, note: new ResponseNote()
                        {
                            NoteType = EnResponseNote.SawdenError,
                            NoteMessage = $"Frame: {frame.FrameNumber} has an Outer Left not of ProdCategory Outer Frames/1"
                        });
                    }
                });

                frame.LstOuterRight().ForEach(right =>
                {
                    if (!right.IsFrameOuter())
                    {
                        BadResponseGenerator.Add(_response, note: new ResponseNote()
                        {
                            NoteType = EnResponseNote.SawdenError,
                            NoteMessage = $"Frame: {frame.FrameNumber} has an Outer Right not of ProdCategory Outer Frames/1"
                        });
                    }
                });


                frame.LstInner().ForEach(inner =>
                {
                    if (!inner.IsMulTra())
                    {
                        BadResponseGenerator.Add(_response, note: new ResponseNote()
                        {
                            NoteType = EnResponseNote.SawdenError,
                            NoteMessage = $"Frame: {frame.FrameNumber} has a Mullion/Transom not of ProdCategory TransomsAndMullions/3"
                        });
                    }
                });
            });
        }

        private void checkFrameStormguardSashOffset()
        {
            _order.LstFrame.ForEach(frame =>
            {
                frame.LstProfile.ForEach(profile =>
                {
                    if (profile.IsStorm())
                    {
                        if (profile.SashOffSet <= 0)
                        {
                            BadResponseGenerator.Add(_response, note: new ResponseNote()
                            {
                                NoteType = EnResponseNote.SawdenError,
                                NoteMessage = $"Frame: {frame.FrameNumber} has a storm without SashOffset set from dbo.OuterFrames"
                            });
                        }
                    }
                    else
                    {
                        if (profile.SashOffSet != 0)
                        {
                            BadResponseGenerator.Add(_response, note: new ResponseNote()
                            {
                                NoteType = EnResponseNote.SawdenError,
                                NoteMessage = $"Frame: {frame.FrameNumber} has profile with SashOffset set without being needed!"
                            });
                        }
                    }
                });
            });
        }

        private void checkSashProfile()
        {
            _order.LstFrame.ForEach(frame =>
            {
                frame.LstNormalSash().ForEach(sash =>
                {
                    sash.SashOuter(frame).ForEach(outer =>
                    {
                        if (!outer.IsSashOuter())
                        {
                            BadResponseGenerator.Add(_response, note: new ResponseNote()
                            {
                                NoteType = EnResponseNote.SawdenError,
                                NoteMessage = $"Frame: {frame.FrameNumber}, Sash: {sash.SashNumber} has an Outer profile not of ProdCategory Sashes/2"
                            });
                        }
                    });

                    sash.LstInner().ForEach(inner =>
                    {
                        if (!inner.IsMulTra())
                        {
                            BadResponseGenerator.Add(_response, note: new ResponseNote()
                            {
                                NoteType = EnResponseNote.SawdenError,
                                NoteMessage = $"Frame: {frame.FrameNumber}, Sash: {sash.SashNumber} has a Mullion/Transom not of ProdCategory TransomsAndMullions/3"
                            });
                        }
                    });
                });
            });
        }

        private void checkSashSashOffset()
        {
            _order.LstFrame.ForEach(frame =>
            {
                frame.LstNormalSash().ForEach(sash =>
                {
                    sash.LstProfile.ForEach(profile =>
                    {
                        if (profile.SashOffSet != 0)
                        {
                            BadResponseGenerator.Add(_response, note: new ResponseNote()
                            {
                                NoteType = EnResponseNote.SawdenError,
                                NoteMessage = $"Frame: {frame.FrameNumber}, Sash: {sash.SashNumber} has profile with SashOffset set without being needed!"
                            });
                        }
                    });

                    sash.LstSashCeption.ForEach(sashception =>
                    {
                        sashception.LstProfile.ForEach(profile =>
                        {
                            if (profile.SashOffSet != 0)
                            {
                                BadResponseGenerator.Add(_response, note: new ResponseNote()
                                {
                                    NoteType = EnResponseNote.SawdenError,
                                    NoteMessage = $"Frame: {frame.FrameNumber}, Sashception: {sashception.SashNumber} has profile with SashOffset set without being needed!"
                                });
                            }
                        });
                    });
                });
            });
        }
    }
}
