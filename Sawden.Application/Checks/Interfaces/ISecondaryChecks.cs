﻿using Sawden.Domain.Main.Models.Main;

namespace Sawden.Application.Checks.Interfaces
{
    public interface ISecondaryChecks
    {
        void Check(ClsOrder order);
    }
}
