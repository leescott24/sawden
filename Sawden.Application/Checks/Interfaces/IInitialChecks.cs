﻿using Sawden.Domain.Input.Models;

namespace Sawden.Application.Checks.Interfaces
{
    public interface IInitialChecks
    {
        void Check(ClsInputOrder inputOrder);
    }
}
