﻿using Sawden.Application.Checks.Interfaces;
using Sawden.Application.Extensions.Input;
using Sawden.Domain.Input.Models;
using Sawden.Domain.Shared;
using Sawden.Shared;
using Sawden.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sawden.Application.Checks
{
    public class InitialChecks : IInitialChecks
    {
        private IResponseData _response;

        private readonly ISawdenSettings _settings;

        private ClsInputOrder _inputOrder;

        private List<string> invalidPcodes;

        public InitialChecks(IResponseData response, ISawdenSettings settings)
        {
            _response = response;

            _settings = settings;            
        }

        public void Check(ClsInputOrder inputOrder)
        {
            try
            {
                _inputOrder = inputOrder;

                //cant be ctor as _settings is invalid before setup
                invalidPcodes = _settings.InvalidPcodes.Where(w => w.IsError).Select(s => s.PCode).ToList();

                masterProductCheck();

                systemTypeCheck();

                frameCheckRowsCols();

                frameOuterCheck();

                frameTransomMullionTypeCheck();

                frameAllAperturesCoveredAndDontOverlap();

                sashOuterCheck();

                sashDontOverlap();

                sashCeptionOpening();

                sashMasterSlaveFrenchAndDummy();

                sashMasterSlaveStableAndDummy();

                sashMasterSlaveMiniFrenchAndDummy();

                //check mullions/transoms have proper ends (all together)
                //for both frame and sash!!
                //___________
                //|    |     |
                //|    |     |
                //|____|     |
                //|          |
                //|__________|
            }
            catch (Exception ex)
            {
                BadResponseGenerator.Exception(_response, ex);
            }
        }

        private void masterProductCheck()
        {
            _inputOrder.LstFrame.ForEach(frame =>
            {
                var mp = Convert.ToInt32(frame.MasterProductTypeId);

                if (!_settings.AllowedMasterProductTypes.Contains(mp))
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.SawdenError,
                        NoteMessage = $"Frame: {frame.FrameNumber} Sawden does not currently support MasterProductId: {mp.ToString()}"
                    });
                }
            });
        }

        private void systemTypeCheck()
        {
            _inputOrder.LstFrame.ForEach(frame =>
            {
                var st = Convert.ToInt32(frame.SystemTypeId);
                if (!_settings.AllowedSystemTypes.Contains(st))
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.SawdenError,
                        NoteMessage = $"Frame: {frame.FrameNumber} Sawden does not currently support SystemType: {st.ToString()}"
                    });
                }
            });
        }

        private void frameCheckRowsCols()
        {
            _inputOrder.LstFrame.ForEach(frame =>
            {
                int rowId = 0;

                frame.LstRow.ForEach(row =>
                {
                    if (row.Id != rowId++)
                    {
                        BadResponseGenerator.Add(_response, note: new ResponseNote()
                        {
                            NoteType = EnResponseNote.CamdenError,
                            NoteMessage = $"Frame {frame.FrameNumber}, LstRow are not numbered incremently from 0"
                        });
                    }
                });

                int colId = 0;

                frame.LstCol.ForEach(col =>
                {
                    if (col.Id != colId++)
                    {
                        BadResponseGenerator.Add(_response, note: new ResponseNote()
                        {
                            NoteType = EnResponseNote.CamdenError,
                            NoteMessage = $"Frame {frame.FrameNumber}, LstCol are not numbered incremently from 0"
                        });
                    }
                });

            });
        }

        private void frameOuterCheck()
        {
            _inputOrder.LstFrame.ForEach(frame =>
            {

                List<ClsInputProfile> lstTop = frame.LstOuterTop();
                List<ClsInputProfile> lstBottom = frame.LstOuterBottom();
                List<ClsInputProfile> lstLeft = frame.LstOuterLeft();
                List<ClsInputProfile> lstRight = frame.LstOuterRight();

                //check 0 -- count should only be 1
                if (lstTop.Count != 1)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, 'TOP' outer is not 1 peice!"
                    });
                }

                if (lstBottom.Count != 1)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, 'BOTTOM' outer is not 1 peice!"
                    });
                }

                if (lstLeft.Count != 1)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, 'LEFT' outer is not 1 peice!"
                    });
                }

                if (lstRight.Count != 1)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, 'RIGHT' outer is not 1 peice!"
                    });
                }


                //CHECK 2 outer profle matches frame lst rows/cols
                if (lstTop.Sum(s => s.ColSpan) != frame.MaxCol())
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, 'TOP' peice col spans of outer frame do not corelate with frame.lstCol"
                    });
                }

                if (lstBottom.Sum(s => s.ColSpan) != frame.MaxCol())
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, 'BOTTOM' peice col spans of outer frame do not corelate with frame.lstCol"
                    });
                }

                if (lstLeft.Sum(s => s.RowSpan) != frame.MaxRow())
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, 'LEFT' peice row spans of outer frame do not corelate with frame.lstRow"
                    });
                }

                if (lstRight.Sum(s => s.RowSpan) != frame.MaxRow())
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, 'RIGHT' peice row spans of outer frame do not corelate with frame.lstRow"
                    });
                }

                //CHECK 3 no overlapping tops/bottoms/rights/lefts
                int maxTopOverlapping = groupAndCountListInt(lstTop.SelectMany(s => s.LstCol()).ToList());
                int maxBotOverlapping = groupAndCountListInt(lstBottom.SelectMany(s => s.LstCol()).ToList());
                int maxLeftOverlapping = groupAndCountListInt(lstLeft.SelectMany(s => s.LstRow()).ToList());
                int maxRightOverlapping = groupAndCountListInt(lstRight.SelectMany(s => s.LstRow()).ToList());

                if (maxTopOverlapping > 2)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, 'TOP' peice row ID's & rowspan don't add up"
                    });
                }

                if (maxBotOverlapping > 2)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = ($"Frame {frame.FrameNumber}, 'BOTTOM' peice row ID's & rowspan don't add up")
                    });
                }

                if (maxLeftOverlapping > 2)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, 'LEFT' peice row ID's & rowspan don't add up"
                    });
                }

                if (maxRightOverlapping > 2)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, 'RIGHT' peice row ID's & rowspan don't add up"
                    });
                }

                //checks 4 PCODES
                if(lstTop.Where(w => invalidPcodes.Contains(w.Pcode)).Count() > 0)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, 'Top' peice have invalid PCodes"
                    });
                }

                if (lstBottom.Where(w => invalidPcodes.Contains(w.Pcode)).Count() > 0)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, 'Bottom' peice have invalid PCodes"
                    });
                }

                if (lstLeft.Where(w => invalidPcodes.Contains(w.Pcode)).Count() > 0)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, 'Left' peice have invalid PCodes"
                    });
                }

                if (lstRight.Where(w => invalidPcodes.Contains(w.Pcode)).Count() > 0)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, 'Right' peice have invalid PCodes"
                    });
                }

            });
        }

        private int groupAndCountListInt(List<int> lstInt)
        {
            return lstInt.GroupBy(i => i, (e, g) => new { Val = e, count = g.Count() })
                        .Select(s => s.count).Max();
        }

        private void frameTransomMullionTypeCheck()
        {
            _inputOrder.LstFrame.ForEach(frame =>
            {
                frame.LstInner().ForEach(inner =>
                {

                    if (inner.TransomType == EnTransomType.NonTransom)
                    {
                        BadResponseGenerator.Add(_response, note: new ResponseNote()
                        {
                            NoteType = EnResponseNote.CamdenError,
                            NoteMessage = $"Frame {frame.FrameNumber}, contains peices of EnTransomType non-transom in inner frame (not outter, top,bot,left,right)"
                        });
                    }
                });
            });
        }

        private void frameAllAperturesCoveredAndDontOverlap()
        {
            _inputOrder.LstFrame.ForEach(frame =>
            {
                var frameRows = frame.LstRow.Count;
                var frameCols = frame.LstCol.Count;

                //total frame array
                var frameArray = new int[frameCols, frameRows];

                //glass glazed/unglazed or panels
                if (frame.LstGlass != null)
                {
                    frame.LstGlass.ForEach(glass =>
                    {
                        frameAllAperturesCoveredAndDontOverlapProcess(frame, frameArray, glass, "(Glass/Unglazed/Panel)");
                    });
                }

                //compo doors
                frame.LstCompoSash().ForEach(compo =>
                {
                    frameAllAperturesCoveredAndDontOverlapProcess(frame, frameArray, compo, "(Compo)");
                });
                


                for (int x = 0; x < frameCols; x++)
                {
                    for (int y = 0; y < frameRows; y++)
                    {
                        if (frameArray[x, y] == 0)
                        {
                            BadResponseGenerator.Add(_response, note: new ResponseNote()
                            {
                                NoteType = EnResponseNote.CamdenError,
                                NoteMessage = $"Frame {frame.FrameNumber}, contains NO Glass/Panel/Compo for apperture of RowId: {y} ,RowSpan:1, ColId: {x}, ColSpan: 1"
                            });
                        }
                    }
                }
            });
        }

        private void frameAllAperturesCoveredAndDontOverlapProcess(ClsInputFrame frame, int[,] frameArray, ClsInputPosition inputAperture, string inputDesc)
        {
            var colId = Convert.ToInt32(inputAperture.ColId);
            var colSpan = Convert.ToInt32(inputAperture.ColSpan);
            var rowId = Convert.ToInt32(inputAperture.RowId);
            var rowSpan = Convert.ToInt32(inputAperture.RowSpan);

            apertureOrSashOverLapFrameOuter(frame, colId, colSpan, rowId, rowSpan, inputDesc); //checks overlapping Frame boundaries & throws error
            apertureOrSashOverLapInnerFrame(frame, colId, colSpan, rowId, rowSpan, inputDesc); //check overlapping Frame inners & throws error

            if (frameArray != null) //adds to frameArray if checking (glass/panel/compo) non sash
            {
                //if non sash >>> check it doesnt overlap sash inner either
                apertureOverlapSashInner(frame, colId, colSpan, rowId, rowSpan, inputDesc);

                frameAllAperturesframeArray(frame, frameArray, colId, colSpan, rowId, rowSpan); 
            }
        }

        private void apertureOrSashOverLapFrameOuter(ClsInputFrame frame, int colId, int colSpan, int rowId, int rowSpan, string inputDesc)
        {
            if (frame.LstCol.Count < (colId + colSpan))
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, contains {inputDesc} which exceeds frame boundaries (right side) & will cause null reference error"
                });
            }

            if (frame.LstRow.Count < (rowId + rowSpan))
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, contains {inputDesc} which exceeds frame boundaries (bottom side) & will cause null reference error"
                });
            }
        }

        private void apertureOrSashOverLapInnerFrame(ClsInputFrame frame, int colId, int colSpan, int rowId, int rowSpan, string inputDesc)
        {

            //check overlapping mullions
            if (colSpan > 1)
            {
                frame.LstInnerMullion().ForEach(mullion =>
                {
                    apertureOverlapMullion(frame, mullion, colId, colSpan, rowId, rowSpan, inputDesc, "frame");
                });
            }

            //check overlapping transom
            if (rowSpan > 1)
            {
                frame.LstInnerTransom().ForEach(transom =>
                {
                    apertureOverlapTransom(frame, transom, colId, colSpan, rowId, rowSpan, inputDesc, "frame");
                });
            }

        }

        private void apertureOverlapMullion(ClsInputFrame frame, ClsInputProfile mullion, int colId, int colSpan, int rowId, int rowSpan, string inputDesc, string sashOrFrame)
        {
            if (mullion.RowId <= rowId && //starts vertically in line or to the above #FILTER#
                        ((mullion.RowId + mullion.RowSpan) > rowId) && //and runs through Aperture vertically #FILTER#
                        mullion.ColId > colId && //starts on right of Aperture #FILTER#
                        (mullion.ColId < (colId + colSpan)) // Aperture crosses this mullion #FAIL#
                        )
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, contains {inputDesc} which overlaps {sashOrFrame} Mullion"
                });
            }
        }

        private void apertureOverlapTransom(ClsInputFrame frame, ClsInputProfile transom, int colId, int colSpan, int rowId, int rowSpan, string inputDesc, string sashOrFrame)
        {
            if (transom.ColId <= colId && //starts horizontallly in line or to the left #FILTER#
                        ((transom.ColId + transom.ColSpan) > colId) && //and runs through Aperture horizontally #FILTER#
                        transom.RowId > rowId && //starts below the Aperture #FILTER#
                        (transom.RowId < (rowId + rowSpan)) // Aperture crosses this transom #FAIL#
                        )
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, contains {inputDesc} which overlaps {sashOrFrame} Transom"
                });
            }
        }

        private void apertureOverlapSashInner(ClsInputFrame frame, int colId, int colSpan, int rowId, int rowSpan, string inputDesc)
        {
            frame.LstNormalSash().ForEach(sash =>
            {
                if (sash.ColId <= colId &&
                    sash.MaxCol() >= (colId + colSpan) &&
                    sash.RowId <= rowId &&
                    sash.MaxRow() >= (rowId + rowSpan))
                {
                    //Aperture is within the sash

                    //check overlapping sash mullion
                    sash.LstInnerMullion().ForEach(mullion =>
                    {
                        apertureOverlapMullion(frame, mullion, colId, colSpan, rowId, rowSpan, inputDesc, "sash");
                    });

                    //check overlapping sash transom
                    sash.LstInnerTransom().ForEach(transom =>
                    {
                        apertureOverlapTransom(frame, transom, colId, colSpan, rowId, rowSpan, inputDesc, "sash");
                    });
                }
            });
        }

        private void frameAllAperturesframeArray(ClsInputFrame frame, int[,] frameArray, int colId, int colSpan, int rowId, int rowSpan)
        {

            for (int x = colId; x < (colId + colSpan); x++)
            {
                for (int y = rowId; y < (rowId + rowSpan); y++)
                {
                    if (frameArray[x, y] != 0)
                    {
                        BadResponseGenerator.Add(_response, note: new ResponseNote()
                        {
                            NoteType = EnResponseNote.CamdenError,
                            NoteMessage = $"Frame {frame.FrameNumber}, contains OVERLAPPING (MULTIPLE) Glass/Panel/Compo for apperture of RowId: {y} ,RowSpan:1, ColId: {x}, ColSpan: 1"
                        });
                    }
                    else
                    {
                        frameArray[x, y] = 1;
                    }
                }
            }
        }

        private void sashOuterCheck()
        {
            _inputOrder.LstFrame.ForEach(frame =>
            {
                frame.LstNormalSash().ForEach(sash =>
                {
                    
                });
            });
        }

        private void sashOuterCheckNormal(ClsInputFrame frame, ClsInputSash sash)
        {
            var top = sash.SashTop(frame);
            var bottom = sash.SashBottom(frame);
            var left = sash.SashLeft(frame);
            var right = sash.SashRight(frame);

            //checks fpr only 1 top and colspans equal
            if (top.ColSpan != sash.ColSpan)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber} Sash Colspan != Sash Top peice Colspan"
                });
            }

            //checks for only 1 bottom and colspans equal
            if (bottom.ColSpan != sash.ColSpan)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber} Sash Colspan != Sash Bottom peice Colspan"
                });
            }

            //checks for only 1 left and rowspans equal
            if (left.RowSpan != sash.RowSpan)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber} Sash Rowspan != Sash Left peice Colspan"
                });
            }

            //checks for only 1 right anf rowspans equal
            if (right.RowSpan != sash.RowSpan)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber} Sash Rowspan != Sash Right peice Colspan"
                });
            }


            //pcode checks
            if (invalidPcodes.Contains(top.Pcode))
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber} 'Top' peice has an invlaid PCode"
                });
            }

            if (invalidPcodes.Contains(bottom.Pcode))
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber} 'Bottom' peice has an invlaid PCode"
                });
            }

            if (invalidPcodes.Contains(left.Pcode))
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber} 'Left' peice has an invlaid PCode"
                });
            }

            if (invalidPcodes.Contains(right.Pcode))
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber} 'Right' peice has an invlaid PCode"
                });
            }
        }

        private void sashOuterCheckCeption(ClsInputFrame frame, ClsInputSashCeption sash)
        {
            var top = sash.SashTop(frame);
            var bottom = sash.SashBottom(frame);
            var left = sash.SashLeft(frame);
            var right = sash.SashRight(frame);

            //checks fpr only 1 top and colspans equal
            if (top.ColSpan != sash.ColSpan)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber} Sash Colspan != Sash Top peice Colspan"
                });
            }

            //checks for only 1 bottom and colspans equal
            if (bottom.ColSpan != sash.ColSpan)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber} Sash Colspan != Sash Bottom peice Colspan"
                });
            }

            //checks for only 1 left and rowspans equal
            if (left.RowSpan != sash.RowSpan)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber} Sash Rowspan != Sash Left peice Colspan"
                });
            }

            //checks for only 1 right anf rowspans equal
            if (right.RowSpan != sash.RowSpan)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber} Sash Rowspan != Sash Right peice Colspan"
                });
            }


            //pcode checks
            if (invalidPcodes.Contains(top.Pcode))
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber} 'Top' peice has an invlaid PCode"
                });
            }

            if (invalidPcodes.Contains(bottom.Pcode))
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber} 'Bottom' peice has an invlaid PCode"
                });
            }

            if (invalidPcodes.Contains(left.Pcode))
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber} 'Left' peice has an invlaid PCode"
                });
            }

            if (invalidPcodes.Contains(right.Pcode))
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.CamdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber} 'Right' peice has an invlaid PCode"
                });
            }
        }

        private void sashDontOverlap()
        {
            _inputOrder.LstFrame.ForEach(frame =>
            {
                frame.LstNormalSash().ForEach(sash =>
                { 
                    frameAllAperturesCoveredAndDontOverlapProcess(frame, null, sash, "Sash");
                });
            });
        }

        private void sashCeptionOpening()
        {
            _inputOrder.LstFrame.ForEach(frame =>
            {
                frame.LstNormalSash().ForEach(sash =>
                {
                    sash.GetSashCeption().ForEach(sashCeption =>
                    {
                        if (!Convert.ToBoolean(sashCeption.OpenIn))
                        {
                            BadResponseGenerator.Add(_response, note: new ResponseNote()
                            {
                                NoteType = EnResponseNote.CamdenError,
                                NoteMessage = $"Frame {frame.FrameNumber}, SashCeption {sashCeption.SashNumber} Must Be Open In!!"
                            });
                        }
                    });
                });
            });
        }

        private void sashMasterSlaveFrenchAndDummy()
        {
            int fdSlaveLeft = 6;
            int fdSlaveRight = 8;
            int fdMasterLeft = 9;
            int fdMasterRight = 10;

            _inputOrder.LstFrame.Where(w => w.isFrenchDoor()).ToList().ForEach(frame =>
            {
                frenchStyleChecks(frame, fdSlaveLeft, fdSlaveRight, fdMasterLeft, fdMasterRight);
            });
        }

        private void sashMasterSlaveMiniFrenchAndDummy()
        {
            int MFSlaveLeft = 31;
            int MFSlaveRight = 32;
            int MFMasterLeft = 29;
            int MFMasterRight = 30;

            _inputOrder.LstFrame.Where(w => w.isMiniFrench()).ToList().ForEach(frame =>
            {
                frenchStyleChecks(frame, MFSlaveLeft, MFSlaveRight, MFMasterLeft, MFMasterRight);
            });
        }

        private void frenchStyleChecks(ClsInputFrame frame, int slaveLeft, int slaveRight, int masterLeft, int masterRight)
        {
            List<ClsInputProfile> lstFrameDummies = frame.LstProfile.Where(w => w.TransomType == EnTransomType.Dummy).ToList();

            List<int> lstSashTypeIds = frame.LstNormalSash().Select(s => Convert.ToInt32(s.SashTypeId)).ToList();

            if (lstSashTypeIds.Contains(slaveLeft) || lstSashTypeIds.Contains(masterRight))
            {
                List<ClsInputSash> lstSlaves = frame.LstNormalSash().Where(w => w.SashTypeId == slaveLeft).ToList();
                List<ClsInputSash> lstMasters = frame.LstNormalSash().Where(w => w.SashTypeId == masterRight).ToList();

                frenchStyleSlaveAndMasterAndDummiesChecks(frame, lstFrameDummies, lstSlaves, lstMasters, true);

                int slaves = lstSlaves.Count;
                int masters = lstMasters.Count;

                if (slaves != masters)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, Does not Contain complete matching Pairs of Slave Lefts & Master Rights!!"
                    });
                }

                if (slaves != lstFrameDummies.Count)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, Does not Contain dummies mullions for each set of Master/Slave Pairs!!"
                    });
                }
            }

            if (lstSashTypeIds.Contains(masterLeft) || lstSashTypeIds.Contains(slaveRight))
            {
                List<ClsInputSash> lstSlaves = frame.LstNormalSash().Where(w => w.SashTypeId == slaveRight).ToList();
                List<ClsInputSash> lstMasters = frame.LstNormalSash().Where(w => w.SashTypeId == masterLeft).ToList();

                frenchStyleSlaveAndMasterAndDummiesChecks(frame, lstFrameDummies, lstSlaves, lstMasters, false);

                int slaves = lstSlaves.Count;
                int masters = lstMasters.Count;

                if (slaves != masters)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, Does not Contain complete matching Pairs of Master Lefts & Slave Rights!!"
                    });
                }

                if (slaves != lstFrameDummies.Count)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, Does not Contain dummies mullions for each set of Master/Slave Pairs!!"
                    });
                }
            }
        }

        private void frenchStyleSlaveAndMasterAndDummiesChecks(ClsInputFrame frame, List<ClsInputProfile> lstFrameDummies, List<ClsInputSash> slaves, List<ClsInputSash> masters, bool isSlaveLeft)
        {
            slaves.ForEach(slave =>
            {
                //find matching master
                ClsInputSash master = null;
                ClsInputProfile dummy = null;

                if (isSlaveLeft)
                {
                    master = masters.Where(w => w.RowId == slave.RowId && Convert.ToInt32(w.ColId) == slave.MaxCol()).FirstOrDefault();
                    dummy = lstFrameDummies.Where(w => w.RowId == slave.RowId && Convert.ToInt32(w.ColId) == slave.MaxCol() && w.IsVert()).FirstOrDefault();
                }
                else
                {
                    master = masters.Where(w => w.RowId == slave.RowId && w.MaxCol() == Convert.ToInt32(slave.ColId)).FirstOrDefault();
                    dummy = lstFrameDummies.Where(w => w.RowId == slave.RowId && w.MaxCol() == Convert.ToInt32(slave.ColId) && w.IsVert()).FirstOrDefault();
                }

                if (master == null)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, Slave and Master Do not sit beside eachother END COL != START COL or Do not share same Row Id!!"
                    });
                }
                else
                {
                    if (slave.RowSpan != master.RowSpan)
                    {
                        BadResponseGenerator.Add(_response, note: new ResponseNote()
                        {
                            NoteType = EnResponseNote.CamdenError,
                            NoteMessage = $"Frame {frame.FrameNumber}, Slave and Master do not share same row spans!!"
                        });
                    }

                }

                if (dummy == null)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, Slave and Master do not have a Dummy mullion between them!!"
                    });
                }
                else
                {
                    if (slave.RowSpan != dummy.RowSpan)
                    {
                        BadResponseGenerator.Add(_response, note: new ResponseNote()
                        {
                            NoteType = EnResponseNote.CamdenError,
                            NoteMessage = $"Frame {frame.FrameNumber}, Slave and Master and Dummy mullion do not share same row spans!!"
                        });
                    }
                }
            });
        }

        private void sashMasterSlaveStableAndDummy()
        {
            int SDSlaveLeft = 27;
            int SDSlaveRight = 28;
            int SDMasterLeft = 25;
            int SDMasterRight = 26;

            _inputOrder.LstFrame.Where(w => w.isStableDoor()).ToList().ForEach(frame =>
            {
                stableStyleChecks(frame, SDSlaveLeft, SDSlaveRight, SDMasterLeft, SDMasterRight);
            });
        }

        private void stableStyleChecks(ClsInputFrame frame, int slaveLeft, int slaveRight, int masterLeft, int masterRight)
        {
            List<ClsInputProfile> lstFrameDummies = frame.LstProfile.Where(w => w.TransomType == EnTransomType.Dummy).ToList();

            List<int> lstSashTypeIds = frame.LstNormalSash().Select(s => Convert.ToInt32(s.SashTypeId)).ToList();

            if (lstSashTypeIds.Contains(slaveLeft) || lstSashTypeIds.Contains(masterLeft))
            {
                List<ClsInputSash> lstSlaves = frame.LstNormalSash().Where(w => w.SashTypeId == slaveLeft).ToList();
                List<ClsInputSash> lstMasters = frame.LstNormalSash().Where(w => w.SashTypeId == masterLeft).ToList();

                stableStyleSlaveAndMasterAndDummiesChecks(frame, lstFrameDummies, lstSlaves, lstMasters);

                int slaves = lstSlaves.Count;
                int masters = lstMasters.Count;

                if (slaves != masters)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, Does not Contain complete matching Pairs of Slave & Master!!"
                    });
                }

                if (slaves != lstFrameDummies.Count)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, Does not Contain dummies mullions for each set of Master/Slave Pairs!!"
                    });
                }
            }

            if (lstSashTypeIds.Contains(slaveRight) || lstSashTypeIds.Contains(masterRight))
            {
                List<ClsInputSash> lstSlaves = frame.LstNormalSash().Where(w => w.SashTypeId == slaveRight).ToList();
                List<ClsInputSash> lstMasters = frame.LstNormalSash().Where(w => w.SashTypeId == masterRight).ToList();

                stableStyleSlaveAndMasterAndDummiesChecks(frame, lstFrameDummies, lstSlaves, lstMasters);

                int slaves = lstSlaves.Count;
                int masters = lstMasters.Count;

                if (slaves != masters)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, Does not Contain complete matching Pairs of Slave & Master!!"
                    });
                }

                if (slaves != lstFrameDummies.Count)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, Does not Contain dummies mullions for each set of Master/Slave Pairs!!"
                    });
                }
            }
        }

        private void stableStyleSlaveAndMasterAndDummiesChecks(ClsInputFrame frame, List<ClsInputProfile> lstFrameDummies, List<ClsInputSash> slaves, List<ClsInputSash> masters)
        {
            //MASTER ALWAYS ON TOP!

            slaves.ForEach(slave =>
            {
                ClsInputSash master = masters.Where(w => w.ColId == slave.ColId && w.MaxRow() == Convert.ToInt32(slave.RowId)).FirstOrDefault();
                ClsInputProfile dummy = lstFrameDummies.Where(w => w.ColId == slave.ColId && w.RowId == slave.RowId && !w.IsVert()).FirstOrDefault();


                if (master == null)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, Slave and Master Do not sit beside eachother END ROW != START ROW or Do not share same COL Id!!"
                    });
                }
                else
                {
                    if (slave.ColSpan != master.ColSpan)
                    {
                        BadResponseGenerator.Add(_response, note: new ResponseNote()
                        {
                            NoteType = EnResponseNote.CamdenError,
                            NoteMessage = $"Frame {frame.FrameNumber}, Slave and Master do not share same col spans!!"
                        });
                    }
                }

                if (dummy == null)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.CamdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, Slave and Master do not have a Dummy mullion between them!!"
                    });
                }
                else
                {
                    if (slave.ColSpan != dummy.ColSpan)
                    {
                        BadResponseGenerator.Add(_response, note: new ResponseNote()
                        {
                            NoteType = EnResponseNote.CamdenError,
                            NoteMessage = $"Frame {frame.FrameNumber}, Slave and Master and Dummy mullion do not share same col spans!!"
                        });
                    }
                }
            });
        }
    }
}
