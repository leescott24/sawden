﻿using Sawden.Application.Extensions.Input;
using Sawden.Application.Extensions.Main;
using Sawden.Application.Logic.Interfaces;
using Sawden.Domain.Input.Models;
using Sawden.Domain.Shared;
using Sawden.Shared;
using Sawden.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sawden.Application.Logic
{
    public class MulTraSplitAndCrucifixCalculation : IMulTraSplitAndCrucifixCalculation
    {
        private IResponseData _response;

        private ClsInputOrder _inputOrder;

        public MulTraSplitAndCrucifixCalculation(IResponseData response)
        {
            _response = response;
        }

        public void Process(ClsInputOrder inputOrder)
        {
            try
            {
                _inputOrder = inputOrder;

                processFrame();
            }
            catch (Exception ex)
            {
                BadResponseGenerator.Exception(_response, ex);
            }
        }

        private void processFrame()
        {
            _inputOrder.LstFrame.ForEach(frame =>
            {
                List<ClsInputProfile> mainProfile = getMainProfile(frame, frame.LstInnerTransom(), frame.LstInnerMullion());

                if (mainProfile != null)
                {
                    processInnerFrame(frame, mainProfile);
                }

                frame.LstNormalSash().ForEach(sash =>
                {
                    List<ClsInputProfile> mainSashProfile = getMainProfile(frame, sash.LstInnerTransom(), sash.LstInnerMullion());

                    if (mainSashProfile != null)
                    {
                        processInnerSash(sash, mainSashProfile);
                    }

                    sash.GetSashCeption().ForEach(sashception =>
                    {
                        List<ClsInputProfile> mainSashceptionProfile = getMainProfile(frame, sashception.LstInnerTransom(), sashception.LstInnerMullion());

                        if (mainSashceptionProfile != null)
                        {
                            processInnerSashception(sashception, mainSashceptionProfile);
                        }
                    });
                });
            });
        }

        private List<ClsInputProfile> getMainProfile(ClsInputFrame frame, List<ClsInputProfile> lstTransoms, List<ClsInputProfile> lstMullions)
        {
            var mainTransoms = lstTransoms
                .Where(w => w.ColSpan == frame.MaxCol() && w.TransomType != EnTransomType.Dummy)
                .ToList();

            var mainMullions = lstMullions
                .Where(w => w.RowSpan == frame.MaxRow() && w.TransomType != EnTransomType.Dummy)
                .ToList();

            if (mainTransoms.Count == 0 && mainMullions.Count == 0)
            {
                return null;
            }

            if (mainTransoms.Count > 0 && mainMullions.Count == 0)
            {
                return mainTransoms;
            }

            if (mainTransoms.Count == 0 && mainMullions.Count > 0)
            {
                return mainMullions;
            }

            var mainTran = mainTransoms
                .OrderByDescending(o => (int)o.TransomType)
                .FirstOrDefault();

            var mainMul = mainMullions
                .OrderByDescending(o => (int)o.TransomType)
                .FirstOrDefault();

            if (mainMul.RowSpan > mainTran.ColSpan)
            {
                return mainMullions;
            }
            else if (mainMul.RowSpan == mainTran.ColSpan)
            {
                if (mainMul.GetColSplitDist(frame) > mainTran.GetColSplitDist(frame))
                {
                    return mainMullions;
                }
                else if (mainMul.GetColSplitDist(frame) == mainTran.GetColSplitDist(frame))
                {
                    if ((int)mainMul.TransomType > (int)mainTran.TransomType)
                    {
                        return mainMullions;
                    }
                }
            }

            return mainTransoms;
        }

        private void processInnerFrame(ClsInputFrame frame, List<ClsInputProfile> mainProfile)
        {
            bool isTraMain = !mainProfile.FirstOrDefault().IsVert();

            List<ClsInputProfile> lstTransomsToRemove = new List<ClsInputProfile>();
            List<ClsInputProfile> lstNewTransoms = new List<ClsInputProfile>();

            foreach (var transom in frame.LstInnerTransom())
            {
                if (transom.ColSpan == 1)
                {
                    continue;
                }

                int previousStartingColId = Convert.ToInt32(transom.ColId);

                for (int x = Convert.ToInt32(transom.ColId) + 1; x < transom.MaxCol(); x++)
                {
                    int row = Convert.ToInt32(transom.RowId);

                    List<ClsInputProfile> lstBeside = frame.LstProfile.Where(w => w.IsVert() &&
                                                    w.ColId == x &&
                                                    w.LstRow().Contains(row))
                                                    .ToList();

                    //if no mullions exisit
                    if (lstBeside.Count == 0)
                    {
                        continue;
                    }
                    else if (lstBeside.Count == 1)
                    {
                        ClsInputProfile mullion = lstBeside.FirstOrDefault();

                        //means tran & mull cross! Something needs to split
                        if (mullion.LstRow().Contains(row - 1) && mullion.LstRow().Contains(row + 1))
                        {
                            if (transom.TransomType == mullion.TransomType)
                            {
                                if ((bool)frame.IsCrucifix) //split into 4 (both mullion and transom)
                                {
                                    splitMullion(frame, transom, mullion);

                                    splitTransom(lstTransomsToRemove, lstNewTransoms, transom,
                                        previousStartingColId, x);

                                    previousStartingColId = x;
                                }
                                else
                                {
                                    if (isTraMain) //split mullion
                                    {
                                        splitMullion(frame, transom, mullion);
                                    }
                                    else //split transom
                                    {
                                        splitTransom(lstTransomsToRemove, lstNewTransoms, transom,
                                            previousStartingColId, x);

                                        previousStartingColId = x;
                                    }
                                }
                            }
                            else if ((int)transom.TransomType > (int)mullion.TransomType) //split mullion
                            {
                                splitMullion(frame, transom, mullion);
                            }
                            else //split transom
                            {
                                splitTransom(lstTransomsToRemove, lstNewTransoms, transom,
                                    previousStartingColId, x);

                                previousStartingColId = x;
                            }
                        }
                    }
                }

                if (lstTransomsToRemove.Contains(transom))
                {
                    lstNewTransoms.Add(new ClsInputProfile()
                    {
                        ColId = previousStartingColId,
                        ColSpan = (transom.MaxCol() - previousStartingColId),
                        RowId = transom.RowId,
                        RowSpan = transom.RowSpan,
                        TransomType = transom.TransomType,
                        Pcode = transom.Pcode
                    });
                }
            }

            lstNewTransoms.ForEach(transom =>
            {
                frame.LstProfile.Add(transom);
            });

            lstTransomsToRemove.ForEach(transom =>
            {
                frame.LstProfile.Remove(transom);
            });
        }

        private void processInnerSash(ClsInputSash sash, List<ClsInputProfile> mainProfile)
        {
            bool isTraMain = !mainProfile.FirstOrDefault().IsVert();

            List<ClsInputProfile> lstTransomsToRemove = new List<ClsInputProfile>();
            List<ClsInputProfile> lstNewTransoms = new List<ClsInputProfile>();

            foreach (var transom in sash.LstInnerTransom())
            {
                if (transom.ColSpan == 1)
                {
                    continue;
                }

                int previousStartingColId = Convert.ToInt32(transom.ColId);

                for (int x = Convert.ToInt32(transom.ColId) + 1; x < transom.MaxCol(); x++)
                {
                    int row = Convert.ToInt32(transom.RowId);

                    List<ClsInputProfile> lstBeside = sash.LstProfile.Where(w => w.IsVert() &&
                                                    w.ColId == x &&
                                                    w.LstRow().Contains(row))
                                                    .ToList();

                    //if no mullions exisit
                    if (lstBeside.Count == 0)
                    {
                        continue;
                    }
                    else if (lstBeside.Count == 1)
                    {
                        ClsInputProfile mullion = lstBeside.FirstOrDefault();

                        //means tran & mull cross! Something needs to split
                        if (mullion.LstRow().Contains(row - 1) && mullion.LstRow().Contains(row + 1))
                        {
                            if (transom.TransomType == mullion.TransomType)
                            {
                                if ((bool)sash.IsCrucifix) //split into 4 (both mullion and transom)
                                {
                                    splitMullion(sash, transom, mullion);

                                    splitTransom(lstTransomsToRemove, lstNewTransoms, transom,
                                        previousStartingColId, x);

                                    previousStartingColId = x;
                                }
                                else
                                {
                                    if (isTraMain) //split mullion
                                    {
                                        splitMullion(sash, transom, mullion);
                                    }
                                    else //split transom
                                    {
                                        splitTransom(lstTransomsToRemove, lstNewTransoms, transom,
                                            previousStartingColId, x);

                                        previousStartingColId = x;
                                    }
                                }
                            }
                            else if ((int)transom.TransomType > (int)mullion.TransomType) //split mullion
                            {
                                splitMullion(sash, transom, mullion);
                            }
                            else //split transom
                            {
                                splitTransom(lstTransomsToRemove, lstNewTransoms, transom,
                                    previousStartingColId, x);

                                previousStartingColId = x;
                            }
                        }
                    }
                }

                if (lstTransomsToRemove.Contains(transom))
                {
                    lstNewTransoms.Add(new ClsInputProfile()
                    {
                        ColId = previousStartingColId,
                        ColSpan = (transom.MaxCol() - previousStartingColId),
                        RowId = transom.RowId,
                        RowSpan = transom.RowSpan,
                        TransomType = transom.TransomType,
                        Pcode = transom.Pcode
                    });
                }
            }

            lstNewTransoms.ForEach(transom =>
            {
                sash.LstProfile.Add(transom);
            });

            lstTransomsToRemove.ForEach(transom =>
            {
                sash.LstProfile.Remove(transom);
            });
        }

        private void processInnerSashception(ClsInputSashCeption sashception, List<ClsInputProfile> mainProfile)
        {
            bool isTraMain = !mainProfile.FirstOrDefault().IsVert();

            List<ClsInputProfile> lstTransomsToRemove = new List<ClsInputProfile>();
            List<ClsInputProfile> lstNewTransoms = new List<ClsInputProfile>();

            foreach (var transom in sashception.LstInnerTransom())
            {
                if (transom.ColSpan == 1)
                {
                    continue;
                }

                int previousStartingColId = Convert.ToInt32(transom.ColId);

                for (int x = Convert.ToInt32(transom.ColId) + 1; x < transom.MaxCol(); x++)
                {
                    int row = Convert.ToInt32(transom.RowId);

                    List<ClsInputProfile> lstBeside = sashception.LstProfile.Where(w => w.IsVert() &&
                                                    w.ColId == x &&
                                                    w.LstRow().Contains(row))
                                                    .ToList();

                    //if no mullions exisit
                    if (lstBeside.Count == 0)
                    {
                        continue;
                    }
                    else if (lstBeside.Count == 1)
                    {
                        ClsInputProfile mullion = lstBeside.FirstOrDefault();

                        //means tran & mull cross! Something needs to split
                        if (mullion.LstRow().Contains(row - 1) && mullion.LstRow().Contains(row + 1))
                        {
                            if (transom.TransomType == mullion.TransomType)
                            {
                                if ((bool)sashception.IsCrucifix) //split into 4 (both mullion and transom)
                                {
                                    splitMullion(sashception, transom, mullion);

                                    splitTransom(lstTransomsToRemove, lstNewTransoms, transom,
                                        previousStartingColId, x);

                                    previousStartingColId = x;
                                }
                                else
                                {
                                    if (isTraMain) //split mullion
                                    {
                                        splitMullion(sashception, transom, mullion);
                                    }
                                    else //split transom
                                    {
                                        splitTransom(lstTransomsToRemove, lstNewTransoms, transom,
                                            previousStartingColId, x);

                                        previousStartingColId = x;
                                    }
                                }
                            }
                            else if ((int)transom.TransomType > (int)mullion.TransomType) //split mullion
                            {
                                splitMullion(sashception, transom, mullion);
                            }
                            else //split transom
                            {
                                splitTransom(lstTransomsToRemove, lstNewTransoms, transom,
                                    previousStartingColId, x);

                                previousStartingColId = x;
                            }
                        }
                    }
                }

                if (lstTransomsToRemove.Contains(transom))
                {
                    lstNewTransoms.Add(new ClsInputProfile()
                    {
                        ColId = previousStartingColId,
                        ColSpan = (transom.MaxCol() - previousStartingColId),
                        RowId = transom.RowId,
                        RowSpan = transom.RowSpan,
                        TransomType = transom.TransomType,
                        Pcode = transom.Pcode
                    });
                }
            }

            lstNewTransoms.ForEach(transom =>
            {
                sashception.LstProfile.Add(transom);
            });

            lstTransomsToRemove.ForEach(transom =>
            {
                sashception.LstProfile.Remove(transom);
            });
        }

        private ClsInputProfile createProfile(int? colId, int? colSpan, int? rowId, int? rowSpan, EnTransomType? transomType, string pcode)
        {
            return new ClsInputProfile()
            {
                ColId = colId,
                ColSpan = colSpan,
                RowId = rowId,
                RowSpan = rowSpan,
                TransomType = transomType,
                Pcode = pcode
            };
        }

        private void splitMullion(ClsInputFrame frame, ClsInputProfile transom, ClsInputProfile mullion)
        {
            var overlap = mullion.MaxRow() - transom.RowId;

            mullion.RowSpan = transom.RowId - mullion.RowId;

            frame.LstProfile.Add(createProfile(mullion.ColId, mullion.ColSpan, transom.RowId, overlap, mullion.TransomType, mullion.Pcode));
        }

        private void splitMullion(ClsInputSash sash, ClsInputProfile transom, ClsInputProfile mullion)
        {
            var overlap = mullion.MaxRow() - transom.RowId;

            mullion.RowSpan = transom.RowId - mullion.RowId;

            sash.LstProfile.Add(createProfile(mullion.ColId, mullion.ColSpan, transom.RowId, overlap, mullion.TransomType, mullion.Pcode));
        }

        private void splitMullion(ClsInputSashCeption sashception, ClsInputProfile transom, ClsInputProfile mullion)
        {
            var overlap = mullion.MaxRow() - transom.RowId;

            mullion.RowSpan = transom.RowId - mullion.RowId;

            sashception.LstProfile.Add(createProfile(mullion.ColId, mullion.ColSpan, transom.RowId, overlap, mullion.TransomType, mullion.Pcode));
        }

        private void splitTransom(List<ClsInputProfile> lstTransomsToRemove, List<ClsInputProfile> lstNewTransoms, ClsInputProfile transom, int previousStartingColId, int x)
        {
            if (!lstTransomsToRemove.Contains(transom))
            {
                lstTransomsToRemove.Add(transom);
            }

            lstNewTransoms.Add(new ClsInputProfile()
            {
                ColId = previousStartingColId,
                ColSpan = (x - previousStartingColId),
                RowId = transom.RowId,
                RowSpan = transom.RowSpan,
                TransomType = transom.TransomType,
                Pcode = transom.Pcode
            });
        }
    }
}

