﻿using Sawden.Application.Extensions.Main;
using Sawden.Application.Logic.Interfaces;
using Sawden.Domain.Main.Models.Main;
using Sawden.Domain.Shared;
using Sawden.Shared;
using Sawden.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Application.Logic
{
    public class ColourCalculation : IColourCalculation
    {
        private IResponseData _response;

        private ClsOrder _order;

        public ColourCalculation(IResponseData response)
        {
            _response = response;
        }

        public void Process(ClsOrder order)
        {
            try
            {
                _order = order;

                setFrameProfile();

                setSashProfile();
            }
            catch (Exception ex)
            {
                BadResponseGenerator.Exception(_response, ex);
            }
        }

        private void setFrameProfile()
        {
            _order.LstFrame.ForEach(frame =>
            {
                int frameColourInside = Convert.ToInt32(frame.Colour.ColourIdInside);
                int frameColourOutside = Convert.ToInt32(frame.Colour.ColourIdOutside);

                frame.LstProfile.ForEach(profile =>
                {
                    profile.ColourIdLargeFace = profile.LargeFaceDirection == EnLargeFaceDirection.Inside ? frameColourInside : frameColourOutside;
                    profile.ColourIdSmallFace = profile.LargeFaceDirection == EnLargeFaceDirection.Inside ? frameColourOutside : frameColourInside;
                });
            });
        }

        private void setSashProfile()
        {
            _order.LstFrame.ForEach(frame =>
            {
                frame.LstNormalSash().ForEach(sash =>
                {
                    int sashColourInside = Convert.ToInt32(sash.Colour.ColourIdInside);
                    int sashColourOutside = Convert.ToInt32(sash.Colour.ColourIdOutside);

                    sash.LstProfile.ForEach(profile =>
                    {
                        profile.ColourIdLargeFace = profile.LargeFaceDirection == EnLargeFaceDirection.Inside ? sashColourInside : sashColourOutside;
                        profile.ColourIdSmallFace = profile.LargeFaceDirection == EnLargeFaceDirection.Inside ? sashColourOutside : sashColourInside;
                    });

                    sash.LstSashCeption.ForEach(sashCeption =>
                    {
                        int sashceptionColourInside = Convert.ToInt32(sashCeption.Colour.ColourIdInside);
                        int sashceptionColourOutside = Convert.ToInt32(sashCeption.Colour.ColourIdOutside);

                        sashCeption.LstProfile.ForEach(profile =>
                        {
                            profile.ColourIdLargeFace = profile.LargeFaceDirection == EnLargeFaceDirection.Inside ? sashceptionColourInside : sashceptionColourOutside;
                            profile.ColourIdSmallFace = profile.LargeFaceDirection == EnLargeFaceDirection.Inside ? sashceptionColourOutside : sashceptionColourInside;
                        });
                    });
                });
            });
        }
    }
}
