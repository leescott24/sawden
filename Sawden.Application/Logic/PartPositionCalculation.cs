﻿using Sawden.Application.Extensions.Main;
using Sawden.Application.Logic.Interfaces;
using Sawden.Domain.Main.Models.Abstract;
using Sawden.Domain.Main.Models.Main;
using Sawden.Domain.Main.Models.Main.Profile;
using Sawden.Persistance.SawdenDBContext.Models;
using Sawden.Shared;
using Sawden.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sawden.Application.Logic
{
    public class PartPositionCalculation : IPartPositionCalculation
    {
        private IResponseData _response;

        private readonly SawdenContext _sawden;

        private ClsOrder _inputorder;

        public PartPositionCalculation(IResponseData response, SawdenContext sawden)
        {
            _response = response;

            _sawden = sawden;
        }

        public void Process(ClsOrder inputOrder)
        {
            try
            {
                _inputorder = inputOrder;

                processFrame();
            }
            catch (Exception ex)
            {
                BadResponseGenerator.Exception(_response, ex);
            }
        }

        private void processFrame()
        {
            _inputorder.LstFrame.ForEach(frame =>
            {
                partPositionFrame(frame);

                frame.LstNormalSash().ForEach(sash =>
                {
                    partPositionSash(frame, sash.LstProfile, sash.RowId, sash.ColId, sash.SashTypeId);

                    sash.LstSashCeption.ForEach(sashception =>
                    {
                        partPositionSash(frame, sashception.LstProfile, sashception.RowId, sashception.ColId, sashception.SashTypeId);
                    });
                });
            });
        }

        private void partPositionFrame(ACFrame frame)
        {
            frame.LstProfile.ForEach(profile =>
            {
                profile.PartPosition = getLetter(profile.RowId, profile.IsVert());
                profile.PartPosition += getNumber(profile.ColId, profile.IsVert()).ToString();                

                switch (profile)
                {
                    case ClsProfileFrameOuter outer:

                        if (profile.IsVert())
                        {
                            if (profile.ColId == 0)
                            {
                                profile.PartPosition += " L/OUTER";
                            }
                            else
                            {
                                profile.PartPosition += " R/OUTER";
                            }
                        }
                        else
                        {
                            if (profile.RowId == 0)
                            {
                                profile.PartPosition += " T/OUTER";
                            }
                            else
                            {
                                profile.PartPosition += " B/OUTER";
                            }
                        }

                        break;

                    case ClsProfileFrameInner inner:

                        string mulTraTypeChar = profile.Pcode.ToUpper().Contains("MZ") ? "Z" : "T";

                        if (profile.IsVert())
                        {

                            profile.PartPosition += $" {mulTraTypeChar}/MULLION";
                        }
                        else
                        {
                            profile.PartPosition += $" {mulTraTypeChar}/TRANSOM";
                        }

                        break;                    
                }

            });
        }

        private void partPositionSash(ACFrame frame, List<ACProfile> lstProfile, int? sashRowId, int? sashColId, int? sashTypeId)
        {
            string sashPartPosition = getSashPartPosition(sashTypeId);

            lstProfile.ForEach(profile =>
            {
                switch (profile)
                {
                    case ClsProfileSashOuter outer:

                        profile.PartPosition = getLetter(sashRowId, true);
                        profile.PartPosition += getNumber(sashColId, false);

                        if (profile.IsVert())
                        {
                            if (profile.ColId == sashColId)
                            {
                                profile.PartPosition += " L:";
                            }
                            else
                            {
                                profile.PartPosition += " R:";
                            }
                        }
                        else
                        {
                            if (profile.RowId == sashRowId)
                            {
                                profile.PartPosition += " T:";
                            }
                            else
                            {
                                profile.PartPosition += " B:";
                            }
                        }

                        profile.PartPosition += sashPartPosition;

                        break;

                    case ClsProfileSashInner inner:

                        profile.PartPosition = getLetter(profile.RowId, profile.IsVert());
                        profile.PartPosition += getNumber(profile.ColId, profile.IsVert()).ToString();

                        if (profile.IsVert())
                        {

                            profile.PartPosition += " MULLION";
                        }
                        else
                        {
                            profile.PartPosition += " TRANSOM";
                        }

                        break;
                }

            });
        }

        private string getLetter(int? position, bool isVert)
        {
            string aplha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            if (!isVert)
            {
                position = position > 1 ? position - 1 : 0;
            }          

            return aplha[(int)position].ToString();
        }

        private int getNumber(int? position, bool isVert)
        {
            if (isVert)
            {
                return position > 1 ? (int)position : 1;
            }
            else
            {
                return (int)position + 1;
            }            
        }

        private string getSashPartPosition(int? sashTypeId)
        {
            string sashType = "UNDEFINED (ADD TO Sawden.dbo.SashTypePartPosition)";

            var sash = _sawden.SashTypePartPosition.FirstOrDefault(fd => fd.SashTypeId == (int)sashTypeId);

            if (sash != null)
            {
                sashType = sash.PartPosition;
            }

            return sashType;

        }
    }
}
