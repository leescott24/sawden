﻿using Sawden.Application.Extensions.Main;
using Sawden.Application.Logic.Interfaces;
using Sawden.Domain.Main.Models.Abstract;
using Sawden.Domain.Main.Models.Main;
using Sawden.Shared;
using Sawden.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sawden.Application.Logic
{
    public class GlassSizeCalculation : IGlassSizeCalculation
    {
        private IResponseData _response;

        private readonly ISawdenSettings _settings;

        private ClsOrder _inputOrder;

        public GlassSizeCalculation(IResponseData response, ISawdenSettings settings)
        {
            _response = response;

            _settings = settings;
        }

        public void Process(ClsOrder inputOrder)
        {
            try
            {
                _inputOrder = inputOrder;

                processFrame();

            }
            catch (Exception ex)
            {
                BadResponseGenerator.Exception(_response, ex);
            }
        }

        private void processFrame()
        {
            _inputOrder.LstFrame.ForEach(frame =>
            {
                //all glass within sashception
                sashceptionGlass(frame);

                //then all glass within sash
                sashGlass(frame);

                //then all remaining glass
                frameGlass(frame);
            });
        }

        private void sashceptionGlass(ACFrame frame)
        {
            frame.LstNormalSash().ForEach(sash =>
            {
                sash.LstSashCeption.ForEach(sashception =>
                {
                    //glass within this sashception
                    List<ClsGlass> lstGlass = frame.LstGlass.Where(w => sashception.LstCol().Contains((int)w.ColId)
                                                && sashception.MaxCol() != w.ColId
                                                && sashception.LstRow().Contains((int)w.RowId)
                                                && sashception.MaxRow() != w.RowId).ToList();

                    lstGlass.ForEach(glass =>
                    {
                        int row = (int)glass.RowId;
                        int col = (int)glass.ColId;

                        ACProfile sashceptionPeiceAbove = sashception.LstProfile.Where(w => !w.IsVert()
                                                && w.RowId == row
                                                && w.LstCol().Contains(col)).FirstOrDefault();

                        ACProfile sashceptionPeiceBelow = sashception.LstProfile.Where(w => !w.IsVert()
                                                && w.RowId == glass.MaxRow()
                                                && w.LstCol().Contains(col)).FirstOrDefault();

                        ACProfile sashceptionPeiceLeft = sashception.LstProfile.Where(w => w.IsVert()
                                                && w.LstRow().Contains(row)
                                                && w.ColId == col).FirstOrDefault();

                        ACProfile sashceptionPeiceRight = sashception.LstProfile.Where(w => w.IsVert()
                                                && w.LstRow().Contains(row)
                                                && w.ColId == glass.MaxCol()).FirstOrDefault();

                        processGlass(glass,
                            frame,
                            sashceptionPeiceAbove,
                            sashceptionPeiceBelow,
                            sashceptionPeiceLeft,
                            sashceptionPeiceRight,
                            sashception.OffsetTop,
                            sashception.OffsetBot,
                            sashception.OffsetLeft,
                            sashception.OffsetRight);

                    });
                });
            });
        }

        private double outerProfileDifference(ACProfile outerProfile)
        {
            double dif = outerProfile.ProfileHeight
                    - outerProfile.RebateInternal
                    + _settings.GlassOffset;

            return -dif;
        }

        private double MulTraProfileDifference(ACProfile mulTraProfile)
        {

            double dif = (mulTraProfile.ProfileHeight / 2)
                    - mulTraProfile.RebateInternal
                    + _settings.GlassOffset;

            return -dif;
        }

        private void processGlass(ClsGlass glass,
                            ACFrame frame,
                            ACProfile topPeice,
                            ACProfile botPeice,
                            ACProfile leftPeice,
                            ACProfile rightPeice,
                            double topOffset,
                            double botOffset,
                            double leftOffset,
                            double rightOffset
                            )
        {

            double glassWidth = glass.GetColSplitDist(frame);
            double glassHeight = glass.GetRowSplitDist(frame);                        

            if (topPeice.IsSashOuter() || topPeice.IsFrameOuter())
            {
                glassHeight += topOffset;
                glassHeight += outerProfileDifference(topPeice);
            }
            else
            {
                glassHeight += MulTraProfileDifference(topPeice);
            }

            if (botPeice.IsSashOuter() || botPeice.IsFrameOuter())
            {
                glassHeight += botOffset;
                glassHeight += outerProfileDifference(botPeice);
            }
            else
            {
                glassHeight += MulTraProfileDifference(botPeice);
            }

            if (leftPeice.IsSashOuter() || leftPeice.IsFrameOuter())
            {
                glassWidth += leftOffset;
                glassWidth += outerProfileDifference(leftPeice);
            }
            else
            {
                glassWidth += MulTraProfileDifference(leftPeice);
            }

            if (rightPeice.IsSashOuter() || rightPeice.IsFrameOuter())
            {
                glassWidth += rightOffset;
                glassWidth += outerProfileDifference(rightPeice);
            }
            else
            {
                glassWidth += MulTraProfileDifference(rightPeice);
            }

            glass.Width = glassWidth;
            glass.Height = glassHeight;
        }

        private void sashGlass(ACFrame frame)
        {
            frame.LstNormalSash().ForEach(sash =>
            {
                //glass within this sash that has not already been set by sashception
                List<ClsGlass> lstGlass = frame.LstGlass.Where(w => sash.LstCol().Contains((int)w.ColId)
                                            && sash.MaxCol() != w.ColId
                                            && sash.LstRow().Contains((int)w.RowId)
                                            && sash.MaxRow() != w.RowId
                                            && w.Height == 0
                                            && w.Width == 0).ToList();

                lstGlass.ForEach(glass =>
                {
                    int row = (int)glass.RowId;
                    int col = (int)glass.ColId;

                    ACProfile sashPeiceAbove = sash.LstProfile.Where(w => !w.IsVert()
                                            && w.RowId == row
                                            && w.LstCol().Contains(col)).FirstOrDefault();

                    ACProfile sashPeiceBelow = sash.LstProfile.Where(w => !w.IsVert()
                                            && w.RowId == glass.MaxRow()
                                            && w.LstCol().Contains(col)).FirstOrDefault();

                    ACProfile sashPeiceLeft = sash.LstProfile.Where(w => w.IsVert()
                                            && w.LstRow().Contains(row)
                                            && w.ColId == col).FirstOrDefault();

                    ACProfile sashPeiceRight = sash.LstProfile.Where(w => w.IsVert()
                                            && w.LstRow().Contains(row)
                                            && w.ColId == glass.MaxCol()).FirstOrDefault();

                    processGlass(glass,
                            frame,
                            sashPeiceAbove,
                            sashPeiceBelow,
                            sashPeiceLeft,
                            sashPeiceRight,
                            sash.OffsetTop,
                            sash.OffsetBot,
                            sash.OffsetLeft,
                            sash.OffsetRight);

                });

            });
        }

        private void frameGlass(ACFrame frame)
        {
            //glass within this frame that has not already been set by sashception or sash
            List<ClsGlass> lstGlass = frame.LstGlass.Where(w => w.Height == 0
                                        && w.Width == 0).ToList();

            lstGlass.ForEach(glass =>
            {
                int row = (int)glass.RowId;
                int col = (int)glass.ColId;

                ACProfile peiceAbove = frame.LstProfile.Where(w => !w.IsVert()
                                        && w.RowId == row
                                        && w.LstCol().Contains(col)).FirstOrDefault();

                ACProfile peiceBelow = frame.LstProfile.Where(w => !w.IsVert()
                                        && w.RowId == glass.MaxRow()
                                        && w.LstCol().Contains(col)).FirstOrDefault();

                ACProfile peiceLeft = frame.LstProfile.Where(w => w.IsVert()
                                        && w.LstRow().Contains(row)
                                        && w.ColId == col).FirstOrDefault();

                ACProfile peiceRight = frame.LstProfile.Where(w => w.IsVert()
                                        && w.LstRow().Contains(row)
                                        && w.ColId == glass.MaxCol()).FirstOrDefault();

                processGlass(glass,
                            frame,
                            peiceAbove,
                            peiceBelow,
                            peiceLeft,
                            peiceRight,
                            0.0,
                            0.0,
                            0.0,
                            0.0);
            });
        }
    }
}
