﻿using Sawden.Application.Extensions.Main;
using Sawden.Application.Logic.Interfaces;
using Sawden.Domain.Main.Models.Abstract;
using Sawden.Domain.Main.Models.Main;
using Sawden.Shared;
using Sawden.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sawden.Application.Logic
{
    public class SashSlabSizeCalculation : ISashSlabSizeCalculation
    {
        private IResponseData _response;

        private readonly ISawdenSettings _settings;

        private ClsOrder _inputOrder;

        public SashSlabSizeCalculation(IResponseData response, ISawdenSettings settings)
        {
            _response = response;

            _settings = settings;
        }

        public void Process(ClsOrder inputOrder)
        {
            try
            {
                _inputOrder = inputOrder;

                processFrame();
            }
            catch (Exception ex)
            {
                BadResponseGenerator.Exception(_response, ex);
            }
        }

        private void processFrame()
        {
            _inputOrder.LstFrame.ForEach(frame =>
            {
                frame.LstNormalSash().ForEach(sash =>
                {
                    sashSize(frame, sash);

                    sash.LstSashCeption.ForEach(sashception =>
                    {
                        sashceptionSize(frame, sashception);
                    });
                });

                frame.LstCompoSash().ForEach(slab =>
                {
                    slabSize(frame, slab);
                });
            });
        }

        private void sashSize(ACFrame frame, ACSash sash)
        {
            ACProfile topPeice = sash.SashTop(frame);
            ACProfile leftPeice = sash.SashLeft(frame);

            sash.Width = topPeice.Length;
            sash.Height = leftPeice.Length;
        }

        private void sashceptionSize(ACFrame frame, ACSashCeption sashception)
        {
            ACProfile topPeice = sashception.SashTop(frame);
            ACProfile leftPeive = sashception.SashLeft(frame);

            sashception.Width = topPeice.Length;
            sashception.Height = leftPeive.Length;
        }

        private void slabSize(ACFrame frame, ACSash slab)
        {
            int row = (int)slab.RowId;
            int col = (int)slab.ColId;

            ACProfile peiceAbove = frame.LstProfile.Where(w => !w.IsVert()
                                    && w.RowId == row
                                    && w.LstCol().Contains(col)).FirstOrDefault();

            ACProfile peiceBelow = frame.LstProfile.Where(w => !w.IsVert()
                                    && w.RowId == slab.MaxRow()
                                    && w.LstCol().Contains(col)).FirstOrDefault();

            ACProfile peiceLeft = frame.LstProfile.Where(w => w.IsVert()
                                    && w.LstRow().Contains(row)
                                    && w.ColId == col).FirstOrDefault();

            ACProfile peiceRight = frame.LstProfile.Where(w => w.IsVert()

                                    && w.LstRow().Contains(row)
                                    && w.ColId == slab.MaxCol()).FirstOrDefault();

            //continue same as glass size for frame
            throw new Exception();
        }

        private double outerProfileDifference(ACProfile outerProfile, bool is44mm)
        {
            double dif;

            if (is44mm)
            {
                dif = outerProfile.ProfileHeight
                    - outerProfile.RebateInternal
                    + _settings.Comp44mmOffset;
            }
            else
            {
                dif = outerProfile.ProfileHeight
                    - outerProfile.RebateInternal
                    - _settings.Comp70mmOverLap;
            }

            return -dif;
        }

        private double MulTraProfileDifference(ACProfile mulTraProfile, bool is44mm)
        {
            double dif;

            if (is44mm)
            {
                dif = (mulTraProfile.ProfileHeight / 2)
                    - mulTraProfile.RebateInternal
                    + _settings.Comp44mmOffset;
            }
            else
            {
                dif = (mulTraProfile.ProfileHeight / 2)
                    - mulTraProfile.RebateInternal
                    - _settings.Comp70mmOverLap;
            }

            return -dif;
        }
    }
}
