﻿using Sawden.Application.Extensions.Main;
using Sawden.Application.Logic.Interfaces;
using Sawden.Domain.Main.Models.Abstract;
using Sawden.Domain.Main.Models.Main;
using Sawden.Domain.Shared;
using Sawden.Shared;
using Sawden.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sawden.Application.Logic
{
    public class ProfileCalculation : IProfileCalculation
    {
        private IResponseData _response;

        private readonly ISawdenSettings _settings;

        private ClsOrder _order;

        public ProfileCalculation(IResponseData response, ISawdenSettings settings)
        {
            _response = response;

            _settings = settings;
        }

        public void Process(ClsOrder order)
        {
            try
            {
                _order = order;

                processFrame();
            }
            catch (Exception ex)
            {
                BadResponseGenerator.Exception(_response, ex);
            }
        }

        private void processFrame()
        {
            _order.LstFrame.ForEach(frame =>
            {
                frameEndPrepsAndLength(frame);

                sashEndPrepsAndLength(frame);

                sashCeptionEndPrepsAndLength(frame);
            });
        }

        private void frameEndPrepsAndLength(ACFrame frame)
        {
            frameOuterEndPrepsAndLengthTop(frame);

            frameOuterEndPrepsAndLengthBottom(frame);

            frameOuterEndPrepsAndLengthLeft(frame);

            frameOuterEndPrepsAndLengthRight(frame);

            frameInnerEndPrepsAndLenghtMullion(frame);

            frameInnerEndPrepsAndLengthTransom(frame);
        }

        private void frameOuterEndPrepsAndLengthTop(ACFrame frame)
        {
            List<ACProfile> lstTop = frame.LstOuterTop();

            bool first = true;
            int count = 1;
            int lstTopCount = lstTop.Count;

            lstTop.OrderBy(o => o.ColId).ToList().ForEach(top =>
            {
                double length = top.GetColSplitDist(frame);

                if (first)
                {
                    first = false;

                    ACProfile left = frameGetLeft(frame, top.RowId);

                    double leftDif = frameGetDifferenceOuter(top.ProfileHeight, left.ProfileHeight);

                    top.TLEndPrep = frameEndPrepOuter(leftDif);

                    length = lengthCalculation(length, leftDif);

                    top.OffsetTL = frameOuterOffset(leftDif);
                }

                if (lstTopCount == count)
                {
                    ACProfile right = frameGetRight(frame, top.RowId);

                    double rightDif = frameGetDifferenceOuter(top.ProfileHeight, right.ProfileHeight);

                    top.BREndPrep = frameEndPrepOuter(rightDif);

                    length = lengthCalculation(length, rightDif);

                    top.OffsetBR = frameOuterOffset(rightDif);
                }

                if (top.TLEndPrep == null)
                {
                    ACProfile mullion = frameGetMullion(frame, top.ColId, top.RowId);

                    double chamfer = frameGetDifferenceMulTraOuter(frame, top.ProfileHeight, mullion.ProfileHeight);

                    top.TLEndPrep = frameEndPrepOuter(chamfer);
                }

                if (top.BREndPrep == null)
                {
                    ACProfile mullion = frameGetMullion(frame, top.MaxCol(), top.RowId);

                    double chamfer = frameGetDifferenceMulTraOuter(frame, top.ProfileHeight, mullion.ProfileHeight);

                    top.BREndPrep = frameEndPrepOuter(chamfer);
                }

                top.Length = length;

                count++;
            });            
        }

        private void frameOuterEndPrepsAndLengthBottom(ACFrame frame)
        {
            List<ACProfile> lstBottom = frame.LstOuterBottom();

            //check is bottom is storm first & only continue if not!
            //checks by now if storm only 1 bottom
            if(lstBottom.FirstOrDefault().IsStorm())
            {
                ACProfile bottom = lstBottom.FirstOrDefault();

                double length = bottom.GetColSplitDist(frame);

                bottom.TLEndPrep = frameEndPrepOuterFlat();

                bottom.BREndPrep = frameEndPrepOuterFlat();

                bottom.Length = length;

                return;
            }

            bool first = true;
            int count = 1;
            int lstBotCount = lstBottom.Count;

            lstBottom.OrderBy(o => o.ColId).ToList().ForEach(bottom =>
            {
                double length = bottom.GetColSplitDist(frame);

                if (first)
                {
                    first = false;

                    ACProfile left = frameGetLeft(frame, bottom.RowId);

                    double leftDif = frameGetDifferenceOuter(bottom.ProfileHeight, left.ProfileHeight);

                    bottom.TLEndPrep = frameEndPrepOuter(leftDif);

                    length = lengthCalculation(length, leftDif);

                    bottom.OffsetTL = frameOuterOffset(leftDif);
                }

                if (lstBotCount == count)
                {
                    ACProfile right = frameGetRight(frame, bottom.RowId);

                    double rightDif = frameGetDifferenceOuter(bottom.ProfileHeight, right.ProfileHeight);

                    bottom.BREndPrep = frameEndPrepOuter(rightDif);

                    length = lengthCalculation(length, rightDif);

                    bottom.OffsetBR = frameOuterOffset(rightDif);
                }

                if (bottom.TLEndPrep == null)
                {
                    ACProfile mullion = frameGetMullion(frame, bottom.ColId, bottom.RowId);

                    double chamfer = frameGetDifferenceMulTraOuter(frame, bottom.ProfileHeight, mullion.ProfileHeight);

                    bottom.TLEndPrep = frameEndPrepOuter(chamfer);
                }

                if (bottom.BREndPrep == null)
                {
                    ACProfile mullion = frameGetMullion(frame, bottom.MaxCol(), bottom.RowId);

                    double chamfer = frameGetDifferenceMulTraOuter(frame, bottom.ProfileHeight, mullion.ProfileHeight);

                    bottom.BREndPrep = frameEndPrepOuter(chamfer);
                }

                bottom.Length = length;

                count++;
            });
        }

        private void frameOuterEndPrepsAndLengthLeft(ACFrame frame)
        {
            List<ACProfile> lstLeft = frame.LstOuterLeft();

            bool first = true;
            int count = 1;
            int lstLeftCount = lstLeft.Count;

            lstLeft.OrderBy(o => o.RowId).ToList().ForEach(left =>
            {
                double length = left.GetRowSplitDist(frame);

                if (first)
                {
                    first = false;

                    ACProfile top = frameGetTop(frame, left.ColId);

                    double topDif = frameGetDifferenceOuter(left.ProfileHeight, top.ProfileHeight);

                    left.TLEndPrep = frameEndPrepOuter(topDif);

                    length = lengthCalculation(length, topDif);

                    left.OffsetTL = frameOuterOffset(topDif);
                }

                if (lstLeftCount == count)
                {
                    ACProfile bottom = frameGetBot(frame, left.ColId);

                    if (bottom.IsStorm())
                    {
                        left.BREndPrep = frameEndPrepOuterFlat();

                        length = lengthCalculationStorm(length, bottom.ProfileHeight);

                        left.OffsetBR = frameOuterOffset(-bottom.ProfileHeight);
                    }
                    else
                    {
                        double botDif = frameGetDifferenceOuter(left.ProfileHeight, bottom.ProfileHeight);

                        left.BREndPrep = frameEndPrepOuter(botDif);

                        length = lengthCalculation(length, botDif);

                        left.OffsetBR = frameOuterOffset(botDif);
                    }
                }

                if (left.TLEndPrep == null)
                {
                    ACProfile transom = frameGetTransom(frame, left.ColId, left.RowId);

                    double chamfer = frameGetDifferenceMulTraOuter(frame, left.ProfileHeight, transom.ProfileHeight);

                    left.TLEndPrep = frameEndPrepOuter(chamfer);
                }

                if (left.BREndPrep == null)
                {
                    ACProfile transom = frameGetTransom(frame, left.ColId, left.MaxRow());

                    double chamfer = frameGetDifferenceMulTraOuter(frame, left.ProfileHeight, transom.ProfileHeight);

                    left.BREndPrep = frameEndPrepOuter(chamfer);
                }

                left.Length = length;

                count++;
            });
        }

        private void frameOuterEndPrepsAndLengthRight(ACFrame frame)
        {
            List<ACProfile> lstRight = frame.LstOuterRight();

            bool first = true;
            int count = 1;
            int lstRightCount = lstRight.Count;

            lstRight.OrderBy(o => o.RowId).ToList().ForEach(right =>
            {
                double length = right.GetRowSplitDist(frame);

                if (first)
                {
                    first = false;

                    ACProfile top = frameGetTop(frame, right.ColId);

                    double topDif = frameGetDifferenceOuter(right.ProfileHeight, top.ProfileHeight);

                    right.TLEndPrep = frameEndPrepOuter(topDif);

                    length = lengthCalculation(length, topDif);

                    right.OffsetTL = frameOuterOffset(topDif);
                }

                if (lstRightCount == count)
                {
                    ACProfile bottom = frameGetBot(frame, right.ColId);

                    if(bottom.IsStorm())
                    {
                        var botDif = bottom.ProfileHeight;

                        right.BREndPrep = frameEndPrepOuterFlat();

                        length = lengthCalculationStorm(length, bottom.ProfileHeight);

                        right.OffsetBR = frameOuterOffset(-bottom.ProfileHeight);
                    }
                    else
                    {
                        double botDif = frameGetDifferenceOuter(right.ProfileHeight, bottom.ProfileHeight);

                        right.BREndPrep = frameEndPrepOuter(botDif);

                        length = lengthCalculation(length, botDif);

                        right.OffsetBR = frameOuterOffset(botDif);
                    }
                }

                if (right.TLEndPrep == null)
                {
                    ACProfile transom = frameGetTransom(frame, right.ColId, right.RowId);

                    double chamfer = frameGetDifferenceMulTraOuter(frame, right.ProfileHeight, transom.ProfileHeight);

                    right.TLEndPrep = frameEndPrepOuter(chamfer);
                }

                if (right.BREndPrep == null)
                {
                    ACProfile transom = frameGetTransom(frame, right.ColId, right.MaxRow());

                    double chamfer = frameGetDifferenceMulTraOuter(frame, right.ProfileHeight, transom.ProfileHeight);

                    right.BREndPrep = frameEndPrepOuter(chamfer);
                }

                right.Length = length;

                count++;
            });
        }

        private void frameInnerEndPrepsAndLenghtMullion(ACFrame frame)
        {
            List<ACProfile> lstMullion = frame.LstInnerMullion();

            lstMullion.ForEach(mullion =>
            {
                double length = mullion.GetRowSplitDist(frame);

                List<ACProfile> peicesAbove = frameGetPeiceBesideMullion(frame, mullion.ColId, mullion.RowId);
                List<ACProfile> peicesBelow = frameGetPeiceBesideMullion(frame, mullion.ColId, mullion.MaxRow());

                ACProfile peiceAbove = peicesAbove.FirstOrDefault();
                ACProfile peiceBelow = peicesBelow.FirstOrDefault();

                ACProfile mullionAbove = lstMullion.Where(w => w.MaxRow() == mullion.RowId && w.ColId == mullion.ColId).FirstOrDefault();
                ACProfile mullionBelow = lstMullion.Where(w => w.RowId == mullion.MaxRow() && w.ColId == mullion.ColId).FirstOrDefault();

                //mean above == top
                if (peiceAbove.IsFrameOuter())
                {
                    double aboveDif = frameGetDifferenceMulTraOuter(frame, peiceAbove, mullion.ProfileHeight, mullion.TransomType);

                    mullion.TLEndPrep = frameEndPrepMulTra(mullion.TransomType);

                    length = lengthCalculation(length, -aboveDif);

                    mullion.OffsetTL = aboveDif;

                    if (peicesBelow.Count == 1)
                    {
                        addVnotch(peiceAbove, false, mullion.ColId, mullion.RowId, mullion.TransomType);
                    }
                }
                else //transom
                {
                    double aboveDif = frameGetDifferenceMulTraIntoMulTra(frame, peiceAbove.ProfileHeight, mullion.ProfileHeight, mullion.TransomType);
                    double chamfer = frameMulTraChamferFromDif(mullion.ProfileHeight, aboveDif);
                    bool tranRunThrough = (peiceAbove.ColId < mullion.ColId && peiceAbove.MaxCol() > mullion.ColId);

                    if (mullionBelow == null)
                    {
                        if (!tranRunThrough && peicesAbove.Count == 1)
                        {
                            BadResponseGenerator.Add(_response, note: new ResponseNote()
                            {
                                NoteType = EnResponseNote.CamdenError,
                                NoteMessage = $"Frame: {frame.FrameNumber} Mullions & Transoms can not meet as corners"
                            });
                        }

                        if (tranRunThrough)
                        {
                            addVnotch(peiceAbove, false, mullion.ColId, mullion.RowId, mullion.TransomType);
                        }

                        //always diamond with no mullion below unless dummy
                        mullion.TLEndPrep = frameEndPrepMulTra(mullion.TransomType);

                        length = lengthCalculationMulTra(length, -aboveDif);

                        mullion.OffsetTL = aboveDif;
                    }
                    else
                    {
                        if (peicesAbove.Count == 1)
                        {
                            if (tranRunThrough)
                            {
                                //either larger transom or same with no crucifix
                                mullion.TLEndPrep = frameEndPrepMulTra(mullion.TransomType);

                                length = lengthCalculationMulTra(length, -aboveDif);

                                mullion.OffsetTL = aboveDif;

                                addVnotch(peiceAbove, false, mullion.ColId, mullion.RowId, mullion.TransomType);
                            }
                            else
                            {
                                //transom off to 1 side & reverse butt weld to below mullion
                                bool isTL = peiceAbove.LstCol().Min() < mullion.ColId ? true : false;

                                mullion.TLEndPrep = frameEndPrepMulTraChamfer(chamfer, isTL);
                            }
                        }
                        else
                        {
                            if (aboveDif >= 0)
                            {
                                //either crucifix or transom is reverse butt weld
                                mullion.TLEndPrep = frameEndPrepMulTra(mullion.TransomType);

                                length = lengthCalculationMulTra(length, -aboveDif);

                                mullion.OffsetTL = aboveDif;
                            }
                            else
                            {
                                //reverse butt weld
                                mullion.TLEndPrep = frameEndPrepMulTraDoubleChamfer(chamfer);
                            }
                        }
                    }                    
                }

                //bottom outer
                if (peiceBelow.IsFrameOuter())
                {
                    double belowDif = frameGetDifferenceMulTraOuter(frame, peiceBelow, mullion.ProfileHeight, mullion.TransomType);

                    mullion.BREndPrep = frameEndPrepMulTra(mullion.TransomType);

                    length = lengthCalculation(length, -belowDif);

                    mullion.OffsetBR = belowDif;

                    if (peicesBelow.Count == 1)
                    {
                        addVnotch(peiceBelow, true, mullion.ColId, mullion.RowId, mullion.TransomType);
                    }
                }
                else if (peiceBelow.IsStorm())
                {
                    mullion.BREndPrep = frameEndPrepMulTraFlat();

                    double belowDif = frameGetDiffernceMulTraStorm(peiceBelow.ProfileHeight, mullion.TransomType);

                    length = lengthCalculationStorm(length, belowDif);

                    mullion.OffsetBR = belowDif;
                }
                else //transom
                {
                    double belowDif = frameGetDifferenceMulTraIntoMulTra(frame, peiceBelow.ProfileHeight, mullion.ProfileHeight, mullion.TransomType);
                    double chamfer = frameMulTraChamferFromDif(mullion.ProfileHeight, belowDif);
                    bool tranRunThrough = (peiceBelow.ColId < mullion.ColId && peiceBelow.MaxCol() > mullion.ColId);

                    if (mullionBelow == null)
                    {
                        if (!tranRunThrough && peicesBelow.Count == 1)
                        {
                            BadResponseGenerator.Add(_response, note: new ResponseNote()
                            {
                                NoteType = EnResponseNote.CamdenError,
                                NoteMessage = $"Frame: {frame.FrameNumber} Mullions & Transoms can not meet as corners"
                            });
                        }

                        if (tranRunThrough)
                        {
                            addVnotch(peiceBelow, true, mullion.ColId, mullion.RowId, mullion.TransomType);
                        }

                        //always diamond with no mullion below
                        mullion.BREndPrep = frameEndPrepMulTra(mullion.TransomType);

                        length = lengthCalculationMulTra(length, -belowDif);

                        mullion.OffsetBR = belowDif;
                    }
                    else
                    {
                        if (peicesBelow.Count == 1)
                        {
                            if (tranRunThrough)
                            {
                                //either larger transom or same with no crucifix
                                mullion.BREndPrep = frameEndPrepMulTra(mullion.TransomType);

                                length = lengthCalculationMulTra(length, -belowDif);

                                mullion.OffsetBR = belowDif;

                                addVnotch(peiceBelow, true, mullion.ColId, mullion.RowId, mullion.TransomType);
                            }
                            else
                            {
                                //transom off to 1 side & reverse butt weld to below mullion
                                bool isTL = peiceBelow.LstCol().Min() < mullion.ColId ? true : false;

                                mullion.BREndPrep = frameEndPrepMulTraChamfer(chamfer, isTL);
                            }
                        }
                        else
                        {
                            if (belowDif >= 0)
                            {
                                //either crucifix or transom is reverse butt weld
                                mullion.BREndPrep = frameEndPrepMulTra(mullion.TransomType);

                                length = lengthCalculationMulTra(length, -belowDif);

                                mullion.OffsetBR = belowDif;
                            }
                            else
                            {
                                //reverse butt weld
                                mullion.BREndPrep = frameEndPrepMulTraDoubleChamfer(chamfer);
                            }
                        }
                    }
                }

                mullion.Length = length;
            });
        }

        private void frameInnerEndPrepsAndLengthTransom(ACFrame frame)
        {
            List<ACProfile> lstTransom = frame.LstInnerTransom();

            lstTransom.ForEach(transom =>
            {
                double length = transom.GetColSplitDist(frame);

                List<ACProfile> peicesLeft = frameGetPeiceBesideTransom(frame, transom.ColId, transom.RowId);
                List<ACProfile> peicesRight = frameGetPeiceBesideTransom(frame, transom.MaxCol(), transom.RowId);

                ACProfile peiceLeft = peicesLeft.FirstOrDefault();
                ACProfile peiceRight = peicesRight.FirstOrDefault();

                ACProfile transomLeft = lstTransom.Where(w => w.MaxCol() == transom.ColId && w.RowId == transom.RowId).FirstOrDefault();
                ACProfile transomRight = lstTransom.Where(w => w.ColId == transom.MaxCol() && w.RowId == transom.RowId).FirstOrDefault();

                //left outer
                if (peiceLeft.IsFrameOuter())
                {
                    double leftDif = frameGetDifferenceMulTraOuter(frame, peiceLeft, transom.ProfileHeight, transom.TransomType);

                    transom.TLEndPrep = frameEndPrepMulTra(transom.TransomType);

                    length = lengthCalculation(length, -leftDif);

                    transom.OffsetTL = leftDif;

                    if (peicesLeft.Count == 1)
                    {
                        addVnotch(peiceLeft, false, transom.ColId, transom.RowId, transom.TransomType);
                    }
                }
                else //mullion
                {
                    double leftDif = frameGetDifferenceMulTraIntoMulTra(frame, peiceLeft.ProfileHeight, transom.ProfileHeight, transom.TransomType);
                    double chamfer = frameMulTraChamferFromDif(transom.ProfileHeight, leftDif);
                    bool mulRunThrough = (peiceLeft.RowId < transom.RowId && peiceLeft.MaxRow() > transom.RowId);

                    if (transomLeft == null)
                    {
                        if (!mulRunThrough && peicesLeft.Count == 1)
                        {
                            BadResponseGenerator.Add(_response, note: new ResponseNote()
                            {
                                NoteType = EnResponseNote.CamdenError,
                                NoteMessage = $"Frame: {frame.FrameNumber} Mullions & Transoms can not meet as corners"
                            });
                        }

                        if (mulRunThrough)
                        {
                            addVnotch(peiceLeft, false, transom.ColId, transom.RowId, transom.TransomType);
                        }

                        //always diamond with no mullion below unless dummy
                        transom.TLEndPrep = frameEndPrepMulTra(transom.TransomType);

                        length = lengthCalculationMulTra(length, -leftDif);

                        transom.OffsetTL = leftDif;
                    }
                    else
                    {
                        if (peicesLeft.Count == 1)
                        {
                            if (mulRunThrough)
                            {
                                //either larger mullion or same with no crucifix
                                transom.TLEndPrep = frameEndPrepMulTra(transom.TransomType);

                                length = lengthCalculationMulTra(length, -leftDif);

                                transom.OffsetTL = leftDif;

                                addVnotch(peiceLeft, false, transom.ColId, transom.RowId, transom.TransomType);
                            }
                            else
                            {
                                //mullion off to 1 side & reverse butt weld to left transom
                                bool isTL = peiceLeft.LstRow().Min() < transom.RowId ? true : false;

                                transom.TLEndPrep = frameEndPrepMulTraChamfer(chamfer, isTL);
                            }
                        }
                        else
                        {
                            if (leftDif >= 0)
                            {
                                //either crucifix or mullion is reverse butt weld
                                transom.TLEndPrep = frameEndPrepMulTra(transom.TransomType);

                                length = lengthCalculationMulTra(length, -leftDif);

                                transom.OffsetTL = leftDif;
                            }
                            else
                            {
                                //reverse buttweld
                                transom.TLEndPrep = frameEndPrepMulTraDoubleChamfer(chamfer);
                            }
                        }
                    }
                }

                //right outer
                if (peiceRight.IsFrameOuter())
                {
                    double rightDif = frameGetDifferenceMulTraOuter(frame, peiceRight, transom.ProfileHeight, transom.TransomType);

                    transom.BREndPrep = frameEndPrepMulTra(transom.TransomType);

                    length = lengthCalculation(length, -rightDif);

                    transom.OffsetBR = rightDif;

                    if (peicesRight.Count == 1)
                    {
                        addVnotch(peiceRight, true, transom.ColId, transom.RowId, transom.TransomType);
                    }
                }
                else //mullion
                {
                    double rightDif = frameGetDifferenceMulTraIntoMulTra(frame, peiceRight.ProfileHeight, transom.ProfileHeight, transom.TransomType);
                    double chamfer = frameMulTraChamferFromDif(transom.ProfileHeight, rightDif);
                    bool mulRunThrough = (peiceRight.RowId < transom.RowId && peiceRight.MaxRow() > transom.RowId);

                    if (transomRight == null)
                    {
                        if (!mulRunThrough && peicesRight.Count == 1)
                        {
                            BadResponseGenerator.Add(_response, note: new ResponseNote()
                            {
                                NoteType = EnResponseNote.CamdenError,
                                NoteMessage = $"Frame: {frame.FrameNumber} Mullions & Transoms can not meet as corners"
                            });
                        }

                        if (mulRunThrough)
                        {
                            addVnotch(peiceRight, true, transom.ColId, transom.RowId, transom.TransomType);
                        }

                        //always diamond with no mullion below
                        transom.BREndPrep = frameEndPrepMulTra(transom.TransomType);

                        length = lengthCalculationMulTra(length, -rightDif);

                        transom.OffsetBR = rightDif;
                    }
                    else
                    {
                        if (peicesRight.Count == 1)
                        {
                            if (mulRunThrough)
                            {
                                //either larger mullion or same with no crucifix
                                transom.BREndPrep = frameEndPrepMulTra(transom.TransomType);

                                length = lengthCalculationMulTra(length, -rightDif);

                                transom.OffsetBR = rightDif;

                                addVnotch(peiceRight, true, transom.ColId, transom.RowId, transom.TransomType);
                            }
                            else
                            {
                                //mullion off to 1 side & reverse butt weld to transom beside
                                bool isTL = peiceRight.LstRow().Min() < transom.RowId ? true : false;

                                transom.BREndPrep = frameEndPrepMulTraChamfer(chamfer, isTL);
                            }
                        }
                        else
                        {
                            if (rightDif >= 0)
                            {
                                //either larger mullion or same with no crucifix
                                transom.BREndPrep = frameEndPrepMulTra(transom.TransomType);

                                length = lengthCalculationMulTra(length, -rightDif);

                                transom.OffsetBR = rightDif;
                            }
                            else
                            {
                                //reverse butt weld
                                transom.BREndPrep = frameEndPrepMulTraDoubleChamfer(chamfer);
                            }
                        }
                    }
                }

                transom.Length = length;
            });
        }

        private ACProfile frameGetTop(ACFrame frame, int? colId)
        {
            int col = Convert.ToInt32(colId);

            return frame.LstOuterTop().Where(w => w.LstCol().Contains(col)).FirstOrDefault();
        }

        private ACProfile frameGetBot(ACFrame frame, int? colId)
        {
            int col = Convert.ToInt32(colId);

            return frame.LstOuterBottom().Where(w => w.LstCol().Contains(col)).FirstOrDefault();
        }

        private ACProfile frameGetLeft(ACFrame frame, int? rowId)
        {
            int row = Convert.ToInt32(rowId);

            return frame.LstOuterLeft().Where(w => w.LstRow().Contains(row)).FirstOrDefault();
        }

        private ACProfile frameGetRight(ACFrame frame, int? rowId)
        {
            int row = Convert.ToInt32(rowId);

            return frame.LstOuterRight().Where(w => w.LstRow().Contains(row)).FirstOrDefault();
        }

        private ACProfile frameGetMullion(ACFrame frame, int? colId, int? rowId)
        {
            int row = Convert.ToInt32(rowId);

            List<ACProfile> lstMullion = frame.LstInnerMullion().Where(w => w.LstRow().Contains(row) && w.ColId == colId).ToList();

            if (lstMullion.Count != 1)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber} has a split in Outer/Bottom Top with no relevant mullion"
                });
            }

            return lstMullion.FirstOrDefault();
        }

        private ACProfile frameGetTransom(ACFrame frame, int? colId, int? rowId)
        {
            int col = Convert.ToInt32(colId);

            List<ACProfile> lstTransom = frame.LstInnerTransom().Where(w => w.LstCol().Contains(col) && w.RowId == rowId).ToList();

            if (lstTransom.Count != 1)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber} has a split in Outer Left/Right with no relevant transom"
                });
            }

            return lstTransom.FirstOrDefault();
        }

        private List<ACProfile> frameGetPeiceBesideMullion(ACFrame frame, int? colId, int? rowId)
        {
            int col = Convert.ToInt32(colId);

            List<ACProfile> lstBeside = frame.LstProfile.Where(w => !w.IsVert() &&
                                w.RowId == rowId && w.LstCol().Contains(col)).ToList();

            if (lstBeside.Count == 0)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber} has a mullion with no valid peice beside transom/top/bottom"
                });
            }
            else if (lstBeside.Count > 2)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber} has a mullion with too many valid peices beside transom/top/bottom"
                });
            }

            //1||2 will both be same profile above so first is ok
            return lstBeside;
        }

        private List<ACProfile> frameGetPeiceBesideTransom(ACFrame frame, int? colId, int? rowId)
        {
            int row = Convert.ToInt32(rowId);

            List<ACProfile> lstBeside = frame.LstProfile.Where(w => w.IsVert() &&
                                w.ColId == colId && w.LstRow().Contains(row)).ToList();

            if (lstBeside.Count == 0)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber} has a transom with no valid peice beside mullion/left/right"
                });
            }
            else if (lstBeside.Count > 2)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber} has a transom with too many valid peices beside mullion/left/right"
                });
            }

            //1||2 will both be same profile above so first is ok
            return lstBeside;
        }

        private double frameGetDifferenceOuter(double profileHeight, double ConnectingProfileHeight)
        {
            //if positive means chamfer
            //if negative means peice gets smaller
            return profileHeight - ConnectingProfileHeight;
        }

        private double frameGetDifferenceMulTraOuter(ACFrame frame, double profileOuterHeight, double mulTraHeight)
        {
            double dif = profileOuterHeight - (mulTraHeight / 2);

            if (dif <= 0)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber} has a Mullion/Tra of profile Height {mulTraHeight} going into outer" +
                    $"of profile height {profileOuterHeight}, this is not possible"
                });
            }

            return dif;
        }

        private double frameGetDifferenceMulTraOuter(ACFrame frame, ACProfile profileOuter, double mulTraHeight, EnTransomType? transomType)
        {
            double dif = 0.0;

            if (transomType == EnTransomType.Dummy)
            {
                dif = profileOuter.ProfileHeight + _settings.DummyMullionProfileOffset;
            }
            else
            {
                dif = frameGetDifferenceMulTraOuter(frame, profileOuter.ProfileHeight, mulTraHeight);
            }

            return dif;
        }

        private double frameMulTraChamferFromDif(double mulTraHeight, double previousDif)
        {
            //previousDif will be negative so -- = +
            double chamfer = (mulTraHeight / 2) - previousDif;

            return chamfer;
        }

        private double frameGetDiffernceMulTraStorm(double stormHeight, EnTransomType? transomType)
        {
            double dif = 0.0;

            if(transomType == EnTransomType.Dummy)
            {
                dif = stormHeight + _settings.DummyMullionAliOffset;
            }
            else
            {
                dif = stormHeight;
            }

            return dif;
        }

        private double frameGetDifferenceMulTraIntoMulTra(ACFrame frame, double profileGoingIntoHeight, double inputMulTraHeight, EnTransomType? transomType)
        {
            double dif = 0.0;

            if (transomType == EnTransomType.Dummy)
            {
                dif = (profileGoingIntoHeight / 2) + _settings.DummyMullionProfileOffset;
            }
            else
            {
                //can be - or + small mull ino larger = +
                //same mull/tra = 0
                //reverse butweld large = -
                dif = (profileGoingIntoHeight / 2) - (inputMulTraHeight / 2);
            }

            return dif;
        }

        private double frameOuterOffset(double dif)
        {
            if (dif < 0)
            {
                return -dif;
            }

            return 0.0;
        }

        private ClsEndPrep frameEndPrepOuter(double difference)
        {
            ClsEndPrep endPrep = null;

            if (difference > 0)
            {
                endPrep = new ClsEndPrep()
                {
                    EndPrep = EnEndPrep.Chamfer,
                    Chamfer = difference
                };
            }
            else
            {
                endPrep = new ClsEndPrep()
                {
                    EndPrep = EnEndPrep.Acute
                };
            }

            return endPrep;
        }

        private ClsEndPrep frameEndPrepOuterFlat()
        {
            ClsEndPrep endPrep = new ClsEndPrep()
            {
                EndPrep = EnEndPrep.Flat
            };

            return endPrep;
        }

        private ClsEndPrep frameEndPrepMulTra(EnTransomType? transomType)
        {
            ClsEndPrep endPrep = null;

            if (transomType == EnTransomType.Dummy)
            {
                endPrep = new ClsEndPrep()
                {
                    EndPrep = EnEndPrep.Flat
                };
            }
            else
            {
                endPrep = new ClsEndPrep()
                {
                    EndPrep = EnEndPrep.Diamond
                };
            }

            return endPrep;
        }

        private ClsEndPrep frameEndPrepMulTraChamfer(double chamfer, bool isTL)
        {
            ClsEndPrep endPrep = new ClsEndPrep();

            if (isTL)
            {
                endPrep.EndPrep = EnEndPrep.Chamfer;
                endPrep.MulTraChamferTL = chamfer;
            }
            else
            {
                endPrep.EndPrep = EnEndPrep.Chamfer;
                endPrep.MulTraChamferBR = chamfer;
            }

            return endPrep;
        }

        private ClsEndPrep frameEndPrepMulTraDoubleChamfer(double chamfer)
        {
            ClsEndPrep endPrep = new ClsEndPrep()
            {
                EndPrep = EnEndPrep.DoubleChamfer,
                Chamfer = chamfer
            };

            return endPrep;
        }

        private ClsEndPrep frameEndPrepMulTraFlat()
        {
            return frameEndPrepOuterFlat();
        }

        private double lengthCalculation(double length, double difference)
        {
            if (difference < 0)
            {
                length += difference;
            }

            return length;
        }

        private double lengthCalculationMulTra(double length, double difference)
        {
            return length + difference;
        }

        private double lengthCalculationStorm(double length, double stormHeight)
        {
            return length - stormHeight;
        }

        private void sashEndPrepsAndLength(ACFrame frame)
        {
            frame.LstNormalSash().ForEach(sash =>
            {
                //all out in one go as all is the same
                sashOuterEndPrepsAndLength(frame, sash);

                sashTransomEndPrepsAndLength(frame, sash);

                sashMullionEndPrepsAndLength(frame, sash);
            });
            
        }

        private void sashOuterEndPrepsAndLength(ACFrame frame, ACSash sash)
        {
            List<ACProfile> lstSashOuter = sash.SashOuter(frame);

            lstSashOuter.ForEach(outer =>
            {
                double length = 0.0;

                //always will be 45's acute
                outer.TLEndPrep = sashEndPrepOuter();
                outer.BREndPrep = sashEndPrepOuter();                

                //top/bottom sash peice
                if(!outer.IsVert())
                {
                    length = outer.GetColSplitDist(frame);

                    ACProfile leftFramePeice = sashGetFrameLeft(frame, outer.ColId, outer.RowId);
                    ACProfile rightFramePeice = sashGetFrameRight(frame, outer.MaxCol(), outer.RowId);

                    double leftDif = sashGetFrameDif(frame, sash, leftFramePeice);
                    double rightDif = sashGetFrameDif(frame, sash, rightFramePeice);

                    sash.OffsetLeft = leftDif;
                    sash.OffsetRight = rightDif;

                    outer.OffsetTL = -leftDif;
                    outer.OffsetBR = -rightDif;

                    double total = leftDif + rightDif;


                    length = lengthCalculation(length, total);
                }
                else //left right
                {
                    length = outer.GetRowSplitDist(frame);

                    ACProfile aboveFramePeice = sashGetFrameAbove(frame, outer.ColId, outer.RowId);
                    ACProfile belowFramePeice = sashGetFrameBelow(frame, outer.ColId, outer.MaxRow());

                    double aboveDif = sashGetFrameDif(frame, sash, aboveFramePeice);
                    double belowDif = sashGetFrameDif(frame, sash, belowFramePeice);

                    sash.OffsetTop = aboveDif;
                    sash.OffsetBot = belowDif;

                    outer.OffsetTL = -aboveDif;
                    outer.OffsetBR = -belowDif;

                    double total = aboveDif + belowDif;

                    length = lengthCalculation(length, total);
                }

                outer.Length = length;
            });
        }

        private void sashTransomEndPrepsAndLength(ACFrame frame, ACSash sash)
        {
            List<ACProfile> lstSashTransom = sash.LstInnerTransom();

            lstSashTransom.ForEach(transom =>
            {
                double length = transom.GetColSplitDist(frame);

                //all tranoms of sash wil have diamonds end!
                transom.TLEndPrep = sashEndPrepMulTraDiamond();
                transom.BREndPrep = sashEndPrepMulTraDiamond();

                ACProfile leftSashPeice = sashGetPeiceBesideTransom(frame, sash, transom.ColId, transom.RowId);
                ACProfile rightSashPeice = sashGetPeiceBesideTransom(frame, sash, transom.MaxCol(), transom.RowId);

                if (leftSashPeice.IsSashOuter())
                {
                    ACProfile leftFramePeice = frameGetPeiceBesideTransom(frame, transom.ColId, transom.RowId).FirstOrDefault();

                    double leftSashDif = sashGetDifferenceMulTraOuter(frame, sash, leftSashPeice.ProfileHeight, transom.ProfileHeight);

                    double leftFrameDif = sashGetFrameDif(frame, sash, leftFramePeice);

                    double total = leftSashDif + leftFrameDif;

                    length = lengthCalculation(length, total);

                    transom.OffsetTL = -total;

                    addVnotch(leftSashPeice, false, transom.ColId, transom.RowId, transom.TransomType);
                }
                else
                {
                    double leftDif = sashGetDifferenceMulTraIntoMulTra(frame, sash, leftSashPeice.ProfileHeight, transom.ProfileHeight);
                    bool mulRunThrough = (leftSashPeice.RowId < transom.RowId && leftSashPeice.MaxRow() > transom.RowId);

                    if (mulRunThrough)
                    {
                        addVnotch(leftSashPeice, false, transom.ColId, transom.RowId, transom.TransomType);
                    }

                    length = lengthCalculation(length, leftDif);

                    transom.OffsetTL = -leftDif;
                }

                if (rightSashPeice.IsSashOuter())
                {
                    ACProfile rightFramePeice = frameGetPeiceBesideTransom(frame, transom.MaxCol(), transom.RowId).FirstOrDefault();

                    double rightSashDif = sashGetDifferenceMulTraOuter(frame, sash, rightSashPeice.ProfileHeight, transom.ProfileHeight);

                    double rightFrameDif = sashGetFrameDif(frame, sash, rightFramePeice);

                    double total = rightSashDif + rightFrameDif;

                    length = lengthCalculation(length, total);

                    transom.OffsetBR = -total;

                    addVnotch(rightSashPeice, true, transom.ColId, transom.RowId, transom.TransomType);
                }
                else
                {
                    double rightDif = sashGetDifferenceMulTraIntoMulTra(frame, sash, rightSashPeice.ProfileHeight, transom.ProfileHeight);
                    bool mulRunThrough = (rightSashPeice.RowId < transom.RowId && rightSashPeice.MaxRow() > transom.RowId);

                    if (mulRunThrough)
                    {
                        addVnotch(leftSashPeice, true, transom.ColId, transom.RowId, transom.TransomType);
                    }

                    length = lengthCalculation(length, rightDif);

                    transom.OffsetBR = -rightDif;
                }

                transom.Length = length;
            });
        }

        private void sashMullionEndPrepsAndLength(ACFrame frame, ACSash sash)
        {
            List<ACProfile> lstSashMullion = sash.LstInnerMullion();

            lstSashMullion.ForEach(mullion =>
            {
                double length = mullion.GetRowSplitDist(frame);

                //all mullion os sash will have diamond ends!
                mullion.TLEndPrep = sashEndPrepMulTraDiamond();
                mullion.BREndPrep = sashEndPrepMulTraDiamond();

                ACProfile aboveSashPeice = sashGetPeiceBesideMullion(frame, sash, mullion.ColId, mullion.RowId);
                ACProfile belowSashPeice = sashGetPeiceBesideMullion(frame, sash, mullion.ColId, mullion.MaxRow());

                if (aboveSashPeice.IsSashOuter())
                {
                    ACProfile aboveFramePeice = frameGetPeiceBesideMullion(frame, mullion.ColId, mullion.RowId).FirstOrDefault();

                    double aboveSashDif = sashGetDifferenceMulTraOuter(frame, sash, aboveSashPeice.ProfileHeight, mullion.ProfileHeight);

                    double aboveFrameDif = sashGetFrameDif(frame, sash, aboveFramePeice);

                    double total = aboveSashDif + aboveFrameDif;

                    length = lengthCalculation(length, total);

                    mullion.OffsetTL = -total;

                    addVnotch(aboveSashPeice, false, mullion.ColId, mullion.RowId, mullion.TransomType);
                }
                else
                {
                    double aboveDif = sashGetDifferenceMulTraIntoMulTra(frame, sash, aboveSashPeice.ProfileHeight, mullion.ProfileHeight);
                    bool tranRunThrough = (aboveSashPeice.ColId < mullion.ColId && aboveSashPeice.MaxCol() > mullion.ColId);

                    if (tranRunThrough)
                    {
                        addVnotch(aboveSashPeice, false, mullion.ColId, mullion.RowId, mullion.TransomType);
                    }

                    length = lengthCalculation(length, aboveDif);

                    mullion.OffsetTL = -aboveDif;
                }

                if (belowSashPeice.IsSashOuter())
                {
                    ACProfile belowFramePeice = frameGetPeiceBesideMullion(frame, mullion.ColId, mullion.MaxRow()).FirstOrDefault();

                    double belowSashDif = sashGetDifferenceMulTraOuter(frame, sash, belowSashPeice.ProfileHeight, mullion.ProfileHeight);

                    double belowFrameDif = sashGetFrameDif(frame, sash, belowFramePeice);

                    double total = belowSashDif + belowFrameDif;

                    length = lengthCalculation(length, total);

                    mullion.OffsetBR = -total;

                    addVnotch(belowSashPeice, true, mullion.ColId, mullion.RowId, mullion.TransomType);
                }
                else
                {
                    double belowDif = sashGetDifferenceMulTraIntoMulTra(frame, sash, belowSashPeice.ProfileHeight, mullion.ProfileHeight);
                    bool tranRunThrough = (belowSashPeice.ColId < mullion.ColId && belowSashPeice.MaxCol() > mullion.ColId);

                    if(tranRunThrough)
                    {
                        addVnotch(belowSashPeice, true, mullion.ColId, mullion.RowId, mullion.TransomType);
                    }

                    length = lengthCalculation(length, belowDif);

                    mullion.OffsetBR = -belowDif;
                }

                mullion.Length = length;
            });
        }

        private ACProfile sashGetFrameLeft(ACFrame frame, int? colId, int? rowId)
        {
            int col = Convert.ToInt32(colId);

            if(col == 0)
            {
                return frameGetLeft(frame, rowId);
            }
            else
            {
                return frameGetPeiceBesideTransom(frame, colId, rowId).FirstOrDefault();
            }
        }

        private ACProfile sashGetFrameRight(ACFrame frame, int? colId, int? rowId)
        {
            int col = Convert.ToInt32(colId);

            if(col == frame.MaxCol())
            {
                return frameGetRight(frame, rowId);
            }
            else
            {
                return frameGetPeiceBesideTransom(frame, colId, rowId).FirstOrDefault();
            }
        }

        private ACProfile sashGetFrameAbove(ACFrame frame, int? colId, int? rowId)
        {
            int row = Convert.ToInt32(rowId);

            if (row == 0)
            {
                return frameGetTop(frame, colId);
            }
            else
            {
                return frameGetPeiceBesideMullion(frame, colId, rowId).FirstOrDefault();
            }
        }

        private ACProfile sashGetFrameBelow(ACFrame frame, int? colId, int? rowId)
        {
            int row = Convert.ToInt32(rowId);

            if (row == frame.MaxRow())
            {
                return frameGetBot(frame, colId);
            }
            else
            {
                return frameGetPeiceBesideMullion(frame, colId, rowId).FirstOrDefault();
            }
        }

        private ACProfile sashGetPeiceBesideTransom(ACFrame frame, ACSash sash, int? colId, int? rowId)
        {
            int row = Convert.ToInt32(rowId);

            List<ACProfile> lstBeside = sash.LstProfile.Where(w => w.IsVert() &&
                                w.ColId == colId && w.LstRow().Contains(row)).ToList();

            if (lstBeside.Count == 0)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber}, Sash: {sash.SashNumber} has a transom with no valid peice beside mullion/left/right"
                });
            }
            else if (lstBeside.Count > 2)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber}, Sash: {sash.SashNumber} has a transom with too many valid peices beside (mullion's)"
                });
            }

            //1||2 will both be same profile above so first is ok
            return lstBeside.FirstOrDefault();
        }

        private ACProfile sashGetPeiceBesideMullion(ACFrame frame, ACSash sash, int? colId, int? rowId)
        {
            int col = Convert.ToInt32(colId);

            List<ACProfile> lstBeside = sash.LstProfile.Where(w => !w.IsVert() &&
                                w.RowId == rowId && w.LstCol().Contains(col)).ToList();

            if (lstBeside.Count == 0)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber}, Sash: {sash.SashNumber} has a mullion with no valid peice beside transom/top/bottom"
                });
            }
            else if (lstBeside.Count > 2)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber}, Sash: {sash.SashNumber} has a mullion with too many valid peices beside (transoms)"
                });
            }

            //1||2 will both be same profile above so first is ok
            return lstBeside.FirstOrDefault();
        }

        private double sashGetFrameDifCalc(ACProfile frameProfile)
        {
            double dif = 0.0;

            if (frameProfile.IsFrameOuter())
            {
                dif = frameProfile.ProfileHeight
                    - frameProfile.RebateInternal
                    - _settings.SashOverlap;
            }
            else if (frameProfile.IsStorm())
            {
                dif = sashGetDifferenceStormBottom(frameProfile);
            }
            else //mullion or transom
            {
                dif = (frameProfile.ProfileHeight / 2)
                    - frameProfile.RebateInternal
                    - _settings.SashOverlap;
            }

            return dif;
        }

        private double sashGetFrameDif(ACFrame frame, ACSash sash, ACProfile frameProfile)
        {
            double dif = sashGetFrameDifCalc(frameProfile);

            if (dif <= 0)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber}, Sash: {sash.SashNumber} overlaps its outer/mullion/transom completely, this is not possible"
                });
            }

            return -dif;
        }

        private double sashGetDifferenceStormBottom(ACProfile storm)
        {
            return storm.SashOffSet;
        }

        private double sashGetDifferenceMulTraIntoMulTra(ACFrame frame, ACSash sash, double profileGoingIntoHeight, double inputMulTraHeight)
        {
            //can be + but not - on sash!!!
            //same mull/tra = 0
            double dif = (profileGoingIntoHeight / 2) - (inputMulTraHeight / 2);

            if (dif < 0)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber}, Sash: {sash.SashNumber} has a larger mullion/transom going into a smaller mullion/transom, this is not possible"
                });
            }

            return -dif;
        }

        private double sashGetDifferenceMulTraOuter(ACFrame frame, ACSash sash, double profileOuterHeight, double mulTraHeight)
        {
            double dif = profileOuterHeight - (mulTraHeight / 2);

            if (dif <= 0)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber}, Sash: {sash.SashNumber} has a Mullion/Tra of profile Height {mulTraHeight} going into outer" +
                    $"of profile height {profileOuterHeight}, this is not possible"
                });
            }

            return -dif;
        }

        private ClsEndPrep sashEndPrepOuter()
        {
            ClsEndPrep endPrep = new ClsEndPrep()
            {
                EndPrep = EnEndPrep.Acute
            };

            return endPrep;
        }

        private ClsEndPrep sashEndPrepMulTraDiamond()
        {
            ClsEndPrep endPrep = new ClsEndPrep()
            {
                EndPrep = EnEndPrep.Diamond
            };

            return endPrep;
        }

        private void sashCeptionEndPrepsAndLength(ACFrame frame)
        {
            frame.LstNormalSash().ForEach(sash =>
            {
                sash.LstSashCeption.ForEach(sashCeption =>
                {
                    sashCeptionOuterEndPrepsAndLength(frame, sash, sashCeption);

                    sashCeptionTransomEndPrepsAndLength(frame, sash, sashCeption);

                    sashCeptionMullionEndPrepsAndLength(frame, sash, sashCeption);
                });
            });
        }

        private void sashCeptionOuterEndPrepsAndLength(ACFrame frame, ACSash sash, ACSashCeption sashCeption)
        {
            List<ACProfile> lstSashCeptionOuter = sashCeption.SashOuter(frame);

            lstSashCeptionOuter.ForEach(outer =>
            {
                double length = 0.0;

                //always will be 45's acute
                outer.TLEndPrep = sashEndPrepOuter();
                outer.BREndPrep = sashEndPrepOuter();

                //top/bottom sash peice
                if (!outer.IsVert())
                {
                    length = outer.GetColSplitDist(frame);

                    ACProfile leftSashPeice = sashCeptionGetSashLeft(frame, sash, outer.ColId, outer.RowId);
                    ACProfile rightSashPeice = sashCeptionGetSashRight(frame, sash, outer.MaxCol(), outer.RowId);

                    if (leftSashPeice.IsSashOuter())
                    {
                        ACProfile leftFramePeice = sashGetFrameLeft(frame, outer.ColId, outer.RowId);

                        double sashLeftDif = sashCeptionGetSashDifference(leftSashPeice);
                        double frameLeftDif = sashCeptionGetFrameDif(frame, sashCeption, leftFramePeice);

                        double total = sashLeftDif + frameLeftDif;

                        sashCeption.OffsetLeft = total;

                        outer.OffsetTL = -total;

                        length = lengthCalculation(length, total);

                    }
                    else
                    {
                        double leftDif = sashCeptionGetSashMulTraDifference(leftSashPeice);

                        sashCeption.OffsetLeft = leftDif;

                        outer.OffsetTL = -leftDif;

                        length = lengthCalculation(length, leftDif);
                    }

                    if (rightSashPeice.IsSashOuter())
                    {
                        ACProfile rightFramePeice = sashGetFrameRight(frame, outer.MaxCol(), outer.RowId);

                        double sashRightDif = sashCeptionGetSashDifference(rightSashPeice);
                        double frameRightDif = sashCeptionGetFrameDif(frame, sashCeption, rightFramePeice);

                        double total = sashRightDif + frameRightDif;

                        sashCeption.OffsetRight = total;

                        outer.OffsetBR = -total;

                        length = lengthCalculation(length, total);
                    }
                    else
                    {
                        double rightDif = sashCeptionGetSashMulTraDifference(rightSashPeice);

                        sashCeption.OffsetRight = rightDif;

                        outer.OffsetBR = -rightDif;

                        length = lengthCalculation(length, rightDif);
                    }

                }
                else //left/right
                {
                    length = outer.GetRowSplitDist(frame);

                    ACProfile aboveSashPeice = sashCeptionGetSashTop(frame, sash, outer.ColId, outer.RowId);
                    ACProfile belowSashPeice = sashCeptionGetSashBottom(frame, sash, outer.ColId, outer.MaxRow());

                    if(aboveSashPeice.IsSashOuter())
                    {
                        ACProfile aboveFramePeice = sashGetFrameAbove(frame, outer.ColId, outer.RowId);

                        double sashAboveDif = sashCeptionGetSashDifference(aboveSashPeice);
                        double frameAboveDif = sashCeptionGetFrameDif(frame, sashCeption, aboveFramePeice);

                        double total = sashAboveDif + frameAboveDif;

                        sashCeption.OffsetTop = total;

                        outer.OffsetTL = -total;

                        length = lengthCalculation(length, total);
                    }
                    else
                    {
                        double topDif = sashCeptionGetSashMulTraDifference(aboveSashPeice);

                        sashCeption.OffsetTop = topDif;

                        outer.OffsetTL = -topDif;

                        length = lengthCalculation(length, topDif);
                    }

                    if(belowSashPeice.IsSashOuter())
                    {
                        ACProfile belowFramePeice = sashGetFrameBelow(frame, outer.ColId, outer.MaxRow());

                        double sashBelowDif = sashCeptionGetSashDifference(belowSashPeice);
                        double frameBelowDif = sashCeptionGetFrameDif(frame, sashCeption, belowSashPeice);

                        double total = sashBelowDif + frameBelowDif;

                        sashCeption.OffsetBot = total;

                        outer.OffsetBR = -total;

                        length = lengthCalculation(length, total);
                    }
                    else
                    {
                        double bottomDif = sashCeptionGetSashMulTraDifference(belowSashPeice);

                        sashCeption.OffsetBot = bottomDif;

                        outer.OffsetBR = -bottomDif;

                        length = lengthCalculation(length, bottomDif);
                    }
                }

                outer.Length = length;
            });
        }

        private void sashCeptionTransomEndPrepsAndLength(ACFrame frame, ACSash sash, ACSashCeption sashCeption)
        {
            List<ACProfile> lstSashCeptionTransom = sashCeption.LstInnerTransom();

            lstSashCeptionTransom.ForEach(transom =>
            {
                double length = transom.GetColSplitDist(frame);

                //all tranoms of sashCeption will have diamonds end!
                transom.TLEndPrep = sashEndPrepMulTraDiamond();
                transom.BREndPrep = sashEndPrepMulTraDiamond();

                ACProfile leftSashCeptionPeice = sashCeptionGetPeiceBesideTransom(frame, sashCeption, transom.ColId, transom.RowId);
                ACProfile rightSashCeptionPeice = sashCeptionGetPeiceBesideTransom(frame, sashCeption, transom.MaxCol(), transom.RowId);

                if (leftSashCeptionPeice.IsSashOuter())
                {
                    ACProfile leftSashPeice = sashGetPeiceBesideTransom(frame, sash, transom.ColId, transom.RowId);

                    double leftSashCeptionDif = sashCeptionGetDifferenceMulTraOuter(frame, sashCeption, leftSashCeptionPeice.ProfileHeight, transom.ProfileHeight);

                    if (leftSashPeice.IsSashOuter())
                    {
                        ACProfile leftFramePeice = frameGetPeiceBesideTransom(frame, transom.ColId, transom.RowId).FirstOrDefault();

                        double leftSashDif = sashCeptionGetSashDifference(leftSashPeice);
                        double leftFrameDif = sashCeptionGetFrameDif(frame, sashCeption, leftFramePeice);

                        double total = leftSashCeptionDif + leftSashDif + leftFrameDif;

                        length = lengthCalculation(length, total);

                        transom.OffsetTL = -total;

                    }
                    else
                    {
                        double leftSashDif = sashCeptionGetSashMulTraDifference(leftSashPeice);

                        double total = leftSashCeptionDif + leftSashDif;

                        length = lengthCalculation(length, total);

                        transom.OffsetTL = -total;
                    }

                    addVnotch(leftSashCeptionPeice, false, transom.ColId, transom.RowId, transom.TransomType);
                }
                else
                {
                    double leftDif = sashCeptionGetDifferenceMulTraIntoMulTra(frame, sashCeption, leftSashCeptionPeice.ProfileHeight, transom.ProfileHeight);
                    bool mulRunThrough = (leftSashCeptionPeice.RowId < transom.RowId && leftSashCeptionPeice.MaxRow() > transom.RowId);

                    if (mulRunThrough)
                    {
                        addVnotch(leftSashCeptionPeice, false, transom.ColId, transom.RowId, transom.TransomType);
                    }

                    length = lengthCalculation(length, leftDif);

                    transom.OffsetTL = -leftDif;
                }

                if (rightSashCeptionPeice.IsSashOuter())
                {
                    ACProfile rightSashPeice = sashGetPeiceBesideTransom(frame, sash, transom.MaxCol(), transom.RowId);

                    double rightSashCeptionDif = sashCeptionGetDifferenceMulTraOuter(frame, sashCeption, rightSashCeptionPeice.ProfileHeight, transom.ProfileHeight);

                    if (rightSashPeice.IsSashOuter())
                    {
                        ACProfile rightFramePeice = frameGetPeiceBesideTransom(frame, transom.MaxCol(), transom.RowId).FirstOrDefault();

                        double rightSashDif = sashCeptionGetSashDifference(rightSashPeice);
                        double rightFrameDif = sashCeptionGetFrameDif(frame, sashCeption, rightFramePeice);

                        double total = rightSashCeptionDif + rightSashDif + rightFrameDif;

                        length = lengthCalculation(length, total);

                        transom.OffsetBR = -total;
                    }
                    else
                    {
                        double rightSashDif = sashCeptionGetSashMulTraDifference(rightSashPeice);

                        double total = rightSashCeptionDif + rightSashDif;

                        length = lengthCalculation(length, total);

                        transom.OffsetBR = -total;
                    }

                    addVnotch(rightSashCeptionPeice, true, transom.ColId, transom.RowId, transom.TransomType);
                }
                else
                {
                    double rightDif = sashCeptionGetDifferenceMulTraIntoMulTra(frame, sashCeption, rightSashCeptionPeice.ProfileHeight, transom.ProfileHeight);
                    bool mulRunThrough = (rightSashCeptionPeice.RowId < transom.RowId && rightSashCeptionPeice.MaxRow() > transom.RowId);

                    if (mulRunThrough)
                    {
                        addVnotch(rightSashCeptionPeice, true, transom.ColId, transom.RowId, transom.TransomType);
                    }

                    length = lengthCalculation(length, rightDif);

                    transom.OffsetBR = -rightDif;
                }

                transom.Length = length;
            });
        }

        private void sashCeptionMullionEndPrepsAndLength(ACFrame frame, ACSash sash, ACSashCeption sashCeption)
        {
            List<ACProfile> lstSashCeptionMullion = sashCeption.LstInnerMullion();

            lstSashCeptionMullion.ForEach(mullion =>
            {
                double length = mullion.GetRowSplitDist(frame);

                //all mullion of sashCeption will have diamond ends!
                mullion.TLEndPrep = sashEndPrepMulTraDiamond();
                mullion.BREndPrep = sashEndPrepMulTraDiamond();

                ACProfile aboveSashCeptionPeice = sashCeptionGetPeiceBesideMullion(frame, sashCeption, mullion.ColId, mullion.RowId);
                ACProfile belowSashCeptionPeice = sashCeptionGetPeiceBesideMullion(frame, sashCeption, mullion.ColId, mullion.MaxRow());

                if (aboveSashCeptionPeice.IsSashOuter())
                {
                    ACProfile aboveSashPeice = sashGetPeiceBesideMullion(frame, sash, mullion.ColId, mullion.RowId);

                    double aboveSashCeptionDif = sashCeptionGetDifferenceMulTraOuter(frame, sashCeption, aboveSashCeptionPeice.ProfileHeight, mullion.ProfileHeight);

                    if(aboveSashPeice.IsSashOuter())
                    {
                        ACProfile aboveFramePeice = frameGetPeiceBesideMullion(frame, mullion.ColId, mullion.RowId).FirstOrDefault();

                        double aboveSashDif = sashCeptionGetSashDifference(aboveSashPeice);
                        double aboveFrameDif = sashCeptionGetFrameDif(frame, sashCeption, aboveFramePeice);

                        double total = aboveSashCeptionDif + aboveSashDif + aboveFrameDif;

                        length = lengthCalculation(length, total);

                        mullion.OffsetTL = -total;
                    }
                    else
                    {
                        double aboveSashDif = sashCeptionGetSashMulTraDifference(aboveSashPeice);

                        double total = aboveSashCeptionDif + aboveSashDif;

                        length = lengthCalculation(length, total);

                        mullion.OffsetTL = -total;
                    }

                    addVnotch(aboveSashCeptionPeice, false, mullion.ColId, mullion.RowId, mullion.TransomType);
                }
                else
                {
                    double aboveDif = sashCeptionGetDifferenceMulTraIntoMulTra(frame, sashCeption, aboveSashCeptionPeice.ProfileHeight, mullion.ProfileHeight);
                    bool tranRunThrough = (aboveSashCeptionPeice.ColId < mullion.ColId && aboveSashCeptionPeice.MaxCol() > mullion.ColId);

                    if (tranRunThrough)
                    {
                        addVnotch(aboveSashCeptionPeice, false, mullion.ColId, mullion.RowId, mullion.TransomType);
                    }

                    length = lengthCalculation(length, aboveDif);

                    mullion.OffsetTL = -aboveDif;
                }

                if (belowSashCeptionPeice.IsSashOuter())
                {
                    ACProfile belowSashPeice = sashGetPeiceBesideMullion(frame, sash, mullion.ColId, mullion.MaxRow());

                    double belowSashCeptionDif = sashCeptionGetDifferenceMulTraOuter(frame, sashCeption, belowSashCeptionPeice.ProfileHeight, mullion.ProfileHeight);

                    if (belowSashPeice.IsSashOuter())
                    {
                        ACProfile belowFramePeice = frameGetPeiceBesideMullion(frame, mullion.ColId, mullion.MaxRow()).FirstOrDefault();

                        double belowSashDif = sashCeptionGetSashDifference(belowSashPeice);
                        double belowFrameDif = sashCeptionGetFrameDif(frame, sashCeption, belowFramePeice);

                        double total = belowSashCeptionDif + belowSashDif + belowFrameDif;

                        length = lengthCalculation(length, total);

                        mullion.OffsetBR = -total;
                    }
                    else
                    {
                        double belowSashDif = sashCeptionGetSashMulTraDifference(belowSashPeice);

                        double total = belowSashCeptionDif + belowSashDif;

                        length = lengthCalculation(length, total);

                        mullion.OffsetBR = -total;
                    }

                    addVnotch(belowSashCeptionPeice, true, mullion.ColId, mullion.RowId, mullion.TransomType);
                }
                else
                {
                    double belowDif = sashCeptionGetDifferenceMulTraIntoMulTra(frame, sashCeption, belowSashCeptionPeice.ProfileHeight, mullion.ProfileHeight);
                    bool tranRunThrough = (belowSashCeptionPeice.ColId < mullion.ColId && belowSashCeptionPeice.MaxCol() > mullion.ColId);

                    if (tranRunThrough)
                    {
                        addVnotch(belowSashCeptionPeice, true, mullion.ColId, mullion.RowId, mullion.TransomType);
                    }

                    length = lengthCalculation(length, belowDif);

                    mullion.OffsetBR = -belowDif;
                }

                mullion.Length = length;                
            });
        }

        private ACProfile sashCeptionGetSashLeft(ACFrame frame, ACSash sash, int? colId, int? rowId)
        {
            int row = Convert.ToInt32(rowId);

            if (colId == sash.ColId) //left peice is left of parent sash + means need frame aswell
            {
                return sash.SashLeft(frame);
            }
            else //left is sash mullion
            {
                return sash.LstInnerMullion().Where(w => w.ColId == colId
                            && w.LstRow().Contains(row)).FirstOrDefault();
            }
        }

        private ACProfile sashCeptionGetSashRight(ACFrame frame, ACSash sash, int? colId, int? rowId)
        {
            int row = Convert.ToInt32(rowId);

            if (colId == sash.MaxCol()) //right peice is right of parent sash + means need frame aswell
            {
                return sash.SashRight(frame);
            }
            else //right is sash mullion
            {
                return sash.LstInnerMullion().Where(w => w.ColId == colId
                            && w.LstRow().Contains(row)).FirstOrDefault();
            }
        }

        private ACProfile sashCeptionGetSashTop(ACFrame frame, ACSash sash, int? colId, int? rowId)
        {
            int col = Convert.ToInt32(rowId);

            if(rowId == sash.RowId) //top peice is top of parent sash + means need frame aswell
            {
                return sash.SashTop(frame);
            }
            else //top is sash transom
            {
                return sash.LstInnerTransom().Where(w => w.RowId == rowId
                            && w.LstCol().Contains(col)).FirstOrDefault();
            }
        }

        private ACProfile sashCeptionGetSashBottom(ACFrame frame, ACSash sash, int? colId, int? rowId)
        {
            int col = Convert.ToInt32(colId);

            if(rowId == sash.MaxRow()) //bottom peice is bottom of parent sash + means need frame aswell
            {
                return sash.SashBottom(frame);
            }
            else //bottom is sash transom
            {
                return sash.LstInnerTransom().Where(w => w.RowId == rowId
                            && w.LstCol().Contains(col)).FirstOrDefault();
            }
        }

        private ACProfile sashCeptionGetPeiceBesideTransom(ACFrame frame, ACSashCeption sashCeption, int? colId, int? rowId)
        {
            int row = Convert.ToInt32(rowId);

            List<ACProfile> lstBeside = sashCeption.LstProfile.Where(w => w.IsVert() &&
                                w.ColId == colId && w.LstRow().Contains(row)).ToList();

            if (lstBeside.Count == 0)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber}, SashCeption: {sashCeption.SashNumber} has a transom with no valid peice beside mullion/left/right"
                });
            }
            else if (lstBeside.Count > 2)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber}, SashCeption: {sashCeption.SashNumber} has a transom with too many valid peices beside (mullions)"
                });
            }

            //1||2 will both be same profile above so first is ok
            return lstBeside.FirstOrDefault();
        }

        private ACProfile sashCeptionGetPeiceBesideMullion(ACFrame frame, ACSashCeption sashCeption, int? colId, int? rowId)
        {
            int col = Convert.ToInt32(colId);

            List<ACProfile> lstBeside = sashCeption.LstProfile.Where(w => !w.IsVert() &&
                                w.RowId == rowId && w.LstCol().Contains(col)).ToList();

            if (lstBeside.Count == 0)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber}, SashCeption: {sashCeption.SashNumber} has a mullion with no valid peice beside transom/top/bottom"
                });
            }
            else if (lstBeside.Count > 2)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber}, SashCeption: {sashCeption.SashNumber} has a mullion with too many valid peices beside (transoms)"
                });
            }

            //1||2 will both be same profile above so first is ok
            return lstBeside.FirstOrDefault();
        }

        private double sashCeptionGetSashDifference(ACProfile sashOuterProfile)
        {
            double dif = sashOuterProfile.ProfileHeight
                    - sashOuterProfile.RebateInternal
                    - _settings.SashOverlap;

            return -dif;
        }

        private double sashCeptionGetSashMulTraDifference(ACProfile sashMulTraProfile)
        {
            double dif = (sashMulTraProfile.ProfileHeight / 2)
                    - sashMulTraProfile.RebateInternal
                    - _settings.SashOverlap;

            return -dif;
        }

        private double sashCeptionGetFrameDif(ACFrame frame, ACSashCeption sashCeption, ACProfile frameProfile)
        {
            double dif = sashGetFrameDifCalc(frameProfile);

            if (dif <= 0)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber}, SashCeption: {sashCeption.SashNumber} overlaps its outer/mullion/transom completely, this is not possible"
                });
            }

            return -dif;
        }

        private double sashCeptionGetDifferenceMulTraIntoMulTra(ACFrame frame, ACSashCeption sashCeption, double profileGoingIntoHeight, double inputMulTraHeight)
        {
            //can be + but not - on sash!!!
            //same mull/tra = 0
            double dif = (profileGoingIntoHeight / 2) - (inputMulTraHeight / 2);

            if (dif < 0)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber}, SashCeption: {sashCeption.SashNumber} has a larger mullion/transom going into a smaller mullion/transom, this is not possible"
                });
            }

            return -dif;
        }

        private double sashCeptionGetDifferenceMulTraOuter(ACFrame frame, ACSashCeption sashCeption, double profileOuterHeight, double mulTraHeight)
        {
            double dif = profileOuterHeight - (mulTraHeight / 2);

            if (dif <= 0)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame: {frame.FrameNumber}, SashCeption: {sashCeption.SashNumber} has a Mullion/Tra of profile Height {mulTraHeight} going into outer" +
                    $"of profile height {profileOuterHeight}, this is not possible"
                });
            }

            return -dif;
        }

        private void addVnotch(ACProfile profile, bool isTL, int? colId, int? rowId, EnTransomType? transomType)
        {
            if (profile.LstVNotch == null)
            {
                profile.LstVNotch = new List<ClsVNotch>();
            }

            EnVnotchType type = EnVnotchType.Small;

            switch(transomType)
            {
                case EnTransomType.Transom1:
                    type = EnVnotchType.Small;
                    break;
                case EnTransomType.Transom2:
                    type = EnVnotchType.Medium;
                    break;
                case EnTransomType.Transom3:
                    type = EnVnotchType.Large;
                    break;
            }

            profile.LstVNotch.Add(new ClsVNotch()
            {
                IsTL = isTL,
                ColId = Convert.ToInt32(colId),
                RowId = Convert.ToInt32(rowId),
                Type = type
            });
        }

    }
}
