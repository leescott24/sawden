﻿using Sawden.Application.Extensions.Input;
using Sawden.Application.Logic.Interfaces;
using Sawden.Domain.Input.Models;
using Sawden.Domain.Main.Models.Main;
using Sawden.Domain.Shared;
using Sawden.Persistance.FrameDesignerDBContext.Models;
using Sawden.Shared;
using Sawden.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sawden.Application.Logic
{
    public class PeiceSplitAndOrientationCalculation : IPeiceSplitAndOrientationCalculation
    {
        private IResponseData _response;

        private readonly ISawdenSettings _settings;

        private readonly FrameDesignerContext _frameDesignerDB;

        private ClsInputOrder _inputOrder;

        private List<string> invalidPcodes;

        public PeiceSplitAndOrientationCalculation(IResponseData response, ISawdenSettings settings, FrameDesignerContext frameDesignerDb)
        {
            _response = response;

            _settings = settings;            

            _frameDesignerDB = frameDesignerDb;            
        }

        public void Process(ClsInputOrder inputOrder)
        {
            try
            {
                _inputOrder = inputOrder;

                //cant be ctor as _settings is invalid before setup
                invalidPcodes = _settings.InvalidPcodes.Where(w => w.IsError).Select(s => s.PCode).ToList();

                processFrame();
            }
            catch (Exception ex)
            {
                BadResponseGenerator.Exception(_response, ex);
            }
        }        

        private void processFrame()
        {
            _inputOrder.LstFrame.ForEach(frame =>
            {
                processFrameProfile(frame);

                processSashProfile(frame);

                processSashCeptionProfile(frame);
            });
        }

        private List<ClsAperture> getFrameAperture(ClsInputFrame frame)
        {
            List<ClsAperture> lstAperture = new List<ClsAperture>();

            frameBlankLstAperture(frame, lstAperture);

            //sash have prority first
            frameNormalSashLstAperture(frame, lstAperture);

            //then compo next
            frameCompoLstAperture(frame, lstAperture);

            //then glass/panels last
            GlassLstAperture(frame, lstAperture);

            //all should be either internal/external by this stage! SHOULD NEVER HIT THIS as checks already in initial checks
            if (lstAperture.Where(w => w.ApertureType == EnApertureType.None).Count() > 0)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, does not have a complete set of Apertures!! (sash/glass/panel/compo)"
                });
            }

            return lstAperture;
        }

        private void frameBlankLstAperture(ClsInputFrame frame, List<ClsAperture> lstAperture)
        {
            frame.LstCol.ForEach(col =>
            {
                frame.LstRow.ForEach(row =>
                {
                    lstAperture.Add(new ClsAperture()
                    {
                        ColId = col.Id,
                        ColSpan = 1,
                        RowId = row.Id,
                        RowSpan = 1
                    });
                });
            });
        }

        private void frameNormalSashLstAperture(ClsInputFrame frame, List<ClsAperture> lstAperture)
        {
            frame.LstNormalSash().ForEach(sash =>
            {
                for (int x = Convert.ToInt32(sash.ColId); x < sash.MaxCol(); x++)
                {
                    for (int y = Convert.ToInt32(sash.RowId); y < sash.MaxRow(); y++)
                    {
                        lstAperture.Where(w => w.ColId == x && w.RowId == y).FirstOrDefault()
                            .ApertureType = Convert.ToBoolean(sash.OpenIn) ? EnApertureType.Internal : EnApertureType.External; 
                    }
                }
            });
        }

        private void frameCompoLstAperture(ClsInputFrame frame, List<ClsAperture> lstAperture)
        {
            frame.LstCompoSash().ForEach(compo =>
            {
                for (int x = Convert.ToInt32(compo.ColId); x < compo.MaxCol(); x++)
                {
                    for (int y = Convert.ToInt32(compo.RowId); y < compo.MaxRow(); y++)
                    {                        
                        lstAperture.Where(w => w.ColId == x && w.RowId == y).FirstOrDefault()
                            .ApertureType = Convert.ToBoolean(compo.OpenIn) ? EnApertureType.Internal : EnApertureType.External;
                    }
                }
            });
        }

        private void GlassLstAperture(ClsInputFrame frame, List<ClsAperture> lstAperture)
        {
            if (frame.LstGlass != null)
            {
                frame.LstGlass.ForEach(glass =>
                {
                    for (int x = Convert.ToInt32(glass.ColId); x < glass.MaxCol(); x++)
                    {
                        for (int y = Convert.ToInt32(glass.RowId); y < glass.MaxRow(); y++)
                        {
                            var apperture = lstAperture.Where(w => w.ColId == x && w.RowId == y).FirstOrDefault();

                            if (apperture != null) //check for when sash use
                            {
                                //check if this has already been covered by sash/compo, if not set apperture
                                if (apperture.ApertureType == EnApertureType.None)
                                {
                                    apperture.ApertureType = Convert.ToBoolean(glass.IsInternallyGlazed) ? EnApertureType.Internal : EnApertureType.External;
                                }
                            }
                        }
                    }

                });
            }
        }

        private void processFrameProfile(ClsInputFrame frame)
        {
            List<ClsAperture> lstAperture = getFrameAperture(frame);

            //mapping for mt & mz, large/small face && splitting
            checkProfileFaceDirectionEmpty(frame, frame.LstInnerMullion());
            frameMapMullions(frame, lstAperture);
            checkProfileFaceDirectionAndPcodeSet(frame, frame.LstInnerMullion());

            //mapping for mt & mz, large/small face && splitting
            checkProfileFaceDirectionEmpty(frame, frame.LstInnerTransom());
            frameMapTransoms(frame, lstAperture);
            checkProfileFaceDirectionAndPcodeSet(frame, frame.LstInnerTransom());

            //mapping for large/small face & splitting
            checkProfileFaceDirectionEmpty(frame, frame.LstOuter());
            frameMapOuter(frame, lstAperture);
            checkProfileFaceDirectionAndPcodeSet(frame, frame.LstOuter());
        }

        private void checkProfileFaceDirectionEmpty(ClsInputFrame frame, List<ClsInputProfile> lstProfile, ClsInputSash sash = null, ClsInputSashCeption sashCeption = null)
        {
            string sashStr = sash == null ? "" : "Sash: " + sash.SashNumber;
            string sashCeptionStr = sashCeption == null ? "" : "SashCeption: " + sashCeption.SashNumber;

            lstProfile.ForEach(profile =>
            {
                if (profile.LargeFaceDirection != EnLargeFaceDirection.None)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.SawdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, {sashStr} {sashCeption} has set EnLargeFaceDirection before required"
                    });
                }
            });
        }

        private void checkProfileFaceDirectionAndPcodeSet(ClsInputFrame frame, List<ClsInputProfile> lstProfile, ClsInputSash sash = null, ClsInputSashCeption sashCeption = null)
        {
            string sashStr = sash == null ? "" : "Sash: " + sash.SashNumber + ",";
            string sashCeptionStr = sashCeption == null ? "" : "SashCeption: " + sashCeption.SashNumber + ",";

            lstProfile.ForEach(profile =>
            {
                if (invalidPcodes.Contains(profile.Pcode))
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.SawdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, {sashStr} {sashCeption} has not set profile PCode and is required!!"
                    });
                }

                if (profile.LargeFaceDirection == EnLargeFaceDirection.None)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.SawdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, {sashStr} {sashCeption} has not set EnLargeFaceDirection and is required!!"
                    });
                }
            });
        }

        private void frameMapMullions(ClsInputFrame frame, List<ClsAperture> lstAperture)
        {
            List<ClsInputProfile> lstMullionsToRemove = new List<ClsInputProfile>();
            List<ClsInputProfile> lstNewMullions = new List<ClsInputProfile>();

            frame.LstInnerMullion().ForEach(mullion =>
            {
                int colIdRight = Convert.ToInt32(mullion.ColId);
                int colIdLeft = colIdRight - 1;

                EnApertureType rigthAperturePrevious = EnApertureType.None;
                EnApertureType leftAperturePrevious = EnApertureType.None;                

                int previousStartingRowId = Convert.ToInt32(mullion.RowId);                

                for (int y = Convert.ToInt32(mullion.RowId); y < mullion.MaxRow(); y++)
                {
                    ClsAperture right = lstAperture.Where(w => w.ColId == colIdRight && w.RowId == y).FirstOrDefault();
                    ClsAperture left = lstAperture.Where(w => w.ColId == colIdLeft && w.RowId == y).FirstOrDefault();

                    //first time only
                    if (rigthAperturePrevious == EnApertureType.None)
                    {
                        rigthAperturePrevious = right.ApertureType;
                    }

                    //first time only
                    if (leftAperturePrevious == EnApertureType.None)
                    {
                        leftAperturePrevious = left.ApertureType;
                    }                                     

                    //apperture has changed and needs new peice!!!
                    if (rigthAperturePrevious != right.ApertureType || leftAperturePrevious != left.ApertureType)
                    {
                        if (!lstMullionsToRemove.Contains(mullion))
                        {
                            lstMullionsToRemove.Add(mullion);
                        }

                        lstNewMullions.Add(mapProfileTransomMullion(frame,
                            mullion.ColId,
                            mullion.ColSpan,
                            previousStartingRowId,
                            (y - previousStartingRowId),
                            mullion.TransomType,
                            leftAperturePrevious,
                            rigthAperturePrevious));

                        //setup for next mullion
                        previousStartingRowId = y;
                        rigthAperturePrevious = right.ApertureType;
                        leftAperturePrevious = left.ApertureType;                        
                    }
                }

                //if mullion didnt get split
                if (!lstMullionsToRemove.Contains(mullion))
                {
                    //update pcode for it
                    mullion.Pcode = frameMapPCodeTransomMullion(frame, mullion.TransomType, leftAperturePrevious, rigthAperturePrevious);
                    mullion.LargeFaceDirection = mapLargeFaceDirectionTransomMullion(leftAperturePrevious, rigthAperturePrevious);
                }
                else
                {
                    //add last mullion that needs created
                    lstNewMullions.Add(mapProfileTransomMullion(frame,
                        mullion.ColId,
                        mullion.ColSpan,
                        previousStartingRowId,
                        (mullion.MaxRow() - previousStartingRowId),
                        mullion.TransomType,
                        leftAperturePrevious,
                        rigthAperturePrevious));
                }                
            });

            //add all new mullion if any
            lstNewMullions.ForEach(mullion =>
            {
                frame.LstProfile.Add(mullion);
            });

            //remove all old mullion if any
            lstMullionsToRemove.ForEach(mullion =>
            {
                frame.LstProfile.Remove(mullion);
            });
        }

        private void frameMapTransoms(ClsInputFrame frame, List<ClsAperture> lstAperture)
        {
            List<ClsInputProfile> lstTransomsToRemove = new List<ClsInputProfile>();
            List<ClsInputProfile> lstNewTransoms = new List<ClsInputProfile>();

            frame.LstInnerTransom().ForEach(transom =>
            {
                int rowIdBottom = Convert.ToInt32(transom.RowId);
                int rowIdTop = rowIdBottom - 1;

                EnApertureType bottomAperturePrevious = EnApertureType.None;
                EnApertureType topAperturePrevious = EnApertureType.None;

                int previousStartingColId = Convert.ToInt32(transom.ColId);

                for (int x = Convert.ToInt32(transom.ColId); x < transom.MaxCol(); x++)
                {
                    ClsAperture bottom = lstAperture.Where(w => w.RowId == rowIdBottom && w.ColId == x).FirstOrDefault();
                    ClsAperture top = lstAperture.Where(w => w.RowId == rowIdTop && w.ColId == x).FirstOrDefault();

                    //first time only
                    if (bottomAperturePrevious == EnApertureType.None)
                    {
                        bottomAperturePrevious = bottom.ApertureType;
                    }

                    //first time only
                    if (topAperturePrevious == EnApertureType.None)
                    {
                        topAperturePrevious = top.ApertureType;
                    }

                    //apperture has changed and needs new peice!!!
                    if (bottomAperturePrevious != bottom.ApertureType || topAperturePrevious != top.ApertureType)
                    {
                        if (!lstTransomsToRemove.Contains(transom))
                        {
                            lstTransomsToRemove.Add(transom);
                        }

                        lstNewTransoms.Add(mapProfileTransomMullion(frame,
                            previousStartingColId,
                            (x - previousStartingColId),
                            transom.RowId,
                            transom.RowSpan,
                            transom.TransomType,
                            topAperturePrevious,
                            bottomAperturePrevious));

                        //setup for next transom
                        previousStartingColId = x;
                        topAperturePrevious = top.ApertureType;
                        bottomAperturePrevious = bottom.ApertureType;
                    }
                }

                //if transom didnt get split 
                if(!lstTransomsToRemove.Contains(transom))
                {
                    //update pcode for it
                    transom.Pcode = frameMapPCodeTransomMullion(frame, transom.TransomType, topAperturePrevious, bottomAperturePrevious);
                    transom.LargeFaceDirection = mapLargeFaceDirectionTransomMullion(topAperturePrevious, bottomAperturePrevious);
                }
                else
                {
                    //add last transom that needs created
                    lstNewTransoms.Add(mapProfileTransomMullion(frame,
                        previousStartingColId,
                        (transom.MaxCol() - previousStartingColId),
                        transom.RowId,
                        transom.RowSpan,
                        transom.TransomType,
                        topAperturePrevious,
                        bottomAperturePrevious));
                }

            });

            //add all new transoms if any
            lstNewTransoms.ForEach(transom =>
            {
                frame.LstProfile.Add(transom);
            });

            //remove all old transoms if any
            lstTransomsToRemove.ForEach(transom =>
            {
                frame.LstProfile.Remove(transom);
            });
        }

        private ClsInputProfile mapProfileTransomMullion(ClsInputFrame frame, int? colId, int? colSpan, int? rowId, int? rowSpan, EnTransomType? transomType, EnApertureType TopOrLeft, EnApertureType BottomOrRight)
        {
            return new ClsInputProfile()
            {
                ColId = colId,
                ColSpan = colSpan,
                RowId = rowId,
                RowSpan = rowSpan,
                TransomType = transomType,
                Pcode = frameMapPCodeTransomMullion(frame, transomType, TopOrLeft, BottomOrRight),
                LargeFaceDirection = mapLargeFaceDirectionTransomMullion(TopOrLeft, BottomOrRight)
            };
        }

        private string frameMapPCodeTransomMullion(ClsInputFrame frame, EnTransomType? transomType,  EnApertureType TopOrLeft, EnApertureType BottomOrRight)
        {
            string pcode = "";

            if (frame.is60mm())
            {
                if (transomType != EnTransomType.Transom1 && transomType != EnTransomType.Dummy)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.SawdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, cannot have a mullion/transom which isnt EnTransomType.Transom1 or Dummy"
                    });
                }

                if (TopOrLeft != BottomOrRight) //not equal means z profile
                {
                    pcode = _frameDesignerDB.TransomsAndMullions.FirstOrDefault(fd => fd.Width == 60 &&
                                fd.Pcode.Contains("MZ") && fd.TransomType == Convert.ToInt32(transomType)).Pcode;
                }
                else if (transomType != EnTransomType.Dummy) // equal means t profile
                {
                    pcode = _frameDesignerDB.TransomsAndMullions.FirstOrDefault(fd => fd.Width == 60 &&
                                fd.Pcode.Contains("MT") && fd.TransomType == Convert.ToInt32(transomType)).Pcode;
                }
                else // dummy mullion
                {
                    pcode = _frameDesignerDB.TransomsAndMullions.FirstOrDefault(fd => fd.Width == 60 &&
                                fd.Pcode.Contains("DM") && fd.TransomType == Convert.ToInt32(transomType)).Pcode;
                }
            }

            if(frame.is70mm())
            {
                if (TopOrLeft != BottomOrRight) //not equal means z profile
                {
                    pcode = _frameDesignerDB.TransomsAndMullions.FirstOrDefault(fd => fd.Width == 70 &&
                                fd.Pcode.Contains("MZ") && fd.TransomType == Convert.ToInt32(transomType)).Pcode;
                }
                else if (transomType != EnTransomType.Dummy) // equal means t profile
                {
                    pcode = _frameDesignerDB.TransomsAndMullions.FirstOrDefault(fd => fd.Width == 70 &&
                                fd.Pcode.Contains("MT") && fd.TransomType == Convert.ToInt32(transomType)).Pcode;
                }
                else
                {
                    pcode = _frameDesignerDB.TransomsAndMullions.FirstOrDefault(fd => fd.Width == 70 &&
                                fd.Pcode.Contains("DM") && fd.TransomType == Convert.ToInt32(transomType)).Pcode;
                }
            }

            return pcode;
        }

        private EnLargeFaceDirection mapLargeFaceDirectionTransomMullion(EnApertureType TopOrLeft, EnApertureType BottomOrRight)
        {
            var face = EnLargeFaceDirection.None;

            if (TopOrLeft == EnApertureType.Internal && BottomOrRight == EnApertureType.Internal) //t profile
            {
                face = EnLargeFaceDirection.Outside;
            }
            
            if (TopOrLeft == EnApertureType.External && BottomOrRight == EnApertureType.External) //t profile
            {
                face = EnLargeFaceDirection.Inside;
            }

            if (TopOrLeft == EnApertureType.Internal && BottomOrRight == EnApertureType.External) //z profile
            {
                face = EnLargeFaceDirection.Outside;
            }

            if (TopOrLeft == EnApertureType.External && BottomOrRight == EnApertureType.Internal) //z profile
            {
                face = EnLargeFaceDirection.Inside;
            }

            return face;
        }

        private void frameMapOuter(ClsInputFrame frame, List<ClsAperture> lstAperture)
        {
            frameMapTop(frame, lstAperture);

            frameMapBottom(frame, lstAperture);

            frameMapLeft(frame, lstAperture);

            frameMapRight(frame, lstAperture);
        }

        private void frameMapTop(ClsInputFrame frame, List<ClsAperture> lstAperture)
        {
            var top = frame.LstOuterTop().FirstOrDefault();

            var lstNewTop = new List<ClsInputProfile>();

            bool orignalTopRemoved = false;

            int rowIdBottom = 0;

            EnApertureType bottomAperturePrevious = EnApertureType.None;

            int previousStartingColId = 0;

            for (int x = 0; x < frame.LstCol.Count; x++)
            {
                ClsAperture bottom = lstAperture.Where(w => w.RowId == rowIdBottom && w.ColId == x).FirstOrDefault();

                //first time only
                if (bottomAperturePrevious == EnApertureType.None)
                {
                    bottomAperturePrevious = bottom.ApertureType;
                }

                //apperture has changed and needs new part
                if (bottomAperturePrevious != bottom.ApertureType)
                {
                    orignalTopRemoved = true;

                    lstNewTop.Add(mapProfileOuter(frame,
                        top.Pcode,
                        previousStartingColId,
                        (x - previousStartingColId),
                        0,
                        0,
                        top.TransomType,
                        bottomAperturePrevious
                        ));

                    //setup for next top peice
                    previousStartingColId = x;
                    bottomAperturePrevious = bottom.ApertureType;
                }
            }

            //if top didnt get split
            if(!orignalTopRemoved)
            {
                //update peice
                top.Pcode = mapPCodeOuter(frame, top.Pcode, top.TransomType, bottomAperturePrevious);
                top.LargeFaceDirection = mapLargeFaceDirectionOuter(frame, bottomAperturePrevious, top.TransomType, top.Pcode);
            }
            else
            {
                //remove old peice
                frame.LstProfile.Remove(top);

                //add remaining peice
                lstNewTop.Add(mapProfileOuter(frame,
                        top.Pcode,
                        previousStartingColId,
                        (top.MaxCol() - previousStartingColId),
                        0,
                        0,
                        top.TransomType,
                        bottomAperturePrevious
                        ));

                //add all new peices
                lstNewTop.ForEach(profile =>
                {
                    frame.LstProfile.Add(profile);
                });
            }
        }

        private void frameMapBottom(ClsInputFrame frame, List<ClsAperture> lstAperture)
        {
            var bottom = frame.LstOuterBottom().FirstOrDefault();

            var lstNewBottom = new List<ClsInputProfile>();

            bool orignalBotRemoved = false;

            int maxRow = frame.LstRow.Count;
            int rowIdTop = maxRow - 1;

            EnApertureType topAperturePrevious = EnApertureType.None;

            int previousStartingCol = 0;

            for(int x = 0; x < frame.LstCol.Count; x++)
            {
                ClsAperture top = lstAperture.Where(w => w.RowId == rowIdTop && w.ColId == x).FirstOrDefault();

                //first time only
                if (topAperturePrevious == EnApertureType.None)
                {
                    topAperturePrevious = top.ApertureType;
                }

                //apperture has changed and needs new part
                if (topAperturePrevious != top.ApertureType)
                {
                    orignalBotRemoved = true;

                    lstNewBottom.Add(mapProfileOuter(frame,
                        bottom.Pcode,
                        previousStartingCol,
                        (x - previousStartingCol),
                        maxRow,
                        0,
                        bottom.TransomType,
                        topAperturePrevious
                        ));

                    //setup for next bottom peice
                    previousStartingCol = x;
                    topAperturePrevious = top.ApertureType;
                }
            }

            //if bottom didnt get split
            if(!orignalBotRemoved)
            {
                //update peice
                bottom.Pcode = mapPCodeOuter(frame, bottom.Pcode, bottom.TransomType, topAperturePrevious);
                bottom.LargeFaceDirection = mapLargeFaceDirectionOuter(frame, topAperturePrevious, bottom.TransomType, bottom.Pcode);
            }
            else
            {
                //remove old peice
                frame.LstProfile.Remove(bottom);

                //add remaining peice
                lstNewBottom.Add(mapProfileOuter(frame,
                        bottom.Pcode,
                        previousStartingCol,
                        (bottom.MaxCol() - previousStartingCol),
                        maxRow,
                        0,
                        bottom.TransomType,
                        topAperturePrevious
                        ));

                //add all new peices
                lstNewBottom.ForEach(profile =>
                {
                    frame.LstProfile.Add(profile);
                });
            }
        }

        private void frameMapLeft(ClsInputFrame frame, List<ClsAperture> lstAperture)
        {
            var left = frame.LstOuterLeft().FirstOrDefault();

            var lstNewLeft = new List<ClsInputProfile>();

            bool orignalLeftRemoved = false;

            int colIdRight = 0;

            EnApertureType rightAperturePrevious = EnApertureType.None;

            int previousStartingRowId = 0;

            for(int y = 0; y < frame.LstRow.Count; y++)
            {
                ClsAperture right = lstAperture.Where(w => w.ColId == colIdRight && w.RowId == y).FirstOrDefault();

                //first time only
                if (rightAperturePrevious == EnApertureType.None)
                {
                    rightAperturePrevious = right.ApertureType;
                }

                //aperture has changed and needs new part
                if (rightAperturePrevious != right.ApertureType)
                {
                    orignalLeftRemoved = true;

                    lstNewLeft.Add(mapProfileOuter(frame,
                        left.Pcode,
                        0,
                        0,
                        previousStartingRowId,
                        (y - previousStartingRowId),
                        left.TransomType,
                        rightAperturePrevious
                        ));

                    //setup for next left
                    previousStartingRowId = y;
                    rightAperturePrevious = right.ApertureType;
                }
            }

            //if left didnt get split
            if (!orignalLeftRemoved)
            {
                //update peice 
                left.Pcode = mapPCodeOuter(frame, left.Pcode, left.TransomType, rightAperturePrevious);
                left.LargeFaceDirection = mapLargeFaceDirectionOuter(frame, rightAperturePrevious, left.TransomType, left.Pcode);
            }
            else
            {
                //remove old peice
                frame.LstProfile.Remove(left);

                //add remaining peice
                lstNewLeft.Add(mapProfileOuter(frame,
                        left.Pcode,
                        0,
                        0,
                        previousStartingRowId,
                        (left.MaxRow() - previousStartingRowId),
                        left.TransomType,
                        rightAperturePrevious
                        ));

                //add all new peices
                lstNewLeft.ForEach(profile =>
                {
                    frame.LstProfile.Add(profile);
                });
            }
        }

        private void frameMapRight(ClsInputFrame frame, List<ClsAperture> lstAperture)
        {
            var right = frame.LstOuterRight().FirstOrDefault();

            var lstNewRight = new List<ClsInputProfile>();

            bool orignalRightRemoved = false;

            int colMax = frame.LstCol.Count();
            int colIdLeft = colMax - 1;

            EnApertureType leftAperturePrevious = EnApertureType.None;

            int previousStartingRowId = 0;

            for (int y = 0; y < frame.LstRow.Count; y++)
            {
                ClsAperture left = lstAperture.Where(w => w.ColId == colIdLeft && w.RowId == y).FirstOrDefault();

                //first time only
                if (leftAperturePrevious == EnApertureType.None)
                {
                    leftAperturePrevious = left.ApertureType;
                }

                //aperture has changed and needs new parts
                if (leftAperturePrevious != left.ApertureType)
                {
                    orignalRightRemoved = true;

                    lstNewRight.Add(mapProfileOuter(frame,
                        right.Pcode,
                        colMax,
                        0,
                        previousStartingRowId,
                        (y - previousStartingRowId),
                        right.TransomType,
                        leftAperturePrevious
                        ));

                    //setup for next right
                    previousStartingRowId = y;
                    leftAperturePrevious = left.ApertureType;
                }
            }

            //if right didnt get split
            if(!orignalRightRemoved)
            {
                //update peice
                right.Pcode = mapPCodeOuter(frame, right.Pcode, right.TransomType, leftAperturePrevious);
                right.LargeFaceDirection = mapLargeFaceDirectionOuter(frame, leftAperturePrevious, right.TransomType, right.Pcode);
            }
            else
            {
                //remove old peice
                frame.LstProfile.Remove(right);

                //add remaining peice
                lstNewRight.Add(mapProfileOuter(frame,
                        right.Pcode,
                        colMax,
                        0,
                        previousStartingRowId,
                        (right.MaxRow() - previousStartingRowId),
                        right.TransomType,
                        leftAperturePrevious
                        ));

                //add all new peices
                lstNewRight.ForEach(profile =>
                {
                    frame.LstProfile.Add(profile);
                });
            }
        }

        private ClsInputProfile mapProfileOuter(ClsInputFrame frame, string Pcode, int? colId, int? colSpan, int? rowId, int? rowSpan, EnTransomType? transomType, EnApertureType apertureType)
        {
            var profile = new ClsInputProfile()
            {
                ColId = colId,
                ColSpan = colSpan,
                RowId = rowId,
                RowSpan = rowSpan,
                TransomType = transomType,
                Pcode = mapPCodeOuter(frame, Pcode, transomType, apertureType)
            };

            profile.LargeFaceDirection = mapLargeFaceDirectionOuter(frame, apertureType, transomType, profile.Pcode);

            return profile;
        }

        private string mapPCodeOuter(ClsInputFrame frame, string Pcode, EnTransomType? transomType, EnApertureType apertureType)
        {
            var pcode = "";

            if (transomType == EnTransomType.NonTransom)
            {
                //should never hit already checks
                if(invalidPcodes.Contains(Pcode))
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.SawdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, cannot have outer of type EnTransomType.NonTransom, with invalid PCode"
                    });
                }

                pcode = Pcode;
            }
            else if (transomType != EnTransomType.NonTransom && transomType != EnTransomType.Dummy)
            {
                string transomM = apertureType == EnApertureType.Internal ? "MT" : "MZ";
                string transomW = "";

                if (transomType == EnTransomType.Transom1)
                {
                    transomW = "70";
                }
                else if (transomType == EnTransomType.Transom2)
                {
                    transomW = "88";
                }
                else if (transomType == EnTransomType.Transom3)
                {
                    transomW = "108";
                }

                pcode = _frameDesignerDB.OuterFrames.FirstOrDefault(fd => fd.Pcode.Contains(transomW) &&
                            fd.Pcode.Contains(transomM) && fd.IsOddLeg).Pcode;
            }
            else
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, cannot have outer of type EnTransomType.Dummy"
                });
            }

            return pcode;
        }

        private EnLargeFaceDirection mapLargeFaceDirectionOuter(ClsInputFrame frame, EnApertureType apertureType, EnTransomType? transomType, string Pcode)
        {
            var largeFaceDirection = EnLargeFaceDirection.None;

            if (transomType == EnTransomType.NonTransom)
            {
                if (apertureType == EnApertureType.Internal)
                {
                    largeFaceDirection = EnLargeFaceDirection.Outside;
                }
                else
                {
                    largeFaceDirection = EnLargeFaceDirection.Inside;
                }
            }
            else if (transomType != EnTransomType.NonTransom && transomType != EnTransomType.Dummy)
            {
                //correct outcome
                if (Pcode.Contains("MZ") && apertureType == EnApertureType.External)
                {
                    largeFaceDirection = EnLargeFaceDirection.Outside;
                }

                //incorrect outcome!
                if (Pcode.Contains("MZ") && apertureType == EnApertureType.Internal)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.SawdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, cannot have outer oddleg Z beside internal aperture"
                    });
                }

                //correct outcome
                if (Pcode.Contains("MT") && apertureType == EnApertureType.Internal)
                {
                    largeFaceDirection = EnLargeFaceDirection.Outside;
                }

                //incorrect outcome!
                if (Pcode.Contains("MT") && apertureType == EnApertureType.External)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.SawdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, cannot have outer oddleg T beside external aperture"
                    });
                }
            }
            else
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, cannot have outer of type EnTransomType.Dummy"
                });
            }

            return largeFaceDirection;
        }

        private void processSashProfile(ClsInputFrame frame)
        {
            frame.LstNormalSash().ForEach(sash =>
            {
                List<ClsAperture> lstAperture = getSashAperture(frame, sash);

                //all same sash profile will have same large/small face
                // && all sash must have T mullion/transoms!
                checkProfileFaceDirectionEmpty(frame, sash.LstProfile, sash);
                sashMapTransoms(frame, sash);
                sashMapMullions(frame, sash);
                sashMapOuter(frame, sash);
                checkProfileFaceDirectionAndPcodeSet(frame, sash.LstProfile, sash);
            });
        }

        private List<ClsAperture> getSashAperture(ClsInputFrame frame, ClsInputSash sash)
        {
            List<ClsAperture> lstAperture = new List<ClsAperture>();

            sashBlankLstAperture(sash, lstAperture);

            //sashception have priority first
            sashSashCeptionLstAperture(sash, lstAperture);

            //then glass & panels ..... COMPO WILL/CAN NOT BE SASHCEPTION!
            GlassLstAperture(frame, lstAperture);

            //all should be either internal/external by this stage! SHOULD NEVER HIT THIS as checks already in initial checks
            if (lstAperture.Where(w => w.ApertureType == EnApertureType.None).Count() > 0)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, does not have a complete set of Apertures!! (sash/glass/panel/compo)"
                });
            }

            //one sash should always have all appertures equal!!!!
            if(lstAperture.Select(s => s.ApertureType).Distinct().Count() > 1)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber} has a mixed set of Apertures internal/external!! This is not possible (sash/glass/panel)"
                });
            }

            //all sash should be glazed internally except sashception!
            if(lstAperture.FirstOrDefault().ApertureType != EnApertureType.Internal)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber} has external appertures!! This is not possible (sash/glass/panel)"
                });
            }

            return lstAperture;
        }

        private void sashBlankLstAperture(ClsInputSash sash, List<ClsAperture> lstAperture)
        {
            var colId = Convert.ToInt32(sash.ColId);
            var colSpan = Convert.ToInt32(sash.ColSpan);
            var rowId = Convert.ToInt32(sash.RowId);
            var rowSpan = Convert.ToInt32(sash.RowSpan);

            for (int x = colId; x < (colId + colSpan); x++)
            {
                for (int y = rowId; y < (rowId + rowSpan); y++)
                {
                    lstAperture.Add(new ClsAperture()
                    {
                        ColId = x,
                        ColSpan = 1,
                        RowId = y,
                        RowSpan = 1
                    });
                }
            }
        }

        private void sashSashCeptionLstAperture(ClsInputSash sash, List<ClsAperture> lstAperture)
        {
            sash.GetSashCeption().ForEach(sashCeption =>
            {
                var colId = Convert.ToInt32(sashCeption.ColId);
                var colSpan = Convert.ToInt32(sashCeption.ColSpan);
                var rowId = Convert.ToInt32(sashCeption.RowId);
                var rowSpan = Convert.ToInt32(sashCeption.RowSpan);

                for (int x = colId; x < (colId + colSpan); x++)
                {
                    for (int y = rowId; y < (rowId + rowSpan); y++)
                    {
                        lstAperture.Where(w => w.ColId == x && w.RowId == y).FirstOrDefault()
                        .ApertureType = Convert.ToBoolean(sashCeption.OpenIn) ? EnApertureType.Internal : EnApertureType.External;
                    }
                }
            });
        }

        private void sashMapTransoms(ClsInputFrame frame, ClsInputSash sash)
        {
            sash.LstInnerTransom().ForEach(transom =>
            {
                transom.Pcode = sashMapPCodeTransomMullion(frame, transom, sash);
                transom.LargeFaceDirection = EnLargeFaceDirection.Outside;
            });
        }

        private void sashMapMullions(ClsInputFrame frame, ClsInputSash sash)
        {
            sash.LstInnerMullion().ForEach(mullion =>
            {
                mullion.Pcode = sashMapPCodeTransomMullion(frame, mullion, sash);
                mullion.LargeFaceDirection = EnLargeFaceDirection.Outside;
            });
        }

        private string sashMapPCodeTransomMullion(ClsInputFrame frame, ClsInputProfile profile , ClsInputSash sash = null, ClsInputSashCeption sashCeption = null)
        {
            string sashStr = sash == null ? "" : "Sash: " + sash.SashNumber;
            string sashCeptionStr = sashCeption == null ? "" : "SashCeption: " + sashCeption.SashNumber;

            string pcode = "";

            if (frame.is60mm())
            {
                if(profile.TransomType != EnTransomType.Transom1)
                {
                    BadResponseGenerator.Add(_response, note: new ResponseNote()
                    {
                        NoteType = EnResponseNote.SawdenError,
                        NoteMessage = $"Frame {frame.FrameNumber}, {sashStr} {sashCeptionStr} cannot have a mullion/transom which isnt EnTransomType.Transom1"
                    });
                }

                pcode = _frameDesignerDB.TransomsAndMullions.FirstOrDefault(fd => fd.Pcode.Contains("60") &&
                            fd.Pcode.Contains("MT") && fd.TransomType == Convert.ToInt32(profile.TransomType)).Pcode;
            }

            if (frame.is70mm())
            {
                pcode = _frameDesignerDB.TransomsAndMullions.FirstOrDefault(fd => fd.Pcode.Contains("70") &&
                            fd.Pcode.Contains("MT") && fd.TransomType == Convert.ToInt32(profile.TransomType)).Pcode;
            }

            return pcode;
        }

        private void sashMapOuter(ClsInputFrame frame, ClsInputSash sash)
        {
            sash.SashOuter(frame).ForEach(outer =>
            {
                outer.Pcode = sashMapPCodeOuter(frame, outer);
                outer.LargeFaceDirection = EnLargeFaceDirection.Outside;
            });
        }

        private string sashMapPCodeOuter(ClsInputFrame frame, ClsInputProfile profile)
        {
            string pcode = "";

            pcode = profile.Pcode;

            return pcode;
        }

        private void processSashCeptionProfile(ClsInputFrame frame)
        {
            frame.LstNormalSash().ForEach(sash =>
            {
                sash.GetSashCeption().ForEach(sashCeption =>
                {
                    List<ClsAperture> lstAperture = getSashCeptionAperture(frame, sashCeption);

                    //all same sash profile will have same large/small face
                    // && all sash must have T mullion/transoms!
                    checkProfileFaceDirectionEmpty(frame, sashCeption.LstProfile, sashCeption: sashCeption);
                    sashCeptionMapTransoms(frame, sashCeption);
                    sashCeptionMapMullion(frame, sashCeption);
                    sashCeptionMapOuter(frame, sashCeption);
                    checkProfileFaceDirectionAndPcodeSet(frame, sashCeption.LstProfile, sashCeption: sashCeption);
                });
            });
        }

        private List<ClsAperture> getSashCeptionAperture(ClsInputFrame frame, ClsInputSashCeption sashCeption)
        {
            List<ClsAperture> lstAperture = new List<ClsAperture>();

            sashCeptionBlankLstAperture(sashCeption, lstAperture);

            //only glass & panels .... NO SASH/COMPO IN SASHCEPTION
            GlassLstAperture(frame, lstAperture);

            //all should be either internal/external by this stage! SHOULD NEVER HIT THIS as checks already in initial checks
            if (lstAperture.Where(w => w.ApertureType == EnApertureType.None).Count() > 0)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, SashCeption {sashCeption.SashNumber} does not have a complete set of Apertures!! (sash/glass/panel/compo)"
                });
            }

            //one sashception should always have all appertures equal!!!!
            if (lstAperture.Select(s => s.ApertureType).Distinct().Count() > 1)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, SashCeption {sashCeption.SashNumber} has a mixed set of Apertures internal/external!! This is not possible (sash/glass/panel)"
                });
            }

            //all sashception should be glazed externally except sashception!
            if (lstAperture.FirstOrDefault().ApertureType == EnApertureType.Internal)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, SashCeption {sashCeption.SashNumber} has internal appertures!! This is not possible (sash/glass/panel)"
                });
            }

            return lstAperture;
        }

        private void sashCeptionBlankLstAperture(ClsInputSashCeption sashCeption, List<ClsAperture> lstAperture)
        {
            var colId = Convert.ToInt32(sashCeption.ColId);
            var colSpan = Convert.ToInt32(sashCeption.ColSpan);
            var rowId = Convert.ToInt32(sashCeption.RowId);
            var rowSpan = Convert.ToInt32(sashCeption.RowSpan);

            for (int x = colId; x < (colId + colSpan); x++)
            {
                for (int y = rowId; y < (rowId + rowSpan); y++)
                {
                    lstAperture.Add(new ClsAperture()
                    {
                        ColId = x,
                        ColSpan = 1,
                        RowId = y,
                        RowSpan = 1
                    });
                }
            }
        }

        private void sashCeptionMapTransoms(ClsInputFrame frame, ClsInputSashCeption sashCeption)
        {
            sashCeption.LstInnerTransom().ForEach(transom =>
            {
                transom.Pcode = sashMapPCodeTransomMullion(frame, transom, sashCeption: sashCeption);
                transom.LargeFaceDirection = EnLargeFaceDirection.Inside;
            });
        }

        private void sashCeptionMapMullion(ClsInputFrame frame, ClsInputSashCeption sashCeption)
        {
            sashCeption.LstInnerMullion().ForEach(mullion =>
            {
                mullion.Pcode = sashMapPCodeTransomMullion(frame, mullion, sashCeption: sashCeption);
                mullion.LargeFaceDirection = EnLargeFaceDirection.Inside;
            });
        }

        private void sashCeptionMapOuter(ClsInputFrame frame, ClsInputSashCeption sashCeption)
        {
            sashCeption.SashOuter(frame).ForEach(outer =>
            {
                outer.Pcode = sashMapPCodeOuter(frame, outer);
                outer.LargeFaceDirection = EnLargeFaceDirection.Inside;
            });
        }
    }
}
