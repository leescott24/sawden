﻿using Sawden.Domain.Main.Models.Main;

namespace Sawden.Application.Logic.Interfaces
{
    public interface IProfileCalculation
    {
        void Process(ClsOrder order);
    }
}
