﻿using Sawden.Domain.Input.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Application.Logic.Interfaces
{
    public interface IPeiceSplitAndOrientationCalculation
    {
        void Process(ClsInputOrder inputOrder);
    }
}
