﻿using Sawden.Domain.Main.Models.Main;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Application.Logic.Interfaces
{
    public interface IMechanicalJointCalculation
    {
        void Process(ClsOrder order);
    }
}
