﻿using Sawden.Domain.Input.Models;
using Sawden.Domain.Main.Models.Main;

namespace Sawden.Application.Logic.Interfaces
{
    public interface IMulTraSplitAndCrucifixCalculation
    {
        void Process(ClsInputOrder inputOrder);
    }
}
