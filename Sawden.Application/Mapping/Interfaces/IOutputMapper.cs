﻿using Sawden.Domain.Main.Models.Main;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Application.Mapping.Interfaces
{
    public interface IOutputMapper
    {
        void Map(ClsOrder inputOrder);
    }
}
