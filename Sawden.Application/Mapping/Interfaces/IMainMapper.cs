﻿using Sawden.Domain.Input.Models;

namespace Sawden.Application.Mapping.Interfaces
{
    public interface IMainMapper
    {
        void Map(ClsInputOrder order);
    }
}
