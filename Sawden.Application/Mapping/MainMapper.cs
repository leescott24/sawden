﻿using AutoMapper;
using Sawden.Application.Extensions.Input;
using Sawden.Application.Extensions.Main;
using Sawden.Application.Mapping.Interfaces;
using Sawden.Domain.Input.Models;
using Sawden.Domain.Main.Models.Abstract;
using Sawden.Domain.Main.Models.Main;
using Sawden.Domain.Main.Models.Main.Frame;
using Sawden.Domain.Main.Models.Main.Profile;
using Sawden.Domain.Main.Models.Main.Sash;
using Sawden.Domain.Main.Models.Main.Sashception;
using Sawden.Domain.Shared;
using Sawden.Persistance.FrameDesignerDBContext.Models;
using Sawden.Shared;
using Sawden.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sawden.Application.Mapping
{
    public class MainMapper : IMainMapper
    {
        private IResponseData _response;

        private readonly IMapper _mapper;

        private readonly FrameDesignerContext _frameDesignerDB;

        public MainMapper(IResponseData response, IMapper mapper, FrameDesignerContext frameDesignerDb)
        {
            _response = response;

            _mapper = mapper;

            _frameDesignerDB = frameDesignerDb;
        }

        public void Map(ClsInputOrder inputOrder)
        {
            try
            {
                ClsOrder order = new ClsOrder()
                {
                    OrderNumber = inputOrder.OrderNumber
                };

                order.LstFrame = new List<ACFrame>();

                inputOrder.LstFrame.ForEach(inputFrame =>
                {
                    var frame = mapFrame(inputFrame);

                    if (frame != null)
                    {
                        order.LstFrame.Add(frame);
                    }
                });

                _response.Data = order;
            }
            catch (Exception ex)
            {
                BadResponseGenerator.Exception(_response, ex);
            }
        }

        private ACFrame mapFrame(ClsInputFrame inputFrame)
        {
            ACFrame frame = mapFrameSystemType(inputFrame);          

            //should never be null as initial checks prevent any unsupported system types getting this far
            if (frame != null)
            {
                frame.FrameNumber = inputFrame.FrameNumber;
                frame.TotalWidth = inputFrame.TotalWidth;
                frame.TotalHeight = inputFrame.TotalHeight;
                frame.FrameWidth = inputFrame.FrameWidth;
                frame.FrameHeight = inputFrame.FrameHeight;
                frame.MasterProductTypeId = inputFrame.MasterProductTypeId;
                frame.SystemTypeId = inputFrame.SystemTypeId;
                frame.DrainageTypeId = inputFrame.DrainageTypeId;
                frame.IsCrucifix = inputFrame.IsCrucifix;

                frame.Colour = _mapper.Map<ClsInputColour, ClsColour>(inputFrame.Colour);
                frame.Cill = _mapper.Map<ClsInputCill, ClsCill>(inputFrame.Cill);

                frame.LstRow = _mapper.Map<List<ClsInputSplit>, List<ClsSplit>>(inputFrame.LstRow);
                frame.LstCol = _mapper.Map<List<ClsInputSplit>, List<ClsSplit>>(inputFrame.LstCol);

                frame.LstProfile = mapFrameProfile(inputFrame);                            

                frame.LstSash = new List<ACSash>();
                if(inputFrame.LstSash != null)
                {
                    inputFrame.LstSash.ForEach(inputSash =>
                    {
                        var sash = mapSash(inputFrame, inputSash);

                        if (sash != null)
                        {
                            frame.LstSash.Add(sash);
                        }
                    });
                }

                frame.LstGlass = _mapper.Map<List<ClsInputGlass>, List<ClsGlass>>(inputFrame.LstGlass);

                //TODO Need added mapping
                frame.LstExtra = new List<ACProduct>();
                //frame.LstExtra = _mapper.Map<List<ClsInputProduct>, List<ClsProduct>>(inputFrame.LstExtra);
                frame.LstFrameAccessory = new List<ACFrameAccessory>();
                //frame.LstFrameAccessory = _mapper.Map<List<ClsInputFrameAccessory>, List<ClsFrameAccessory>>(inputFrame.LstFrameAccessory);

                return frame;
            }
            else
            {


                return null;
            }
        }

        private ACFrame mapFrameSystemType(ClsInputFrame inputFrame)
        {
            ACFrame frame = null;

            var systemTypeId = Convert.ToInt32(inputFrame.SystemTypeId);

            //60mm & 70mm windows & t&t's & sidelights
            var lstClsFrameWindow = new List<int>() { 1, 2, 13, 18 };

            //mini french
            var lstClsFrameMiniFrench = new List<int>() { 4 };

            //resi doors
            var lstClsFrameResiDoor = new List<int>() { 8 };

            //french doors
            var lstClsFrameFrenchDoor = new List<int>() { 9 };

            //stable doors
            var lstClsFrameStableDoor = new List<int>() { 12 };

            //comp doors
            var lstClsFrameComposite = new List<int>() { 14, 15 };


            if (lstClsFrameWindow.Contains(systemTypeId))
            {
                frame = new ClsFrameWindow();
            }

            if (lstClsFrameMiniFrench.Contains(systemTypeId))
            {
                frame = new ClsFrameMiniFrench();
            }

            if (lstClsFrameResiDoor.Contains(systemTypeId))
            {
                frame = new ClsFrameResiDoor();
            }

            if (lstClsFrameFrenchDoor.Contains(systemTypeId))
            {
                frame = new ClsFrameFrenchDoor();
            }

            if (lstClsFrameStableDoor.Contains(systemTypeId))
            {
                frame = new ClsFrameStableDoor();
            }

            if (lstClsFrameComposite.Contains(systemTypeId))
            {
                frame = new ClsFrameComposite();
            }

            if (frame == null)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame {inputFrame.FrameNumber}, Has a Non implemented Systemtype: {systemTypeId}"
                });
            }

            return frame;
        }

        private ACSash mapSashSashType(ClsInputFrame inputFrame, ClsInputSash inputSash)
        {
            ACSash sash = null;

            var sashType = Convert.ToInt32(inputSash.SashTypeId);

            var lstClsSashWindow = new List<int>() { 1, 2, 3, 16 };

            var lstClsSashDoor = new List<int>() { 4, 5, 17 };

            var lstClsSashFrenchMaster = new List<int>() { 9, 10 };

            var lstClsSashFrenchSlave = new List<int>() { 6, 8 };

            var lstClsSashTiltTurn = new List<int>() { 15, 18, 21 };

            var lstClsCompo44 = new List<int>() { 19, 22 };

            var lstClsCompo70 = new List<int>() { 20, 23 };

            var lstClsSashStableMaster = new List<int> { 25, 26 };

            var lstClsSashStableSlave = new List<int> { 27, 28 };

            var lstClsSashMFMaster = new List<int> { 29, 30 };

            var lstClsSashMFSlave = new List<int> { 31, 32 };


            if (lstClsSashWindow.Contains(sashType))
            {
                sash = new ClsSashWindow();
            }

            if (lstClsSashDoor.Contains(sashType))
            {
                sash = new ClsSashDoor();
            }

            if (lstClsSashFrenchMaster.Contains(sashType))
            {
                sash = new ClsSashFrenchMaster();
            }

            if (lstClsSashFrenchSlave.Contains(sashType))
            {
                sash = new ClsSashFrenchSlave();
            }

            if (lstClsSashTiltTurn.Contains(sashType))
            {
                sash = new ClsSashTiltTurn();
            }

            if (lstClsCompo44.Contains(sashType))
            {
                sash = new ClsSashCompo44mm();
            }

            if (lstClsCompo70.Contains(sashType))
            {
                sash = new ClsSashCompo70mm();
            }

            if (lstClsSashStableMaster.Contains(sashType))
            {
                sash = new ClsSashStableMaster();
            }

            if (lstClsSashStableSlave.Contains(sashType))
            {
                sash = new ClsSashStableSlave();
            }

            if (lstClsSashMFMaster.Contains(sashType))
            {
                sash = new ClsSashMiniFrenchMaster();
            }

            if (lstClsSashMFSlave.Contains(sashType))
            {
                sash = new ClsSashMiniFrenchSlave();
            }

            if (sash == null)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame {inputFrame.FrameNumber}, Sash: {inputSash.SashNumber} has a non implements SashType: {sashType}"
                });
            }

            return sash;
        }

        private ACSashCeption mapSashSashCeptionType(ClsInputFrame inputFrame, ClsInputSashCeption inputSash)
        {
            ACSashCeption sash = null;

            var sashType = Convert.ToInt32(inputSash.SashTypeId);

            var lstClsSashWindow = new List<int>() { 1, 2, 3, 16 };

            var lstClsSashTiltTurn = new List<int>() { 15, 18, 21 };

            if (lstClsSashWindow.Contains(sashType))
            {
                sash = new ClsSashCeptionWindow();
            }

            if (lstClsSashTiltTurn.Contains(sashType))
            {
                sash = new ClsSashCeptionTiltTurn();
            }

            if (sash == null)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame {inputFrame.FrameNumber}, SashCeption: {inputSash.SashNumber} has a non implements SashType: {sashType}"
                });
            }

            return sash;
        }

        private List<ACProfile> mapFrameProfile(ClsInputFrame inputFrame)
        {
            List<ACProfile> lstProfile = new List<ACProfile>();

            inputFrame.LstOuter().ForEach(profile =>
            {
                lstProfile.Add(mapProfileFrameOuter(inputFrame, profile));
            });


            inputFrame.LstInner().ForEach(profile =>
            {
                lstProfile.Add(mapProfileFrameInner(inputFrame, profile));
            });

            return lstProfile;
        }

        private ClsProfileFrameOuter mapProfileFrameOuter(ClsInputFrame frame, ClsInputProfile inputProfile)
        {
            var profile = new ClsProfileFrameOuter();

            mapProfileSimpleInfo(profile, inputProfile);

            mapProfileFrameOuterDBInfo(frame, profile);

            return profile;
        }

        private ClsProfileFrameInner mapProfileFrameInner(ClsInputFrame frame, ClsInputProfile inputProfile)
        {
            var profile = new ClsProfileFrameInner();

            mapProfileSimpleInfo(profile, inputProfile);

            mapProfileInnerDBInfo(frame, profile);

            return profile;
        }

        private void mapProfileSimpleInfo(ACProfile profile, ClsInputProfile inputProfile)
        {
            profile.Pcode = inputProfile.Pcode;
            profile.TransomType = inputProfile.TransomType;
            profile.ColId = inputProfile.ColId;
            profile.ColSpan = inputProfile.ColSpan;
            profile.RowId = inputProfile.RowId;
            profile.RowSpan = inputProfile.RowSpan;
            profile.LargeFaceDirection = inputProfile.LargeFaceDirection;
        }

        private void mapProfileFrameOuterDBInfo(ClsInputFrame frame, ClsProfileFrameOuter profile)
        {
            try
            {
                var info = _frameDesignerDB.OuterFrames.FirstOrDefault(fd => fd.Pcode == profile.Pcode);

                profile.ProfileHeight = info.Height;
                profile.ProfileWidth = info.Width;
                profile.RebateInternal = info.RebatInt;
                profile.RebateExternal = info.RebateExt;
                profile.ProductCategoryId = info.ProductCatId;
                profile.IsOddLeg = info.IsOddLeg;

                if (profile.IsStorm())
                {
                    profile.SashOffSet = info.Sash;
                }
            }
            catch (Exception ex)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Invalid Outer Profile PCode Supplied"
                });

                throw ex;
            }
        }

        private void mapProfileSashOuterDBInfo(ClsInputFrame frame, ISash sash, ClsProfileSashOuter profile)
        {
            try
            {
                var info = _frameDesignerDB.Sashes.FirstOrDefault(fd => fd.Pcode == profile.Pcode);

                profile.ProfileHeight = info.Height;
                profile.ProfileWidth = info.Width;
                profile.RebateInternal = info.RebateInt;
                profile.RebateExternal = info.RebateExt;
                profile.ProductCategoryId = info.ProductCatId;
            }
            catch(Exception ex)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Sash {sash.SashNumber}, Invalid Sash Profile PCode Supplied"
                });

                throw ex;
            }
        }

        private void mapProfileInnerDBInfo(ClsInputFrame frame, ACProfile profile)
        {
            try
            {
                var info = _frameDesignerDB.TransomsAndMullions.FirstOrDefault(fd => fd.Pcode == profile.Pcode);

                profile.ProfileHeight = info.Height;
                profile.ProfileWidth = info.Width;
                profile.RebateInternal = info.RebateInt;
                profile.RebateExternal = info.RebateExt;
                profile.ProductCategoryId = info.ProductCatId;
            }
            catch(Exception ex)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Frame {frame.FrameNumber}, Invalid Transom/Mullion Profile PCode Supplied"
                });

                throw ex;
            }
        }

        private ACSash mapSash(ClsInputFrame inputFrame, ClsInputSash inputSash)
        {
            ACSash sash = mapSashSashType(inputFrame, inputSash);

            if (sash != null)
            {
                sash.SashNumber = inputSash.SashNumber;
                sash.SashTypeId = inputSash.SashTypeId;
                sash.OpenIn = inputSash.OpenIn;
                sash.IsCrucifix = inputSash.IsCrucifix;

                sash.ColId = inputSash.ColId;
                sash.ColSpan = inputSash.ColSpan;
                sash.RowId = inputSash.RowId;
                sash.RowSpan = inputSash.RowSpan;

                sash.Colour = _mapper.Map<ClsInputColour, ClsColour>(inputSash.Colour);

                if (!inputSash.IsComp())
                {
                    sash.LstProfile = mapSashProfile(inputFrame, inputSash);
                }

                //TODO need mapping
                sash.LstExtra = new List<ACProduct>();
                //sash.LstExtra = _mapper.Map<List<ClsInputProduct>, List<ClsProduct>>(inputSash.LstExtra);

                sash.LstSashCeption = new List<ACSashCeption>();
                if(inputSash.LstSashCeption != null)
                {
                    inputSash.LstSashCeption.ForEach(inputSashCeption =>
                    {
                        var sashCeption = mapSashCeption(inputFrame, inputSashCeption);

                        if (sashCeption != null)
                        {
                            sash.LstSashCeption.Add(sashCeption);
                        }
                    });
                }
            }

            return sash;
        }

        private List<ACProfile> mapSashProfile(ClsInputFrame inputFrame, ClsInputSash inputSash)
        {
            List<ACProfile> lstProfile = new List<ACProfile>();

            inputSash.SashOuter(inputFrame).ForEach(profile =>
            {
                lstProfile.Add(mapProfileSashOuter(inputFrame, inputSash, profile));
            });

            inputSash.LstInner().ForEach(profile =>
            {
                lstProfile.Add(mapProfileSashInner(inputFrame, profile));
            });

            return lstProfile;
        }

        private List<ACProfile> mapSashCeptionProfile(ClsInputFrame inputFrame, ClsInputSashCeption inputSashCeption)
        {
            List<ACProfile> lstProfile = new List<ACProfile>();

            inputSashCeption.SashOuter(inputFrame).ForEach(profile =>
            {
                lstProfile.Add(mapProfileSashOuter(inputFrame, inputSashCeption, profile));
            });

            inputSashCeption.LstInner().ForEach(profile =>
            {
                lstProfile.Add(mapProfileSashInner(inputFrame, profile));
            });

            return lstProfile;
        }

        private ClsProfileSashOuter mapProfileSashOuter(ClsInputFrame inputFrame, ISash inputSash, ClsInputProfile inputProfile)
        {
            var profile = new ClsProfileSashOuter();

            mapProfileSimpleInfo(profile, inputProfile);
            mapProfileSashOuterDBInfo(inputFrame, inputSash, profile);

            return profile;
        }

        private ClsProfileSashInner mapProfileSashInner(ClsInputFrame inputFrame, ClsInputProfile inputProfile)
        {
            var profile = new ClsProfileSashInner();

            mapProfileSimpleInfo(profile, inputProfile);
            mapProfileInnerDBInfo(inputFrame, profile);

            return profile;
        }

        private ACSashCeption mapSashCeption(ClsInputFrame inputFrame, ClsInputSashCeption inputSashCeption)
        {
            ACSashCeption sashCeption = mapSashSashCeptionType(inputFrame, inputSashCeption);

            if (sashCeption != null)
            {
                sashCeption.SashNumber = inputSashCeption.SashNumber;
                sashCeption.SashTypeId = inputSashCeption.SashTypeId;
                sashCeption.OpenIn = inputSashCeption.OpenIn;
                sashCeption.IsCrucifix = inputSashCeption.IsCrucifix;

                sashCeption.ColId = inputSashCeption.ColId;
                sashCeption.ColSpan = inputSashCeption.ColSpan;
                sashCeption.RowId = inputSashCeption.RowId;
                sashCeption.RowSpan = inputSashCeption.RowSpan;

                sashCeption.Colour = _mapper.Map<ClsInputColour, ClsColour>(inputSashCeption.Colour);

                sashCeption.LstProfile = mapSashCeptionProfile(inputFrame, inputSashCeption);

                //TODO need mapping
                sashCeption.LstExtra = new List<ACProduct>();
                //sashCeption.LstExtra = _mapper.Map<List<ClsInputProduct>, List<ClsProduct>>(inputSashCeption.LstExtra);
            }

            return sashCeption;
        }
    }
}
