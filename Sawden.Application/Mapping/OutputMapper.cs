﻿using Newtonsoft.Json;
using Sawden.Application.Extensions.Main;
using Sawden.Application.Mapping.Interfaces;
using Sawden.Domain.Api.InitialProcess.Models;
using Sawden.Domain.Main.Models.Abstract;
using Sawden.Domain.Main.Models.Main;
using Sawden.Shared;
using Sawden.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sawden.Application.Mapping
{
    public class OutputMapper : IOutputMapper
    {
        private IResponseData _response;

        private ClsOrder _inputOrder;

        private ClsOutputOrder _order;

        public OutputMapper(IResponseData response)
        {
            _response = response;
        }

        public void Map(ClsOrder inputOrder)
        {
            try
            {
                _inputOrder = inputOrder;

                _order = new ClsOutputOrder()
                {
                    OrderNumber = Convert.ToInt32(inputOrder.OrderNumber)
                };

                var json = JsonConvert.SerializeObject(inputOrder, Formatting.Indented);

                mapFrames();

                mapBars();

                _response.Data = _order;
            }
            catch (Exception ex)
            {
                BadResponseGenerator.Exception(_response, ex);
            }
        }

        private void mapFrames()
        {
            _order.LstFrame = new List<ClsOutputFrame>();

            _inputOrder.LstFrame.ForEach(inputFrame =>
            {
                _order.LstFrame.Add(new ClsOutputFrame()
                {
                    FrameNumber = Convert.ToInt32(inputFrame.FrameNumber),
                    LstProfile = getAllProfileFrame(inputFrame),
                    LstSash = getAllSash(inputFrame),
                    LstGlass = getAllGlass(inputFrame),
                    LstExtras = getAllExtras(inputFrame)
                });
            });
        }

        private List<ClsOutputProfile> getAllProfileFrame(ACFrame inputFrame)
        {
            List<ClsOutputProfile> lstProfile = new List<ClsOutputProfile>();

            inputFrame.LstProfile.ForEach(profile =>
            {
                lstProfile.Add(new ClsOutputProfile()
                {
                    SashNumber = 0,
                    Pcode = profile.Pcode,
                    PartPosition = profile.PartPosition,
                    Length = profile.Length,
                    ColourIdLargeFace = profile.ColourIdLargeFace,
                    ColourIdSmallFace = profile.ColourIdSmallFace
                });
            });

            inputFrame.LstNormalSash().ForEach(sash =>
            {
                sash.LstProfile.ForEach(sashProfile =>
                {
                    lstProfile.Add(new ClsOutputProfile()
                    {
                        SashNumber = (int)sash.SashNumber,
                        Pcode = sashProfile.Pcode,
                        PartPosition = sashProfile.PartPosition,
                        Length = sashProfile.Length,
                        ColourIdLargeFace = sashProfile.ColourIdLargeFace,
                        ColourIdSmallFace = sashProfile.ColourIdSmallFace
                    });
                });

                sash.LstSashCeption.ForEach(sashception =>
                {
                    sashception.LstProfile.ForEach(sashceptionProfile =>
                    {
                        lstProfile.Add(new ClsOutputProfile()
                        {
                            SashNumber = (int)sashception.SashNumber,
                            Pcode = sashceptionProfile.Pcode,
                            PartPosition = sashceptionProfile.PartPosition,
                            Length = sashceptionProfile.Length,
                            ColourIdLargeFace = sashceptionProfile.ColourIdLargeFace,
                            ColourIdSmallFace = sashceptionProfile.ColourIdSmallFace
                        });
                    });
                });
            });

            return lstProfile;
        }

        private List<ClsOutputSash> getAllSash(ACFrame inputFrame)
        {
            List<ClsOutputSash> lstSash = new List<ClsOutputSash>();

            inputFrame.LstSash.ForEach(sash =>
            {
                lstSash.Add(new ClsOutputSash()
                {
                    SashNumber = (int)sash.SashNumber,
                    SashTypeId = (int)sash.SashTypeId,
                    Width = sash.Width,
                    Height = sash.Height,
                    OpenIn = sash.OpenIn,
                    RowId = sash.RowId,
                    RowSpan = sash.RowSpan,
                    ColId = sash.ColId,
                    ColSpan = sash.ColSpan
                });

                sash.LstSashCeption.ForEach(sashception =>
                {
                    lstSash.Add(new ClsOutputSash()
                    {
                        SashNumber = (int)sashception.SashNumber,
                        SashTypeId = (int)sashception.SashTypeId,
                        Width = sashception.Width,
                        Height = sashception.Height,
                        OpenIn = sashception.OpenIn,
                        RowId = sashception.RowId,
                        RowSpan = sashception.RowSpan,
                        ColId = sashception.ColId,
                        ColSpan = sashception.ColSpan
                    });
                });
            });

            return lstSash;
        }

        private List<ClsOutputGlass> getAllGlass(ACFrame inputFrame)
        {
            List<ClsOutputGlass> lstGlass = new List<ClsOutputGlass>();

            inputFrame.LstGlass.ForEach(glass =>
            {
                lstGlass.Add(new ClsOutputGlass()
                {
                    Width = glass.Width,
                    Height = glass.Height,
                    IsInternallyGlazed = glass.IsInternallyGlazed,
                    GlassType = glass.GlassType,
                    ColId = glass.ColId,
                    ColSpan = glass.ColSpan,
                    RowId = glass.RowId,
                    RowSpan = glass.RowSpan
                });
            });

            return lstGlass;
        }

        private List<ClsOutputProduct> getAllExtras(ACFrame inputFrame)
        {
            List<ACProduct> lstACExtras = new List<ACProduct>();
            List<ClsOutputProduct> lstExtras = new List<ClsOutputProduct>();

            lstACExtras.AddRange(inputFrame.LstExtra);

            inputFrame.LstSash.ForEach(sash =>
            {
                lstACExtras.AddRange(sash.LstExtra);

                sash.LstSashCeption.ForEach(sashception =>
                {
                    lstACExtras.AddRange(sashception.LstExtra);
                });
            });

            lstACExtras.ForEach(extra =>
            {
                lstExtras.Add(new ClsOutputProduct()
                {
                    Pcode = extra.Pcode,
                    ProductCategoryId = extra.ProductCategoryId,
                    ProductSubCategoryId = extra.ProductSubCategoryId,
                    SelectionLinkId = extra.SelectionLinkId
                });
            });

            return lstExtras;
        }

        private void mapBars()
        {
            _order.LstBars = new List<ClsOutputBar>();

            List<ACProfile> lstProfile = getAllProfile();

            var uniqueBars = lstProfile.GroupBy(g => new { g.Pcode, g.ColourIdLargeFace, g.ColourIdSmallFace })
                                .Select(s => new { s.Key.Pcode, s.Key.ColourIdLargeFace, s.Key.ColourIdSmallFace })
                                .ToList();

            uniqueBars.ForEach(bar =>
            {
                List<ACProfile> lstProfileInBar = lstProfile.Where(w => w.Pcode == bar.Pcode
                                    && w.ColourIdLargeFace == bar.ColourIdLargeFace
                                    && w.ColourIdSmallFace == bar.ColourIdSmallFace)
                                    .ToList();

                _order.LstBars.Add(new ClsOutputBar()
                {
                    Pcode = bar.Pcode,
                    ColourIdLargeFace = bar.ColourIdLargeFace,
                    ColourIdSmallFace = bar.ColourIdSmallFace,
                    Length = lstProfileInBar.Sum(s => s.Length)
                });
            });
        }

        private List<ACProfile> getAllProfile()
        {
            List<ACProfile> lstProfile = new List<ACProfile>();

            _inputOrder.LstFrame.ForEach(inputFrame =>
            {
                //all frame
                lstProfile.AddRange(inputFrame.LstProfile);

                inputFrame.LstNormalSash().ForEach(sash =>
                {
                    //all sash
                    lstProfile.AddRange(sash.LstProfile);

                    sash.LstSashCeption.ForEach(sashception =>
                    {
                        //all sashception
                        lstProfile.AddRange(sashception.LstProfile);
                    });
                });
            });

            return lstProfile;
        }
    }
}
