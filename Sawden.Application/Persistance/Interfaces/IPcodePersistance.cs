﻿using Sawden.Domain.Persistance;
using Sawden.Persistance.SawdenDBContext.Models;

namespace Sawden.Application.Persistance.Interfaces
{
    public interface IPcodePersistance
    {
        PCodeMaster GetPcodes();

        bool updatePcode(Pcodes pcode);

        int addPcode(Pcodes pcode);

        bool deletePcode(int id);

        int addPcodeType(PcodeTypes type);

        bool updatePcodeGroup(PcodeGroupsWithPcodes pcodeGroup);

        int addPcodeGroup(PcodeGroupsWithPcodes pcodeGroup);

        bool deletePcodeGroup(int id);
    }
}
