﻿using Sawden.Domain.Persistance;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Application.Persistance.Interfaces
{
    public interface IProfilePersistance
    {
        ProfileMaster GetProfiles();
    }
}
