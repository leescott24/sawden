﻿using Sawden.Domain.Main.Models.Main;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Application.Persistance.Interfaces
{
    public interface IOrderJsonPersistance
    {
        void Save(ClsOrder inputOrder);

        void Load(int orderNumber);
    }
}
