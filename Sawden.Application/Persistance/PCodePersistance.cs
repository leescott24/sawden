﻿using Sawden.Application.Persistance.Interfaces;
using Sawden.Domain.Persistance;
using Sawden.Persistance.SawdenDBContext.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sawden.Application.Persistance
{
    public class PCodePersistance : IPcodePersistance
    {
        private readonly SawdenContext _sawdenContext;

        public PCodePersistance(SawdenContext sawdenContext)
        {
            _sawdenContext = sawdenContext;
        }        

        public PCodeMaster GetPcodes()
        {
            var master = new PCodeMaster()
            {
                PCodes = _sawdenContext.Pcodes.OrderBy(o => o.PcodeTypeId).ThenBy(o => o.Pcode).ToList(),
                PCodeTypes = _sawdenContext.PcodeTypes.ToList(),
                PCodeGroups = new List<PcodeGroupsWithPcodes>()
            };

            _sawdenContext.PcodeGroups.ToList().ForEach(pgroup =>
            {
                var pcodes = _sawdenContext.Pcodes.Join(_sawdenContext.PcodeGroupLookup, p => p.Id, pg => pg.PcodeId, (p, pc) => new { p, pc })
                    .Where(w => w.pc.PcodeGroupId == pgroup.Id)
                    .Select(s => s.p).ToList();

                master.PCodeGroups.Add(new PcodeGroupsWithPcodes()
                {
                    Id = pgroup.Id,
                    Group = pgroup.Group,
                    GroupDescription = pgroup.GroupDescription,
                    Pcodes = pcodes
                });
            });

            return master;
        }

        public bool updatePcode(Pcodes pcode)
        {
            Pcodes existing = _sawdenContext.Pcodes.Where(w => w.Id == pcode.Id).FirstOrDefault();

            existing.Pcode = pcode.Pcode;
            existing.Description = pcode.Description;
            existing.PcodeTypeId = pcode.PcodeTypeId;

            _sawdenContext.SaveChanges();

            return true;
        }

        public int addPcode(Pcodes pcode)
        {
            _sawdenContext.Pcodes.Add(pcode);

            _sawdenContext.SaveChanges();

            return pcode.Id;
        }

        public bool deletePcode(int id)
        {
            Pcodes existing = _sawdenContext.Pcodes.Where(w => w.Id == id).FirstOrDefault();

            _sawdenContext.Pcodes.Remove(existing);

            _sawdenContext.SaveChanges();

            return true;
        }

        public int addPcodeType(PcodeTypes type)
        {
            _sawdenContext.PcodeTypes.Add(type);

            _sawdenContext.SaveChanges();

            return type.Id;
        }

        public bool updatePcodeGroup(PcodeGroupsWithPcodes pcodeGroup)
        {
            PcodeGroups existing = _sawdenContext.PcodeGroups.Where(w => w.Id == pcodeGroup.Id).FirstOrDefault();

            existing.Group = pcodeGroup.Group;
            existing.GroupDescription = pcodeGroup.GroupDescription;

            _sawdenContext.PcodeGroupLookup.RemoveRange(_sawdenContext.PcodeGroupLookup.Where(w => w.PcodeGroupId == pcodeGroup.Id));

            pcodeGroup.Pcodes.ForEach(pcode =>
            {
                _sawdenContext.PcodeGroupLookup.Add(new PcodeGroupLookup()
                {
                    PcodeGroupId = pcodeGroup.Id,
                    PcodeId = pcode.Id
                });
            });

            _sawdenContext.SaveChanges();

            return true;
        }

        public int addPcodeGroup(PcodeGroupsWithPcodes pcodeGroup)
        {
            PcodeGroups newGroup = new PcodeGroups()
            {
                Group = pcodeGroup.Group,
                GroupDescription = pcodeGroup.GroupDescription
            };


            _sawdenContext.PcodeGroups.Add(newGroup);

            _sawdenContext.SaveChanges();

            pcodeGroup.Pcodes.ForEach(pcode =>
            {
                _sawdenContext.PcodeGroupLookup.Add(new PcodeGroupLookup()
                {
                    PcodeGroupId = newGroup.Id,
                    PcodeId = pcode.Id
                });
            });

            _sawdenContext.SaveChanges();

            return newGroup.Id;
        }

        public bool deletePcodeGroup(int id)
        {
            PcodeGroups exisiting = _sawdenContext.PcodeGroups.Where(w => w.Id == id).FirstOrDefault();

            _sawdenContext.PcodeGroups.Remove(exisiting);

            _sawdenContext.PcodeGroupLookup.RemoveRange(_sawdenContext.PcodeGroupLookup.Where(w => w.PcodeGroupId == id));

            _sawdenContext.SaveChanges();

            return true;
        }
    }
}
