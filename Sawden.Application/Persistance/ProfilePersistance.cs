﻿using Sawden.Application.Persistance.Interfaces;
using Sawden.Domain.Persistance;
using Sawden.Persistance.SawdenDBContext.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sawden.Application.Persistance
{
    public class ProfilePersistance : IProfilePersistance
    {
        private readonly SawdenContext _sawdenContext;

        public ProfilePersistance(SawdenContext sawdenContext)
        {
            _sawdenContext = sawdenContext;
        }

        public ProfileMaster GetProfiles()
        {
            var master = new ProfileMaster()
            {
                Profiles = _sawdenContext.Profiles.Where(w => w.CutOnSaw).OrderBy(o => o.TransomType).ThenBy(o => o.ProfilePcode).ToList(),
                ProfileTypes = _sawdenContext.ProfileTypes.ToList(),
                ProfileGroups = new List<ProfileGroupsWithProfiles>()
            };

            _sawdenContext.ProfileGroups.ToList().ForEach(pgroup =>
                {
                    var profiles = _sawdenContext.Profiles.Join(_sawdenContext.ProfileGroupLookup, p => p.Id, pg => pg.ProfileId, (p, pc) => new { p, pc })
                    .Where(w => w.pc.ProfileGroupId == pgroup.Id)
                    .Select(s => s.p).ToList();

                    master.ProfileGroups.Add(new ProfileGroupsWithProfiles()
                    {
                        Id = pgroup.Id,
                        Group = pgroup.Group,
                        GroupDescription = pgroup.GroupDescription,
                        Profiles = profiles
                    });
                });

            return master;
        }
    }
}
