﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Sawden.Application.Persistance.Interfaces;
using Sawden.Domain.Main.Models.Main;
using Sawden.Persistance.SawdenDBContext.Models;
using Sawden.Shared;
using Sawden.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Sawden.Application.Persistance
{
    public class OrderJsonPersistance : IOrderJsonPersistance
    {
        private IResponseData _response;

        private readonly IConfiguration _config;

        private readonly SawdenContext _sawdenDB;

        private ClsOrder _order;

        public OrderJsonPersistance(IResponseData response, IConfiguration config, SawdenContext sawdenDB)
        {
            _response = response;

            _config = config;

            _sawdenDB = sawdenDB;
        }

        public void Load(int orderNumber)
        {
            try
            {
                var orderLog = getOrderLog(orderNumber);

                var path = getPath(orderNumber, orderLog.Timestamp, false);

                ClsOrder order = JsonConvert.DeserializeObject<ClsOrder>(File.ReadAllText(path), serializerSettings());

                _response.Data = order;
            }
            catch (Exception ex)
            {
                BadResponseGenerator.Exception(_response, ex);
            }
        }

        private OrderLog getOrderLog(int orderNumber)
        {
            OrderLog orderLog = _sawdenDB.OrderLog.Where(w => w.OrderNumber == orderNumber && w.Active).FirstOrDefault();

            if (orderLog == null)
            {
                BadResponseGenerator.Add(_response, note: new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Order number: {orderNumber}, doesnt not currently exist in Sawden.dbo.OrderLog"
                });
            }

            return orderLog;
        }

        public void Save(ClsOrder inputOrder)
        {
            try
            {
                _order = inputOrder;

                var now = DateTime.Now;

                var path = getPath(_order.OrderNumber, now, true);

                cleanUp();

                File.WriteAllText(path, JsonConvert.SerializeObject(_order, Formatting.Indented, serializerSettings()));

                OrderLog orderLog = new OrderLog()
                {
                    OrderNumber = (int)_order.OrderNumber,
                    FrameCount = _order.LstFrame.Count,
                    Active = true,
                    Deleted = false,
                    Timestamp = now
                };

                _sawdenDB.OrderLog.Add(orderLog);

                _sawdenDB.SaveChanges();
            }
            catch (Exception ex)
            {
                BadResponseGenerator.Exception(_response, ex);
            }
        }

        private string directoryChecks(string baseDirectory,  DateTime now)
        {
            string path = baseDirectory + "\\" + now.Year.ToString();            

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            path += "\\" + now.ToString("MMM");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            path += "\\" + _order.OrderNumber.ToString() + ".json";

            return path;
        }

        private string getPath(int? orderNumber, DateTime now, bool withChecks)
        {
            string path = _config["SawdenJsonDirectory"] + "\\" + now.Year.ToString();

            if (!Directory.Exists(path) && withChecks)
            {
                Directory.CreateDirectory(path);
            }

            path += "\\" + now.ToString("MMM");

            if (!Directory.Exists(path) && withChecks)
            {
                Directory.CreateDirectory(path);
            }

            path += "\\" + orderNumber.ToString() + ".json";

            return path;
        }

        private void cleanUp()
        {
            int orderNumber = Convert.ToInt32(_order.OrderNumber);

            List<OrderLog> lstOldOrders = _sawdenDB.OrderLog.Where(w => w.OrderNumber == orderNumber && w.Active).ToList();

            lstOldOrders.ForEach(oldOrder =>
            {
                var filePath = getPath(_order.OrderNumber, oldOrder.Timestamp, false);

                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }

                var old = _sawdenDB.OrderLog
                    .Where(w => w.OrderNumber == oldOrder.OrderNumber && w.Timestamp == oldOrder.Timestamp)
                    .FirstOrDefault();

                old.Active = false;
                old.Deleted = true;
            });
        }

        private JsonSerializerSettings serializerSettings()
        {
            return new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            };
        }
    }
}
