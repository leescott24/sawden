﻿using Sawden.Domain.Input.Models;
using System;

namespace Sawden.Application.Extensions.Input
{
    public static class ClsInputGlassExtensions
    {
        public static int MaxCol(this ClsInputGlass glass)
        {
            return Convert.ToInt32(glass.ColId) + Convert.ToInt32(glass.ColSpan);
        }

        public static int MaxRow(this ClsInputGlass glass)
        {
            return Convert.ToInt32(glass.RowId) + Convert.ToInt32(glass.RowSpan);
        }
    }
}
