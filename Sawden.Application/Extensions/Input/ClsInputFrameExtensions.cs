﻿using Sawden.Domain.Input.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sawden.Application.Extensions.Input
{
    public static class ClsInputFrameExtensions
    {
        public static int MaxCol(this ClsInputFrame frame)
        {
            return frame.LstCol.Count;
        }

        public static int MaxRow(this ClsInputFrame frame)
        {
            return frame.LstRow.Count;
        }

        public static List<ClsInputProfile> LstOuterTop(this ClsInputFrame frame)
        {
            return frame.LstProfile.Where(w => !w.IsVert() && w.LstRow().Contains(0)).ToList();
        }

        public static List<ClsInputProfile> LstOuterBottom(this ClsInputFrame frame)
        {
            return frame.LstProfile.Where(w => !w.IsVert() && w.LstRow().Contains(frame.MaxRow())).ToList();
        }

        public static List<ClsInputProfile> LstOuterLeft(this ClsInputFrame frame)
        {
            return frame.LstProfile.Where(w => w.IsVert() && w.LstCol().Contains(0)).ToList();
        }

        public static List<ClsInputProfile> LstOuterRight(this ClsInputFrame frame)
        {
            return frame.LstProfile.Where(w => w.IsVert() && w.LstCol().Contains(frame.MaxCol())).ToList();
        }

        public static List<ClsInputProfile> LstInnerTransom(this ClsInputFrame frame)
        {
            return frame.LstProfile.Where(w => !w.IsVert() && w.RowId > 0 && w.RowId < frame.MaxRow()).ToList();
        }

        public static List<ClsInputProfile> LstInnerMullion(this ClsInputFrame frame)
        {
            return frame.LstProfile.Where(w => w.IsVert() && w.ColId > 0 && w.ColId < frame.MaxCol()).ToList();
        }

        public static List<ClsInputProfile> LstOuter(this ClsInputFrame frame)
        {
            var lstOuter = new List<ClsInputProfile>();

            lstOuter.AddRange(frame.LstOuterTop());
            lstOuter.AddRange(frame.LstOuterBottom());
            lstOuter.AddRange(frame.LstOuterLeft());
            lstOuter.AddRange(frame.LstOuterRight());

            return lstOuter;
        }

        public static List<ClsInputProfile> LstInner(this ClsInputFrame frame)
        {
            var lstInner = new List<ClsInputProfile>();

            lstInner.AddRange(frame.LstInnerMullion());
            lstInner.AddRange(frame.LstInnerTransom());

            return lstInner;
        }

        public static List<ClsInputSash> LstCompoSash(this ClsInputFrame frame)
        {
            var lstCompoSash = new List<ClsInputSash>();

            if (frame.LstSash != null)
            {
                lstCompoSash = frame.LstSash.Where(w => w.IsComp()).ToList();
            }

            return lstCompoSash;
        }

        public static List<ClsInputSash> LstNormalSash(this ClsInputFrame frame)
        {
            var lstNormalSash = new List<ClsInputSash>();

            if (frame.LstSash != null)
            {
                lstNormalSash = frame.LstSash.Where(w => !w.IsComp()).ToList();
            }

            return lstNormalSash;
        }

        public static bool is60mm(this ClsInputFrame frame)
        {
            return frame.SystemTypeId == 1;
        }

        public static bool is70mm(this ClsInputFrame frame)
        {
            return !frame.is60mm();
        }

        public static bool isFrenchDoor(this ClsInputFrame frame)
        {
            List<int> lstFrench = new List<int>() { 9, 11};

            return lstFrench.Contains(Convert.ToInt32(frame.SystemTypeId)); 
        }

        public static bool isMiniFrench(this ClsInputFrame frame)
        {
            return Convert.ToInt32(frame.SystemTypeId) == 4;
        }

        public static bool isStableDoor(this ClsInputFrame frame)
        {
            return Convert.ToInt32(frame.SystemTypeId) == 12;
        }
    }
}
