﻿using Sawden.Domain.Input.Models;
using System;

namespace Sawden.Application.Extensions.Input
{
    public static class ClsInputProfileExtensions
    {
        public static int MaxCol(this ClsInputProfile profile)
        {
            return Convert.ToInt32(profile.ColId) + Convert.ToInt32(profile.ColSpan);
        }

        public static int MaxRow(this ClsInputProfile profile)
        {
            return Convert.ToInt32(profile.RowId) + Convert.ToInt32(profile.RowSpan);
        }

        public static bool IsVert(this ClsInputProfile profile)
        {
            if (Convert.ToInt32(profile.ColSpan) > 0 && Convert.ToInt32(profile.RowSpan) == 0)
            {
                return false;
            }
            else if (Convert.ToInt32(profile.ColSpan) == 0 && Convert.ToInt32(profile.RowSpan) > 0)
            {
                return true;
            }
            else
            {
                throw new Exception("Profile has both colspan & rowpsan this is not allowed!");
            }
        }
    }
}
