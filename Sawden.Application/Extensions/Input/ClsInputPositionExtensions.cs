﻿using Sawden.Domain.Input.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Application.Extensions.Input
{
    public static class ClsInputPositionExtensions
    {
        public static int MaxCol(this ClsInputPosition position)
        {
            return Convert.ToInt32(position.ColId) + Convert.ToInt32(position.ColSpan);
        }

        public static int MaxRow(this ClsInputPosition position)
        {
            return Convert.ToInt32(position.RowId) + Convert.ToInt32(position.RowSpan);
        }

        public static double GetColSplitDist(this ClsInputPosition position, ClsInputFrame frame)
        {
            double dist = 0;

            for (int x = Convert.ToInt32(position.ColId); x < position.MaxCol(); x++)
            {
                dist += Convert.ToDouble(frame.LstCol[x].Value);
            }

            return dist;
        }

        public static double GetRowSplitDist(this ClsInputPosition position, ClsInputFrame frame)
        {
            double dist = 0;

            for (int y = Convert.ToInt32(position.RowId); y < position.MaxRow(); y++)
            {
                dist += Convert.ToDouble(frame.LstRow[y].Value);
            }

            return dist;
        }

        public static List<int> LstRow(this ClsInputPosition position)
        {
            List<int> lstInt = new List<int>();

            for (var i = position.RowId; i <= position.RowId + position.RowSpan; i++)
            {
                lstInt.Add(Convert.ToInt32(i));
            }

            return lstInt;
        }

        public static List<int> LstCol(this ClsInputPosition position)
        {
            List<int> lstInt = new List<int>();

            for (var i = position.ColId; i <= position.ColId + position.ColSpan; i++)
            {
                lstInt.Add(Convert.ToInt32(i));
            }

            return lstInt;
        }        
    }
}
