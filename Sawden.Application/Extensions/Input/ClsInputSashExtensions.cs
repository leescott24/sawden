﻿using Sawden.Domain.Input.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sawden.Application.Extensions.Input
{
    public static class ClsInputSashExtensions
    {
        public static bool IsComp(this ClsInputSash sash)
        {
            if (sash.SashTypeId == 19 ||
                sash.SashTypeId == 20 ||
                sash.SashTypeId == 22 ||
                sash.SashTypeId == 23
                )
            {
                return true;
            }

            return false;
        }

        public static int MaxCol(this ClsInputSash sash)
        {
            return Convert.ToInt32(sash.ColId) + Convert.ToInt32(sash.ColSpan);
        }

        public static int MaxRow(this ClsInputSash sash)
        {
            return Convert.ToInt32(sash.RowId) + Convert.ToInt32(sash.RowSpan);
        }

        public static ClsInputProfile SashTop(this ClsInputSash sash, ClsInputFrame frame)
        {
            List<ClsInputProfile> lstTop = new List<ClsInputProfile>();

            lstTop = sash.LstProfile.Where(w => !w.IsVert() && w.RowId == sash.RowId).ToList();

            if (lstTop.Count == 0)
            {
                throw new Exception($"Frame {frame.FrameNumber}, Sash {sash.SashNumber} has no valid Top peice");
            }
            else if (lstTop.Count > 1)
            {
                throw new Exception($"Frame {frame.FrameNumber}, Sash {sash.SashNumber} has multiple Top peices");
            }

            return lstTop.FirstOrDefault();
        }

        public static ClsInputProfile SashBottom(this ClsInputSash sash, ClsInputFrame frame)
        {
            List<ClsInputProfile> lstBottom = new List<ClsInputProfile>();

            lstBottom = sash.LstProfile.Where(w => !w.IsVert() && w.RowId == sash.MaxRow()).ToList();

            if (lstBottom.Count == 0)
            {
                throw new Exception($"Frame {frame.FrameNumber}, Sash {sash.SashNumber} has no valid Bottom peice");
            }
            else if (lstBottom.Count > 1)
            {
                throw new Exception($"Frame {frame.FrameNumber}, Sash {sash.SashNumber} has multiple Bottom peices");
            }

            return lstBottom.FirstOrDefault();
        }

        public static ClsInputProfile SashLeft(this ClsInputSash sash, ClsInputFrame frame)
        {
            List<ClsInputProfile> lstLeft = new List<ClsInputProfile>();

            lstLeft = sash.LstProfile.Where(w => w.IsVert() && w.ColId == sash.ColId).ToList();

            if (lstLeft.Count == 0)
            {
                throw new Exception($"Frame {frame.FrameNumber}, Sash {sash.SashNumber} has no valid Left peice");
            }
            else if (lstLeft.Count > 1)
            {
                throw new Exception($"Frame {frame.FrameNumber}, Sash {sash.SashNumber} has multiple Left peices");
            }

            return lstLeft.FirstOrDefault();
        }

        public static ClsInputProfile SashRight(this ClsInputSash sash, ClsInputFrame frame)
        {
            List<ClsInputProfile> lstRight = new List<ClsInputProfile>();

            lstRight = sash.LstProfile.Where(w => w.IsVert() && w.ColId == sash.MaxCol()).ToList();

            if (lstRight.Count == 0)
            {
                throw new Exception($"Frame {frame.FrameNumber}, Sash {sash.SashNumber} has no valid Right peice");
            }
            else if (lstRight.Count > 1)
            {
                throw new Exception($"Frame {frame.FrameNumber}, Sash {sash.SashNumber} has multiple Right peices");
            }

            return lstRight.FirstOrDefault();
        }

        public static List<ClsInputProfile> SashOuter(this ClsInputSash sash, ClsInputFrame frame)
        {
            List<ClsInputProfile> lstOuter = new List<ClsInputProfile>();

            lstOuter.Add(sash.SashTop(frame));
            lstOuter.Add(sash.SashBottom(frame));
            lstOuter.Add(sash.SashLeft(frame));
            lstOuter.Add(sash.SashRight(frame));

            return lstOuter;
        }

        public static List<ClsInputProfile> LstInnerMullion(this ClsInputSash sash)
        {
            var lstMullion = new List<ClsInputProfile>();

            if (!sash.IsComp())
            {
                lstMullion = sash.LstProfile.Where(w => w.IsVert() && w.ColId > sash.ColId && w.ColId < sash.MaxCol()).ToList();
            }

            return lstMullion;
        }

        public static List<ClsInputProfile> LstInnerTransom(this ClsInputSash sash)
        {
            var lstTransom = new List<ClsInputProfile>();

            if (!sash.IsComp())
            {
                lstTransom = sash.LstProfile.Where(w => !w.IsVert() && w.RowId > sash.RowId && w.RowId < sash.MaxRow()).ToList();
            }

            return lstTransom;
        }

        public static List<ClsInputProfile> LstInner(this ClsInputSash sash)
        {
            var lstInner = new List<ClsInputProfile>();

            lstInner.AddRange(sash.LstInnerMullion());
            lstInner.AddRange(sash.LstInnerTransom());

            return lstInner;
        }

        public static List<ClsInputSashCeption> GetSashCeption(this ClsInputSash sash)
        {
            List<ClsInputSashCeption> lstSashCeption = new List<ClsInputSashCeption>();

            if (sash.LstSashCeption != null)
            {
                lstSashCeption = sash.LstSashCeption;
            }

            return lstSashCeption;
        }
    }
}
