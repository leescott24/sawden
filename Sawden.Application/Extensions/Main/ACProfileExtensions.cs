﻿using Sawden.Domain.Main.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Application.Extensions.Main
{
    public static class ACProfileExtensions
    {
        public static bool IsVert(this ACProfile profile)
        {
            if (Convert.ToInt32(profile.ColSpan) > 0 && Convert.ToInt32(profile.RowSpan) == 0)
            {
                return false;
            }
            else if (Convert.ToInt32(profile.ColSpan) == 0 && Convert.ToInt32(profile.RowSpan) > 0)
            {
                return true;
            }
            else
            {
                throw new Exception("Profile has both colspan & rowpsan this is not allowed!");
            }
        }

        public static bool IsStorm(this ACProfile profile)
        {
            return profile.ProductCategoryId == 25;
        }

        public static bool IsFrameOuter(this ACProfile profile)
        {
            return profile.ProductCategoryId == 1;
        }

        public static bool IsSashOuter(this ACProfile profile)
        {
            return profile.ProductCategoryId == 2;
        }

        public static bool IsMulTra(this ACProfile profile)
        {
            return profile.ProductCategoryId == 3;
        }
    }
}
