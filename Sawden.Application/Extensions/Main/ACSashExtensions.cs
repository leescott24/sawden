﻿using Sawden.Domain.Main.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sawden.Application.Extensions.Main
{
    public static class ACSashExtensions
    {
        public static bool IsComp(this ACSash sash)
        {
            if (sash.SashTypeId == 19 ||
                sash.SashTypeId == 20 ||
                sash.SashTypeId == 22 ||
                sash.SashTypeId == 23
                )
            {
                return true;
            }

            return false;
        }

        public static int MaxCol(this ACSash sash)
        {
            return Convert.ToInt32(sash.ColId) + Convert.ToInt32(sash.ColSpan);
        }

        public static int MaxRow(this ACSash sash)
        {
            return Convert.ToInt32(sash.RowId) + Convert.ToInt32(sash.RowSpan);
        }

        public static ACProfile SashTop(this ACSash sash, ACFrame frame)
        {
            List<ACProfile> lstTop = new List<ACProfile>();

            lstTop = sash.LstProfile.Where(w => !w.IsVert() && w.RowId == sash.RowId).ToList();

            if (lstTop.Count == 0)
            {
                throw new Exception($"Frame {frame.FrameNumber}, Sash {sash.SashNumber} has no valid Top peice");
            }
            else if (lstTop.Count > 1)
            {
                throw new Exception($"Frame {frame.FrameNumber}, Sash {sash.SashNumber} has multiple Top peices");
            }

            return lstTop.FirstOrDefault();
        }

        public static ACProfile SashBottom(this ACSash sash, ACFrame frame)
        {
            List<ACProfile> lstBottom = new List<ACProfile>();

            lstBottom = sash.LstProfile.Where(w => !w.IsVert() && w.RowId == sash.MaxRow()).ToList();

            if (lstBottom.Count == 0)
            {
                throw new Exception($"Frame {frame.FrameNumber}, Sash {sash.SashNumber} has no valid Bottom peice");
            }
            else if (lstBottom.Count > 1)
            {
                throw new Exception($"Frame {frame.FrameNumber}, Sash {sash.SashNumber} has multiple Bottom peices");
            }

            return lstBottom.FirstOrDefault();
        }

        public static ACProfile SashLeft(this ACSash sash, ACFrame frame)
        {
            List<ACProfile> lstLeft = new List<ACProfile>();

            lstLeft = sash.LstProfile.Where(w => w.IsVert() && w.ColId == sash.ColId).ToList();

            if (lstLeft.Count == 0)
            {
                throw new Exception($"Frame {frame.FrameNumber}, Sash {sash.SashNumber} has no valid Left peice");
            }
            else if (lstLeft.Count > 1)
            {
                throw new Exception($"Frame {frame.FrameNumber}, Sash {sash.SashNumber} has multiple Left peices");
            }

            return lstLeft.FirstOrDefault();
        }

        public static ACProfile SashRight(this ACSash sash, ACFrame frame)
        {
            List<ACProfile> lstRight = new List<ACProfile>();

            lstRight = sash.LstProfile.Where(w => w.IsVert() && w.ColId == sash.MaxCol()).ToList();

            if (lstRight.Count == 0)
            {
                throw new Exception($"Frame {frame.FrameNumber}, Sash {sash.SashNumber} has no valid Right peice");
            }
            else if (lstRight.Count > 1)
            {
                throw new Exception($"Frame {frame.FrameNumber}, Sash {sash.SashNumber} has multiple Right peices");
            }

            return lstRight.FirstOrDefault();
        }

        public static List<ACProfile> SashOuter(this ACSash sash, ACFrame frame)
        {
            List<ACProfile> lstOuter = new List<ACProfile>();

            lstOuter.Add(sash.SashTop(frame));
            lstOuter.Add(sash.SashBottom(frame));
            lstOuter.Add(sash.SashLeft(frame));
            lstOuter.Add(sash.SashRight(frame));

            return lstOuter;
        }

        public static List<ACProfile> LstInnerMullion(this ACSash sash)
        {
            var lstMullion = new List<ACProfile>();

            if (!sash.IsComp())
            {
                lstMullion = sash.LstProfile.Where(w => w.IsVert() && w.ColId > sash.ColId && w.ColId < sash.MaxCol()).ToList();
            }

            return lstMullion;
        }

        public static List<ACProfile> LstInnerTransom(this ACSash sash)
        {
            var lstTransom = new List<ACProfile>();

            if (!sash.IsComp())
            {
                lstTransom = sash.LstProfile.Where(w => !w.IsVert() && w.RowId > sash.RowId && w.RowId < sash.MaxRow()).ToList();
            }

            return lstTransom;
        }

        public static List<ACProfile> LstInner(this ACSash sash)
        {
            var lstInner = new List<ACProfile>();

            lstInner.AddRange(sash.LstInnerMullion());
            lstInner.AddRange(sash.LstInnerTransom());

            return lstInner;
        }
    }
}
