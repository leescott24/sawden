﻿using Sawden.Domain.Main.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sawden.Application.Extensions.Main
{
    public static class ACFrameExtensions
    {
        public static int MaxCol(this ACFrame frame)
        {
            return frame.LstCol.Count;
        }

        public static int MaxRow(this ACFrame frame)
        {
            return frame.LstRow.Count;
        }

        public static List<ACProfile> LstOuterTop(this ACFrame frame)
        {
            return frame.LstProfile.Where(w => !w.IsVert() && w.LstRow().Contains(0)).ToList();
        }

        public static List<ACProfile> LstOuterBottom(this ACFrame frame)
        {
            return frame.LstProfile.Where(w => !w.IsVert() && w.LstRow().Contains(frame.MaxRow())).ToList();
        }

        public static List<ACProfile> LstOuterLeft(this ACFrame frame)
        {
            return frame.LstProfile.Where(w => w.IsVert() && w.LstCol().Contains(0)).ToList();
        }

        public static List<ACProfile> LstOuterRight(this ACFrame frame)
        {
            return frame.LstProfile.Where(w => w.IsVert() && w.LstCol().Contains(frame.MaxCol())).ToList();
        }

        public static List<ACProfile> LstInnerTransom(this ACFrame frame)
        {
            return frame.LstProfile.Where(w => !w.IsVert() && w.RowId > 0 && w.RowId < frame.MaxRow()).ToList();
        }

        public static List<ACProfile> LstInnerMullion(this ACFrame frame)
        {
            return frame.LstProfile.Where(w => w.IsVert() && w.ColId > 0 && w.ColId < frame.MaxCol()).ToList();
        }

        public static List<ACProfile> LstOuter(this ACFrame frame)
        {
            var lstOuter = new List<ACProfile>();

            lstOuter.AddRange(frame.LstOuterTop());
            lstOuter.AddRange(frame.LstOuterBottom());
            lstOuter.AddRange(frame.LstOuterLeft());
            lstOuter.AddRange(frame.LstOuterRight());

            return lstOuter;
        }

        public static List<ACProfile> LstInner(this ACFrame frame)
        {
            var lstInner = new List<ACProfile>();

            lstInner.AddRange(frame.LstInnerMullion());
            lstInner.AddRange(frame.LstInnerTransom());

            return lstInner;
        }

        public static List<ACSash> LstCompoSash(this ACFrame frame)
        {
            var lstCompoSash = new List<ACSash>();

            if (frame.LstSash != null)
            {
                lstCompoSash = frame.LstSash.Where(w => w.IsComp()).ToList();
            }

            return lstCompoSash;
        }

        public static List<ACSash> LstNormalSash(this ACFrame frame)
        {
            var lstNormalSash = new List<ACSash>();

            if (frame.LstSash != null)
            {
                lstNormalSash = frame.LstSash.Where(w => !w.IsComp()).ToList();
            }

            return lstNormalSash;
        }
    }
}
