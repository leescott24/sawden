﻿using Sawden.Domain.Main.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sawden.Application.Extensions.Main
{
    public static class ACSashCeptionExtensions
    {
        public static int MaxCol(this ACSashCeption sashCeption)
        {
            return Convert.ToInt32(sashCeption.ColId) + Convert.ToInt32(sashCeption.ColSpan);
        }

        public static int MaxRow(this ACSashCeption sashCeption)
        {
            return Convert.ToInt32(sashCeption.RowId) + Convert.ToInt32(sashCeption.RowSpan);
        }

        public static ACProfile SashTop(this ACSashCeption sashCeption, ACFrame frame)
        {
            List<ACProfile> lstTop = new List<ACProfile>();

            lstTop = sashCeption.LstProfile.Where(w => !w.IsVert() && w.RowId == sashCeption.RowId).ToList();

            if (lstTop.Count == 0)
            {
                throw new Exception($"Frame {frame.FrameNumber}, SashCeption {sashCeption.SashNumber} has no valid Top peice");
            }
            else if (lstTop.Count > 1)
            {
                throw new Exception($"Frame {frame.FrameNumber}, SashCeption {sashCeption.SashNumber} has multiple Top peices");
            }

            return lstTop.FirstOrDefault();
        }

        public static ACProfile SashBottom(this ACSashCeption sashCeption, ACFrame frame)
        {
            List<ACProfile> lstBottom = new List<ACProfile>();

            lstBottom = sashCeption.LstProfile.Where(w => !w.IsVert() && w.RowId == sashCeption.MaxRow()).ToList();

            if (lstBottom.Count == 0)
            {
                throw new Exception($"Frame {frame.FrameNumber}, SashCeption {sashCeption.SashNumber} has no valid Bottom peice");
            }
            else if (lstBottom.Count > 1)
            {
                throw new Exception($"Frame {frame.FrameNumber}, SashCeption {sashCeption.SashNumber} has multiple Bottom peices");
            }

            return lstBottom.FirstOrDefault();
        }

        public static ACProfile SashLeft(this ACSashCeption sashCeption, ACFrame frame)
        {
            List<ACProfile> lstLeft = new List<ACProfile>();

            lstLeft = sashCeption.LstProfile.Where(w => w.IsVert() && w.ColId == sashCeption.ColId).ToList();

            if (lstLeft.Count == 0)
            {
                throw new Exception($"Frame {frame.FrameNumber}, SashCeption {sashCeption.SashNumber} has no valid Left peice");
            }
            else if (lstLeft.Count > 1)
            {
                throw new Exception($"Frame {frame.FrameNumber}, SashCeption {sashCeption.SashNumber} has multiple Left peices");
            }

            return lstLeft.FirstOrDefault();
        }

        public static ACProfile SashRight(this ACSashCeption sashCeption, ACFrame frame)
        {
            List<ACProfile> lstRight = new List<ACProfile>();

            lstRight = sashCeption.LstProfile.Where(w => w.IsVert() && w.ColId == sashCeption.MaxCol()).ToList();

            if (lstRight.Count == 0)
            {
                throw new Exception($"Frame {frame.FrameNumber}, SashCeption {sashCeption.SashNumber} has no valid Right peice");
            }
            else if (lstRight.Count > 1)
            {
                throw new Exception($"Frame {frame.FrameNumber}, SashCeption {sashCeption.SashNumber} has multiple Right peices");
            }

            return lstRight.FirstOrDefault();
        }

        public static List<ACProfile> SashOuter(this ACSashCeption sashCeption, ACFrame frame)
        {
            List<ACProfile> lstOuter = new List<ACProfile>();

            lstOuter.Add(sashCeption.SashTop(frame));
            lstOuter.Add(sashCeption.SashBottom(frame));
            lstOuter.Add(sashCeption.SashLeft(frame));
            lstOuter.Add(sashCeption.SashRight(frame));

            return lstOuter;
        }

        public static List<ACProfile> LstInnerMullion(this ACSashCeption sashCeption)
        {
            var lstMullion = new List<ACProfile>();

            lstMullion = sashCeption.LstProfile.Where(w => w.IsVert() && w.ColId > sashCeption.ColId && w.ColId < sashCeption.MaxCol()).ToList();

            return lstMullion;
        }

        public static List<ACProfile> LstInnerTransom(this ACSashCeption sashCeption)
        {
            var lstTransom = new List<ACProfile>();

            lstTransom = sashCeption.LstProfile.Where(w => !w.IsVert() && w.RowId > sashCeption.RowId && w.RowId < sashCeption.MaxRow()).ToList();

            return lstTransom;
        }

        public static List<ACProfile> LstInner(this ACSashCeption sashCeption)
        {
            var lstInner = new List<ACProfile>();

            lstInner.AddRange(sashCeption.LstInnerMullion());
            lstInner.AddRange(sashCeption.LstInnerTransom());

            return lstInner;
        }
    }
}
