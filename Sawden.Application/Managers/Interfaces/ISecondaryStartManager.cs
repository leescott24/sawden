﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Application.Managers.Interfaces
{
    public interface ISecondaryStartManager
    {
        void Start(int orderNumber);
    }
}
