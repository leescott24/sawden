﻿using Sawden.Domain.Input.Models;

namespace Sawden.Application.Managers.Interfaces
{
    public interface IInitialStartManager
    {
        void Start(ClsInputOrder inputOrder);
    }
}
