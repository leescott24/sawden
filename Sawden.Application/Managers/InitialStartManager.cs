﻿using Sawden.Application.Checks.Interfaces;
using Sawden.Application.Logic.Interfaces;
using Sawden.Application.Managers.Interfaces;
using Sawden.Application.Mapping.Interfaces;
using Sawden.Application.Persistance.Interfaces;
using Sawden.Application.Setup.Interfaces;
using Sawden.Domain.Input.Models;
using Sawden.Shared;
using Sawden.Shared.Interfaces;
using System;

namespace Sawden.Application.Managers
{
    public class InitialStartManager : IInitialStartManager
    {
        private readonly IResponseData _response;

        private readonly ISawdenSettingsSetup _sawdenSettingsSetup;

        private readonly IInitialChecks _intialChecks;

        private readonly IMulTraSplitAndCrucifixCalculation _mulTraSplitAndCrucifixCalculation;

        private readonly IPeiceSplitAndOrientationCalculation _peiceSplitAndOrientation;        

        private readonly IMainMapper _mainMapper;

        private readonly ISecondaryChecks _secondaryChecks;        

        private readonly IProfileCalculation _profileCalculation;

        private readonly ISashSlabSizeCalculation _sashSlabSizeCalculation;

        private readonly IGlassSizeCalculation _glassSizeCalculation;

        private readonly IColourCalculation _colourCalculation;

        private readonly IPartPositionCalculation _partPositionCalculation;

        private readonly ILetterBoxCalculation _letterBoxCalculation;

        private readonly ITrickleVentCalculation _trickleVentCalculation;

        private readonly IPCodeSizeCalculation _pCodeSizeCalculation;        

        private readonly IMechanicalJointCalculation _mechanicalJointCalculation;

        private readonly IOrderJsonPersistance _orderJsonPersistance;

        private readonly IOutputMapper _outputMapper;

        public InitialStartManager(IResponseData response,
            ISawdenSettingsSetup sawdenSettingsSetup,
            IInitialChecks intialChecks,
            IMulTraSplitAndCrucifixCalculation mulTraSplitAndCrucifixCalculation,
            IPeiceSplitAndOrientationCalculation peiceSplitAndOrientation,            
            IMainMapper mainMapper,
            ISecondaryChecks secondaryChecks,            
            IProfileCalculation profileCalculation,
            ISashSlabSizeCalculation sashSlabSizeCalculation,
            IGlassSizeCalculation glassSizeCalculation,
            IColourCalculation colourCalculation,
            IPartPositionCalculation partPositionCalculation,
            ILetterBoxCalculation letterBoxCalculation,
            ITrickleVentCalculation trickleVentCalculation,
            IPCodeSizeCalculation pCodeSizeCalculation,            
            IMechanicalJointCalculation mechanicalJointCalculation,
            IOrderJsonPersistance orderJsonPersistance,
            IOutputMapper ouputMapper)
        {
            _response = response;

            _sawdenSettingsSetup = sawdenSettingsSetup;

            _intialChecks = intialChecks;

            _mulTraSplitAndCrucifixCalculation = mulTraSplitAndCrucifixCalculation;

            _peiceSplitAndOrientation = peiceSplitAndOrientation;            

            _mainMapper = mainMapper;

            _secondaryChecks = secondaryChecks;            

            _profileCalculation = profileCalculation;

            _sashSlabSizeCalculation = sashSlabSizeCalculation;

            _glassSizeCalculation = glassSizeCalculation;

            _colourCalculation = colourCalculation;

            _partPositionCalculation = partPositionCalculation;

            _letterBoxCalculation = letterBoxCalculation;

            _trickleVentCalculation = trickleVentCalculation;

            _pCodeSizeCalculation = pCodeSizeCalculation;            

            _mechanicalJointCalculation = mechanicalJointCalculation;

            _orderJsonPersistance = orderJsonPersistance;

            _outputMapper = ouputMapper;
        }

        public void Start(ClsInputOrder inputOrder)
        {
            try
            {
                //intial setup
                _sawdenSettingsSetup.Setup();
                if (!_response.Continue) return;

                //initial checks
                _intialChecks.Check(inputOrder);
                if (!_response.Continue) return;

                //Split mullions/transoms due to differing widths & crucifix property per frame & sash
                //also check that all iscrucifix input are valid
                _mulTraSplitAndCrucifixCalculation.Process(inputOrder);
                if (!_response.Continue) return;

                //peice split & orientation logic before Map
                //only splits on changing apertures (open in/open out) not mul/tras that intersect!
                _peiceSplitAndOrientation.Process(inputOrder);
                if (!_response.Continue) return;                

                //map checked & valid inputOrder to main ClsOrder
                _mainMapper.Map(inputOrder);
                if (!_response.Continue) return;

                //secondary checks after mapping
                _secondaryChecks.Check(_response.Data);
                if (!_response.Continue) return;
                //add all frame/sash specific checks
                //i.e. french doors have master/slave
                //all you can think off :)                
                
                //profile calculations
                _profileCalculation.Process(_response.Data);
                if (!_response.Continue) return;

                //sash & slab size calculation
                _sashSlabSizeCalculation.Process(_response.Data);
                if (!_response.Continue) return;

                //glass size calculation
                _glassSizeCalculation.Process(_response.Data);
                if (!_response.Continue) return;

                //colour logic for all profile
                _colourCalculation.Process(_response.Data);
                if (!_response.Continue) return;

                //fill in part positions for parts/profile
                _partPositionCalculation.Process(_response.Data);
                if (!_response.Continue) return;

                //letter box logic
                _letterBoxCalculation.Process(_response.Data);
                if (!_response.Continue) return;

                //trickle vent logic
                _trickleVentCalculation.Process(_response.Data);
                if (!_response.Continue) return;

                //pcode -- lock & hinge calulation from sash sizes
                _pCodeSizeCalculation.Process(_response.Data);
                if (!_response.Continue) return;                

                //mech joints calculations
                _mechanicalJointCalculation.Process(_response.Data);
                if (!_response.Continue) return;

                //save json for processing optimized parts
                _orderJsonPersistance.Save(_response.Data);
                if (!_response.Continue) return;

                //add mapping to return object for camden
                _outputMapper.Map(_response.Data);
            }
            catch (Exception ex)
            {
                BadResponseGenerator.Exception(_response, ex);
            }
        }
    }
}
