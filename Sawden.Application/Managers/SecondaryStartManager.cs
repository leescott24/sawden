﻿using Sawden.Application.Managers.Interfaces;
using Sawden.Application.Persistance.Interfaces;
using Sawden.Domain.Main.Models.Main;
using Sawden.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Application.Managers
{
    public class SecondaryStartManager : ISecondaryStartManager
    {
        private readonly IResponseData _response;

        private readonly IOrderJsonPersistance _orderJsonPersistance;

        public SecondaryStartManager(IResponseData response,
            IOrderJsonPersistance orderJsonPersistance)
        {
            _response = response;

            _orderJsonPersistance = orderJsonPersistance;
        }

        public void Start(int orderNumber)
        {
            //get data
            _orderJsonPersistance.Load(orderNumber);
            if (!_response.Continue) return;

            _response.Data = "Optimized Parts successfully created";
        }
    }
}
