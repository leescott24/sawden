﻿using Sawden.Application.Setup.Interfaces;
using Sawden.Persistance.SawdenDBContext.Models;
using Sawden.Shared;
using Sawden.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sawden.Application.Setup
{
    public class SawdenSettingsSetup : ISawdenSettingsSetup
    {
        private IResponseData _response;

        private readonly ISawdenSettings _settings;

        private readonly SawdenContext _sawdenDB;

        public SawdenSettingsSetup(IResponseData response, ISawdenSettings settings, SawdenContext sawdenDB)
        {
            _response = response;

            _settings = settings;

            _sawdenDB = sawdenDB;
        }

        public void Setup()
        {
            try
            {
                setupSawdenSettings();
            }
            catch (Exception ex)
            {
                BadResponseGenerator.Exception(_response, ex);
            }
        }

        private void setupSawdenSettings()
        {
            _settings.AllowedMasterProductTypes = _sawdenDB.AllowedMasterProductTypes.Select(s => s.MasterProductId).ToList();

            _settings.AllowedSystemTypes = _sawdenDB.AllowedSystemTypes.Select(s => s.SystemTypeId).ToList();

            _settings.InvalidPcodes = _sawdenDB.InvalidPcodes.Select(s => new ClsInvalidPcode()
                {
                    PCode = s.Pcode,
                    IsError = s.IsError,
                    IsIgnore = s.IsIgnore
                }
            ).ToList();


            Dictionary<string, double> lstDefaultOffets = _sawdenDB.DefaultOffsets.Select(s => new KeyValuePair<string, double>(s.Description, s.Value))
                                                            .ToDictionary(d => d.Key, d => d.Value);

            _settings.GlassOffset = lstDefaultOffets["GlassOffset"];
            _settings.SashOverlap = lstDefaultOffets["SashOverLap"];
            _settings.OddLegOverLap = lstDefaultOffets["OddLegOverLap"];
            _settings.DummyMullionProfileOffset = lstDefaultOffets["DummyMullionProfileOffset"];
            _settings.DummyMullionAliOffset = lstDefaultOffets["DummyMullionAliOffset"];
            _settings.Comp44mmOffset = lstDefaultOffets["Comp44mmOffset"];
            _settings.Comp70mmOverLap = lstDefaultOffets["Comp70mmOverLap"];
        }
    }
}
