﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Application.Setup.Interfaces
{
    public interface ISawdenSettingsSetup
    {
        void Setup();
    }
}
