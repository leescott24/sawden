﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class ErrorsAndWarnings
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string ReturnInfo { get; set; }
    }
}
