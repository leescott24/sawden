﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class ReinforcingParts
    {
        public int Id { get; set; }
        public string ProductCatId { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public int? ReinforcingGroupId { get; set; }
        public int? Length { get; set; }
        public int? CostUomId { get; set; }
        public decimal CostPrice { get; set; }
        public string LabelCharacter { get; set; }
        public bool? Available { get; set; }
    }
}
