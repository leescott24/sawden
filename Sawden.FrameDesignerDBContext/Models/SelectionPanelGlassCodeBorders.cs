﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SelectionPanelGlassCodeBorders
    {
        public int GlassCodeId { get; set; }
        public int BorderColId { get; set; }
    }
}
