﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class CompositeMasterGroups
    {
        public int Id { get; set; }
        public string Mastercompotype { get; set; }
        public string Compocode { get; set; }
        public int? PricingMethodId { get; set; }
        public decimal? Baseprice { get; set; }
        public decimal? Eurobaseprice { get; set; }
        public int? CostUom { get; set; }
        public decimal? CostPrice { get; set; }
        public int? DisplaySequence { get; set; }
        public int? Compotype { get; set; }
        public int Minwidth { get; set; }
        public int Maxwidth { get; set; }
        public int Minheight { get; set; }
        public int Maxheight { get; set; }
        public int? Slabtypeid { get; set; }
    }
}
