﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class PanelColours
    {
        public int Id { get; set; }
        public string Colourdesc { get; set; }
        public int Panelcolourgroup { get; set; }
        public bool? Issprayed { get; set; }
        public bool Isavailable { get; set; }
        public bool Geobaravailable { get; set; }
    }
}
