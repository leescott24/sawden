﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SubFrameJointTypes
    {
        public int Id { get; set; }
        public string JointType { get; set; }
    }
}
