﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class Locks
    {
        public int Id { get; set; }
        public string Pcode { get; set; }
        public string StockCode { get; set; }
        public int? Qty { get; set; }
        public int? ProductCatId { get; set; }
        public int ProductSubCatId { get; set; }
        public int SelectionLinkId { get; set; }
        public string Description { get; set; }
        public int? PricingMethodId { get; set; }
        public decimal? SellPrice { get; set; }
        public int? CostUomId { get; set; }
        public decimal? CostPrice { get; set; }
        public bool IsCentered { get; set; }
        public int MinPieceLength { get; set; }
        public int? MaxPieceLength { get; set; }
        public int HandleHeight { get; set; }
        public string RoutingCode { get; set; }

        public ProductCategories ProductCat { get; set; }
        public ProductSubcategories ProductSubCat { get; set; }
        public SelectionLinks SelectionLink { get; set; }
    }
}
