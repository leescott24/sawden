﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class ProductCategories
    {
        public ProductCategories()
        {
            BayPoles = new HashSet<BayPoles>();
            Beads = new HashSet<Beads>();
            Cills = new HashSet<Cills>();
            Handles = new HashSet<Handles>();
            Hinges = new HashSet<Hinges>();
            Locks = new HashSet<Locks>();
            NightVents = new HashSet<NightVents>();
            ProductSubCatDefaultsExploded = new HashSet<ProductSubCatDefaultsExploded>();
            Sashes = new HashSet<Sashes>();
            SubFrameParts = new HashSet<SubFrameParts>();
            SubFrames = new HashSet<SubFrames>();
            TransomsAndMullions = new HashSet<TransomsAndMullions>();
        }

        public int Id { get; set; }
        public string Table { get; set; }
        public string TableName { get; set; }
        public bool? UsePartsTrans { get; set; }
        public bool? IsSubFrame { get; set; }
        public int? ProductCatTypetId { get; set; }
        public bool? RequirePosition { get; set; }
        public bool? CanSelectMultiple { get; set; }

        public ICollection<BayPoles> BayPoles { get; set; }
        public ICollection<Beads> Beads { get; set; }
        public ICollection<Cills> Cills { get; set; }
        public ICollection<Handles> Handles { get; set; }
        public ICollection<Hinges> Hinges { get; set; }
        public ICollection<Locks> Locks { get; set; }
        public ICollection<NightVents> NightVents { get; set; }
        public ICollection<ProductSubCatDefaultsExploded> ProductSubCatDefaultsExploded { get; set; }
        public ICollection<Sashes> Sashes { get; set; }
        public ICollection<SubFrameParts> SubFrameParts { get; set; }
        public ICollection<SubFrames> SubFrames { get; set; }
        public ICollection<TransomsAndMullions> TransomsAndMullions { get; set; }
    }
}
