﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class ThresholdTypes
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
