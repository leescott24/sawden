﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SelectionSashTypeLinks
    {
        public int SelectionLinkId { get; set; }
        public int SashTypeId { get; set; }

        public SashTypes SashType { get; set; }
        public SelectionLinks SelectionLink { get; set; }
    }
}
