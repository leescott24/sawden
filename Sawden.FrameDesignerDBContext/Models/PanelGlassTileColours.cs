﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class PanelGlassTileColours
    {
        public int Id { get; set; }
        public string TileColour { get; set; }
        public int? DisplaySequence { get; set; }
    }
}
