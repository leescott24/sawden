﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SprayingOptions
    {
        public int Id { get; set; }
        public string Spraytype { get; set; }
        public int? PricingMethodId { get; set; }
        public decimal? ChargePerMetre { get; set; }
    }
}
