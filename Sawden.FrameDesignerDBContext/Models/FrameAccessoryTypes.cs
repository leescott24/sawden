﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class FrameAccessoryTypes
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public int? DisplaySequence { get; set; }
        public bool? IsSubFrame { get; set; }
    }
}
