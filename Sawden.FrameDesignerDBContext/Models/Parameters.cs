﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class Parameters
    {
        public int Id { get; set; }
        public decimal? Euroexchangerate { get; set; }
        public int? WeldBurnoff { get; set; }
        public int? DoorMidRailHeight { get; set; }
    }
}
