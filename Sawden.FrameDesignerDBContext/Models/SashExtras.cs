﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SashExtras
    {
        public int Id { get; set; }
        public int? Qty { get; set; }
        public string Pcode { get; set; }
        public string StockCode { get; set; }
        public string Description { get; set; }
        public int? PricingMethodId { get; set; }
        public decimal? SellPrice { get; set; }
        public int? CostUomId { get; set; }
        public decimal? Costprice { get; set; }
        public int? ProductCatId { get; set; }
        public int? ProductSubCatId { get; set; }
        public bool? AvailableAsExtr { get; set; }
        public bool? Available { get; set; }
        public decimal? SellPriceLoose { get; set; }
        public string RoutingCode { get; set; }
        public int? SelectionLinkId { get; set; }
    }
}
