﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class PanelGlassTypes
    {
        public int Id { get; set; }
        public string Decoglasstype { get; set; }
        public string SubtypeCode { get; set; }
        public string Mastersubtype { get; set; }
    }
}
