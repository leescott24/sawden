﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SelectionOuterPriceExclude
    {
        public int OuterId { get; set; }
        public int SystemId { get; set; }
    }
}
