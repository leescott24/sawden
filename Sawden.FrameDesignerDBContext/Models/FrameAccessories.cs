﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class FrameAccessories
    {
        public int Id { get; set; }
        public int ProductCatId { get; set; }
        public string BasePcode { get; set; }
        public string Description { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public int? Width { get; set; }
        public int? FrameDeduction { get; set; }
        public int? Frameaddition { get; set; }
        public int? Extenderdeduction { get; set; }
        public int? Extenderaddition { get; set; }
        public bool? IsSubframe { get; set; }
        public int? FrameAccTypeId { get; set; }
        public string SystemTypesAvailableOn { get; set; }
        public int? SelectionLinkId { get; set; }
    }
}
