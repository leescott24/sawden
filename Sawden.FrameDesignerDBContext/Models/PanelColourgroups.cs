﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class PanelColourgroups
    {
        public int Id { get; set; }
        public string Colourgroupdesc { get; set; }
        public int? Fullpanelprice { get; set; }
        public int? Halfpanelprice { get; set; }
    }
}
