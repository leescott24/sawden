﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class DecoGlassCodes
    {
        public int Id { get; set; }
        public string Pcode { get; set; }
        public string Description { get; set; }
        public int? GlassBordersAvailId { get; set; }
        public int? TileColoursAvailId { get; set; }
        public int? PricingMethodId { get; set; }
        public int? SellPrice { get; set; }
        public int? MinWidth { get; set; }
        public int? MaxWidth { get; set; }
        public int? MinHeight { get; set; }
        public int? MaxHeight { get; set; }
    }
}
