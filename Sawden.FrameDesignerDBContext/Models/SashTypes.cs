﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SashTypes
    {
        public SashTypes()
        {
            SelectionSashTypeLinks = new HashSet<SelectionSashTypeLinks>();
        }

        public int Id { get; set; }
        public string SashType { get; set; }
        public int? ProductsAvailOn { get; set; }
        public bool? CanAdjustHandleHeight { get; set; }
        public string StdLockPosition { get; set; }
        public string DoubleEspagPosition { get; set; }
        public string TripleEspagPosition { get; set; }
        public int? DefaultWidth { get; set; }
        public int? DefaultHeight { get; set; }
        public int? MaxWidth { get; set; }
        public int? MaxHeight { get; set; }
        public int? MinWidth { get; set; }
        public int? MinHeight { get; set; }
        public bool? OpensIn { get; set; }
        public string DefaultOpenDirection { get; set; }
        public bool? AllowOppositeOpen { get; set; }

        public OpenDirection DefaultOpenDirectionNavigation { get; set; }
        public ICollection<SelectionSashTypeLinks> SelectionSashTypeLinks { get; set; }
    }
}
