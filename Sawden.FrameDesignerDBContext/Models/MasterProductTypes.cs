﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class MasterProductTypes
    {
        public MasterProductTypes()
        {
            Styles = new HashSet<Styles>();
            SystemTypes = new HashSet<SystemTypes>();
        }

        public int Id { get; set; }
        public int? DisplaySequence { get; set; }
        public bool? Enabled { get; set; }
        public string Producttype { get; set; }

        public ICollection<Styles> Styles { get; set; }
        public ICollection<SystemTypes> SystemTypes { get; set; }
    }
}
