﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class DoorCylinders
    {
        public int Id { get; set; }
        public int? Qty { get; set; }
        public string Pcode { get; set; }
        public string StockCode { get; set; }
        public string Description { get; set; }
        public int? ProductCatId { get; set; }
        public int? SelectionsLinkId { get; set; }
        public int? PricingMethodId { get; set; }
        public decimal? SellPrice { get; set; }
        public bool? AvailableAsExtra { get; set; }
        public decimal? SellPriceLoose { get; set; }
        public int? CostUom { get; set; }
        public decimal? CostPrice { get; set; }
        public bool? Available { get; set; }
        public bool? OpensIn { get; set; }
    }
}
