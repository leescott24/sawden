﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SelectionSystemTypesLinks
    {
        public int SelectionLinkId { get; set; }
        public int SystemTypeId { get; set; }

        public SelectionLinks SelectionLink { get; set; }
    }
}
