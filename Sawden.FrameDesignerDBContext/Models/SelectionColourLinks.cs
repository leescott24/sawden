﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SelectionColourLinks
    {
        public int SelectionLinkId { get; set; }
        public int ColourGroupId { get; set; }

        public ColourGroups ColourGroup { get; set; }
        public SelectionLinks SelectionLink { get; set; }
    }
}
