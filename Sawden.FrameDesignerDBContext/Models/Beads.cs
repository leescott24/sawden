﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class Beads
    {
        public int Id { get; set; }
        public int ProductCatId { get; set; }
        public string BasePcode { get; set; }
        public string Description { get; set; }
        public int Length { get; set; }
        public int GlassSize { get; set; }
        public int SelectionLinkId { get; set; }

        public ProductCategories ProductCat { get; set; }
        public SelectionLinks SelectionLink { get; set; }
    }
}
