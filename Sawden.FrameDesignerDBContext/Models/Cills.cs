﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class Cills
    {
        public int Id { get; set; }
        public int ProductCatId { get; set; }
        public int? ProductSubCatId { get; set; }
        public string Pcode { get; set; }
        public string Description { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int Addition { get; set; }
        public int? FrameAccTypeId { get; set; }
        public int? SelectionlinkId { get; set; }
        public int? WeldedHornAddition { get; set; }

        public ProductCategories ProductCat { get; set; }
        public ProductSubcategories ProductSubCat { get; set; }
        public SelectionLinks Selectionlink { get; set; }
    }
}
