﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class ProductSubCatDefaultsExploded
    {
        public int SystemTypeId { get; set; }
        public int ColourGroup { get; set; }
        public int ProductCategoryId { get; set; }
        public int? ProductSubCategoryId { get; set; }
        public int DefaultSelectionLinkId { get; set; }
        public int Ident { get; set; }

        public ColourGroups ColourGroupNavigation { get; set; }
        public ProductCategories ProductCategory { get; set; }
        public ProductSubcategories ProductSubCategory { get; set; }
    }
}
