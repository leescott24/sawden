﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SpecialsBreakdown
    {
        public int Id { get; set; }
        public string ShapedFrameType { get; set; }
        public int? ProductCategoryId { get; set; }
        public int? ProductSubCatId { get; set; }
        public string Subtype { get; set; }
        public int? PricingMethodId { get; set; }
        public decimal SellPriceStg { get; set; }
        public decimal? SellPriceEuro { get; set; }
        public int? CostUom { get; set; }
        public decimal? Cost { get; set; }
        public string ProductCode { get; set; }
        public bool? Available { get; set; }
        public int? QtySubType { get; set; }
    }
}
