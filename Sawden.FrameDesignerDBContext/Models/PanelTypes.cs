﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class PanelTypes
    {
        public int Id { get; set; }
        public string PanelType { get; set; }
    }
}
