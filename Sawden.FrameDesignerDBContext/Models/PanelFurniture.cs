﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class PanelFurniture
    {
        public int Id { get; set; }
        public int Qty { get; set; }
        public string Pcode { get; set; }
        public string StockCode { get; set; }
        public string SellLooseCode { get; set; }
        public string FurnitureDesc { get; set; }
        public int FurnitureType { get; set; }
        public int? PricingMethod { get; set; }
        public int? SellPriceFitted { get; set; }
        public int? SellPriceLoose { get; set; }
        public int? SelectionsLinkId { get; set; }
    }
}
