﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class TransomsAndMullions
    {
        public TransomsAndMullions()
        {
            SystemTypes = new HashSet<SystemTypes>();
        }

        public int Id { get; set; }
        public int ProductCatId { get; set; }
        public string Pcode { get; set; }
        public string ShortCode { get; set; }
        public string Description { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int RebateExt { get; set; }
        public int RebateInt { get; set; }
        public int Glass { get; set; }
        public int Sash { get; set; }
        public bool IsEurogroove { get; set; }
        public int? ReinforcingTypeId { get; set; }
        public int? TransomType { get; set; }

        public ProductCategories ProductCat { get; set; }
        public ICollection<SystemTypes> SystemTypes { get; set; }
    }
}
