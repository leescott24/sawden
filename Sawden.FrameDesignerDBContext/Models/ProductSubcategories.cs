﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class ProductSubcategories
    {
        public ProductSubcategories()
        {
            Cills = new HashSet<Cills>();
            Hinges = new HashSet<Hinges>();
            Locks = new HashSet<Locks>();
            ProductSubCatDefaultsExploded = new HashSet<ProductSubCatDefaultsExploded>();
        }

        public int Id { get; set; }
        public string SubCatDesc { get; set; }
        public int? ProdCatId { get; set; }

        public ICollection<Cills> Cills { get; set; }
        public ICollection<Hinges> Hinges { get; set; }
        public ICollection<Locks> Locks { get; set; }
        public ICollection<ProductSubCatDefaultsExploded> ProductSubCatDefaultsExploded { get; set; }
    }
}
