﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SelectionProdCatLinks
    {
        public int SystemLinkId { get; set; }
        public int ProductCategoryLinkId { get; set; }
    }
}
