﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class BayDeductions
    {
        public int Id { get; set; }
        public int? From { get; set; }
        public int? To { get; set; }
        public int? Deduction { get; set; }
        public string TypeId { get; set; }
    }
}
