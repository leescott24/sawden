﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class Styles
    {
        public string StyleType { get; set; }
        public int StyleId { get; set; }
        public int? NoLights { get; set; }
        public int? SashCount { get; set; }
        public int? FixedCount { get; set; }
        public string Coord1 { get; set; }
        public string Coord2 { get; set; }
        public string StyleDesc { get; set; }
        public int? PrcGroup { get; set; }
        public int? SequenceNo { get; set; }
        public short? WindowGroup { get; set; }
        public int? BatchGrp { get; set; }
        public byte GroupNumber { get; set; }
        public bool NightShiftStyle { get; set; }
        public bool WebUse { get; set; }
        public byte? PrepWeld { get; set; }
        public short? AltGrpNumber { get; set; }
        public byte? AluGrpNumber { get; set; }
        public string WeldingOpt { get; set; }
        public bool IsGlassFitted { get; set; }
        public string AliAltWeldingOpt { get; set; }
        public byte SlaveSash { get; set; }
        public string AltHangingOpt { get; set; }
        public string SashHanding { get; set; }
        public int NoHwelds { get; set; }
        public int? BatchingGroupNo { get; set; }
        public int NoPanes { get; set; }
        public int NoFullPanel { get; set; }
        public int NoHalfPanel { get; set; }
        public int OutsideViewChecked { get; set; }
        public bool CrucifixWelder { get; set; }
        public bool SashHorns { get; set; }
        public int? MasterProductTypeId { get; set; }

        public MasterProductTypes MasterProductType { get; set; }
    }
}
