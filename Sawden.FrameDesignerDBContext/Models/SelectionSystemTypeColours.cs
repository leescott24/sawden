﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SelectionSystemTypeColours
    {
        public int ColourId { get; set; }
        public int SystemId { get; set; }

        public Colours Colour { get; set; }
        public SystemTypes System { get; set; }
    }
}
