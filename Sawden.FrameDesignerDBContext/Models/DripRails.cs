﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class DripRails
    {
        public int Id { get; set; }
        public string DripRail { get; set; }
        public int? SelectionLinkId { get; set; }
        public int? ProdCatId { get; set; }
    }
}
