﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SelectPanelMasterFurnitureLinks
    {
        public int PanelMasterId { get; set; }
        public int PanelFurnitureId { get; set; }
    }
}
