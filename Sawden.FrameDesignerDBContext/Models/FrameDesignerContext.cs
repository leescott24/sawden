﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class FrameDesignerContext : DbContext
    {
        public FrameDesignerContext()
        {
        }

        public FrameDesignerContext(DbContextOptions<FrameDesignerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<BayDeductions> BayDeductions { get; set; }
        public virtual DbSet<BayPoleParts> BayPoleParts { get; set; }
        public virtual DbSet<BayPoles> BayPoles { get; set; }
        public virtual DbSet<Beads> Beads { get; set; }
        public virtual DbSet<Cills> Cills { get; set; }
        public virtual DbSet<ColourGroups> ColourGroups { get; set; }
        public virtual DbSet<Colours> Colours { get; set; }
        public virtual DbSet<CompositeColourGroups> CompositeColourGroups { get; set; }
        public virtual DbSet<CompositeMasterGroups> CompositeMasterGroups { get; set; }
        public virtual DbSet<CompositeTypes> CompositeTypes { get; set; }
        public virtual DbSet<CostUom> CostUom { get; set; }
        public virtual DbSet<DecoGlassCodes> DecoGlassCodes { get; set; }
        public virtual DbSet<DeleteCaravanTrims> DeleteCaravanTrims { get; set; }
        public virtual DbSet<DoorCylinders> DoorCylinders { get; set; }
        public virtual DbSet<DrainageTypes> DrainageTypes { get; set; }
        public virtual DbSet<DripRails> DripRails { get; set; }
        public virtual DbSet<ErrorsAndWarnings> ErrorsAndWarnings { get; set; }
        public virtual DbSet<Extras> Extras { get; set; }
        public virtual DbSet<ExtrasCategories> ExtrasCategories { get; set; }
        public virtual DbSet<FrameAccessories> FrameAccessories { get; set; }
        public virtual DbSet<FrameAccessoryTypes> FrameAccessoryTypes { get; set; }
        public virtual DbSet<GeoDuplexLead> GeoDuplexLead { get; set; }
        public virtual DbSet<Handles> Handles { get; set; }
        public virtual DbSet<Hinges> Hinges { get; set; }
        public virtual DbSet<Jobtypes> Jobtypes { get; set; }
        public virtual DbSet<LinkedProducts> LinkedProducts { get; set; }
        public virtual DbSet<Locks> Locks { get; set; }
        public virtual DbSet<MasterProductTypes> MasterProductTypes { get; set; }
        public virtual DbSet<NightVents> NightVents { get; set; }
        public virtual DbSet<OpenDirection> OpenDirection { get; set; }
        public virtual DbSet<OuterFrames> OuterFrames { get; set; }
        public virtual DbSet<PanelColourgroups> PanelColourgroups { get; set; }
        public virtual DbSet<PanelColours> PanelColours { get; set; }
        public virtual DbSet<PanelFurniture> PanelFurniture { get; set; }
        public virtual DbSet<PanelFurnitureTypes> PanelFurnitureTypes { get; set; }
        public virtual DbSet<PanelGlassbordercols> PanelGlassbordercols { get; set; }
        public virtual DbSet<PanelGlasscodes> PanelGlasscodes { get; set; }
        public virtual DbSet<PanelGlassTileColours> PanelGlassTileColours { get; set; }
        public virtual DbSet<PanelGlassTypes> PanelGlassTypes { get; set; }
        public virtual DbSet<Panelmastergroups> Panelmastergroups { get; set; }
        public virtual DbSet<PanelThickness> PanelThickness { get; set; }
        public virtual DbSet<PanelTypes> PanelTypes { get; set; }
        public virtual DbSet<Parameters> Parameters { get; set; }
        public virtual DbSet<PartTranslations> PartTranslations { get; set; }
        public virtual DbSet<PricingMethod> PricingMethod { get; set; }
        public virtual DbSet<ProductCategories> ProductCategories { get; set; }
        public virtual DbSet<ProductSubCatDefaults> ProductSubCatDefaults { get; set; }
        public virtual DbSet<ProductSubCatDefaultsExploded> ProductSubCatDefaultsExploded { get; set; }
        public virtual DbSet<ProductSubcategories> ProductSubcategories { get; set; }
        public virtual DbSet<ReinforcingGroups> ReinforcingGroups { get; set; }
        public virtual DbSet<ReinforcingParts> ReinforcingParts { get; set; }
        public virtual DbSet<ReinforcingRules> ReinforcingRules { get; set; }
        public virtual DbSet<Sashes> Sashes { get; set; }
        public virtual DbSet<SashExtras> SashExtras { get; set; }
        public virtual DbSet<SashTypes> SashTypes { get; set; }
        public virtual DbSet<SelectionColourLinks> SelectionColourLinks { get; set; }
        public virtual DbSet<SelectionLinks> SelectionLinks { get; set; }
        public virtual DbSet<SelectionOuterPriceExclude> SelectionOuterPriceExclude { get; set; }
        public virtual DbSet<SelectionPanelGlassCodeBorders> SelectionPanelGlassCodeBorders { get; set; }
        public virtual DbSet<SelectionPanelGlassCodeTiles> SelectionPanelGlassCodeTiles { get; set; }
        public virtual DbSet<SelectionPanelMasterGlassCodes> SelectionPanelMasterGlassCodes { get; set; }
        public virtual DbSet<SelectionProdCatLinks> SelectionProdCatLinks { get; set; }
        public virtual DbSet<SelectionSashTypeLinks> SelectionSashTypeLinks { get; set; }
        public virtual DbSet<SelectionSystemTypeColours> SelectionSystemTypeColours { get; set; }
        public virtual DbSet<SelectionSystemTypesLinks> SelectionSystemTypesLinks { get; set; }
        public virtual DbSet<SelectPanelMasterFurnitureLinks> SelectPanelMasterFurnitureLinks { get; set; }
        public virtual DbSet<SpacerBar> SpacerBar { get; set; }
        public virtual DbSet<SpacerBarMeasurements> SpacerBarMeasurements { get; set; }
        public virtual DbSet<SpecialsBreakdown> SpecialsBreakdown { get; set; }
        public virtual DbSet<SprayingOptions> SprayingOptions { get; set; }
        public virtual DbSet<Styles> Styles { get; set; }
        public virtual DbSet<SubFrameJointTypes> SubFrameJointTypes { get; set; }
        public virtual DbSet<SubFrameParts> SubFrameParts { get; set; }
        public virtual DbSet<SubFrames> SubFrames { get; set; }
        public virtual DbSet<SystemTypes> SystemTypes { get; set; }
        public virtual DbSet<ThresholdTypes> ThresholdTypes { get; set; }
        public virtual DbSet<TransomsAndMullions> TransomsAndMullions { get; set; }

        // Unable to generate entity type for table 'dbo.CompositeColours'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.CompositeFurniture'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PanelCompoGlassCuttingtypes'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.CompoSlabTypes'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.CompositeGlasstypes'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.LockPositions'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Compoglasscodes'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Articles'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.GeobarTypes'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.AstrigalBars'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ProductsWithNoSubProd'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ProductCatPartTypes'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=FrameDesigner;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BayDeductions>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.TypeId).HasMaxLength(50);
            });

            modelBuilder.Entity<BayPoleParts>(entity =>
            {
                entity.HasKey(e => e.Pcode);

                entity.Property(e => e.Pcode)
                    .HasMaxLength(20)
                    .ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.PricingMethodId).HasColumnName("PricingMethodID");

                entity.Property(e => e.StockCode).HasMaxLength(20);
            });

            modelBuilder.Entity<BayPoles>(entity =>
            {
                entity.HasKey(e => e.BasePcode);

                entity.Property(e => e.BasePcode)
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.SystemTypesAvailOn).HasMaxLength(50);

                entity.HasOne(d => d.ProductCat)
                    .WithMany(p => p.BayPoles)
                    .HasForeignKey(d => d.ProductCatId)
                    .HasConstraintName("FK_BayPoles_ProductCategories");
            });

            modelBuilder.Entity<Beads>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BasePcode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.ProductCat)
                    .WithMany(p => p.Beads)
                    .HasForeignKey(d => d.ProductCatId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Beads_ProductCategories1");

                entity.HasOne(d => d.SelectionLink)
                    .WithMany(p => p.Beads)
                    .HasForeignKey(d => d.SelectionLinkId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Beads_SelectionLinks");
            });

            modelBuilder.Entity<Cills>(entity =>
            {
                entity.HasKey(e => e.Pcode);

                entity.Property(e => e.Pcode)
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.HasOne(d => d.ProductCat)
                    .WithMany(p => p.Cills)
                    .HasForeignKey(d => d.ProductCatId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Cills_ProductCategories");

                entity.HasOne(d => d.ProductSubCat)
                    .WithMany(p => p.Cills)
                    .HasForeignKey(d => d.ProductSubCatId)
                    .HasConstraintName("FK_Cills_ProductSubcategories");

                entity.HasOne(d => d.Selectionlink)
                    .WithMany(p => p.Cills)
                    .HasForeignKey(d => d.SelectionlinkId)
                    .HasConstraintName("FK_Cills_SelectionLinks");
            });

            modelBuilder.Entity<ColourGroups>(entity =>
            {
                entity.Property(e => e.Colourgroupdesc)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Colours>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Colour)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DefaultBeadColourId).HasMaxLength(50);

                entity.Property(e => e.ForeColour)
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.Geopanel).HasColumnName("GEOPANEL");

                entity.Property(e => e.VisualColour)
                    .IsRequired()
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.HasOne(d => d.ColourGrpNavigation)
                    .WithMany(p => p.Colours)
                    .HasForeignKey(d => d.ColourGrp)
                    .HasConstraintName("FK_Colours_ColourGroups");
            });

            modelBuilder.Entity<CompositeColourGroups>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CompoColourGroup).HasMaxLength(50);
            });

            modelBuilder.Entity<CompositeMasterGroups>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Compocode).HasMaxLength(10);

                entity.Property(e => e.Mastercompotype)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<CompositeTypes>(entity =>
            {
                entity.HasKey(e => e.Compositetype);

                entity.Property(e => e.Compositetype)
                    .HasMaxLength(30)
                    .ValueGeneratedNever();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<CostUom>(entity =>
            {
                entity.ToTable("CostUOM");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CostUom1)
                    .HasColumnName("CostUOM")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<DecoGlassCodes>(entity =>
            {
                entity.HasKey(e => e.Pcode);

                entity.Property(e => e.Pcode)
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.GlassBordersAvailId).HasColumnName("GLassBordersAvailID");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.PricingMethodId).HasColumnName("PricingMethodID");
            });

            modelBuilder.Entity<DeleteCaravanTrims>(entity =>
            {
                entity.HasKey(e => e.Pcode);

                entity.ToTable("____Delete___CaravanTrims");

                entity.Property(e => e.Pcode)
                    .HasColumnName("PCode")
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.StockCode).HasMaxLength(10);
            });

            modelBuilder.Entity<DoorCylinders>(entity =>
            {
                entity.HasKey(e => e.Pcode);

                entity.Property(e => e.Pcode)
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.CostUom).HasColumnName("CostUOM");

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.StockCode).HasMaxLength(50);
            });

            modelBuilder.Entity<DrainageTypes>(entity =>
            {
                entity.HasKey(e => e.DraingeType);

                entity.Property(e => e.DraingeType)
                    .HasMaxLength(30)
                    .ValueGeneratedNever();

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.RoutingCode).HasMaxLength(50);
            });

            modelBuilder.Entity<DripRails>(entity =>
            {
                entity.Property(e => e.DripRail).HasMaxLength(50);
            });

            modelBuilder.Entity<ErrorsAndWarnings>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ReturnInfo)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Extras>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Pcode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ShortCode)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ExtrasCategories>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description).HasMaxLength(50);
            });

            modelBuilder.Entity<FrameAccessories>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BasePcode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.SystemTypesAvailableOn).HasMaxLength(50);
            });

            modelBuilder.Entity<FrameAccessoryTypes>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Type).HasMaxLength(30);
            });

            modelBuilder.Entity<GeoDuplexLead>(entity =>
            {
                entity.HasKey(e => e.Pcode);

                entity.Property(e => e.Pcode)
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.ColourId)
                    .HasColumnName("ColourID")
                    .HasMaxLength(50);

                entity.Property(e => e.ColoursAvailbeOn).HasMaxLength(50);

                entity.Property(e => e.CostUom).HasColumnName("CostUOM");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.StockCode).HasMaxLength(50);
            });

            modelBuilder.Entity<Handles>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ChargeOnColourGroups).HasMaxLength(50);

                entity.Property(e => e.CostOumId).HasColumnName("CostOumID");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Pcode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ProductCatId).HasColumnName("ProductCatID");

                entity.Property(e => e.ProductSubCatId).HasColumnName("ProductSubCatID");

                entity.Property(e => e.ShortCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.ProductCat)
                    .WithMany(p => p.Handles)
                    .HasForeignKey(d => d.ProductCatId)
                    .HasConstraintName("FK_Handles_ProductCategories");

                entity.HasOne(d => d.SelectionLink)
                    .WithMany(p => p.Handles)
                    .HasForeignKey(d => d.SelectionLinkId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Handles_SelectionLinks");
            });

            modelBuilder.Entity<Hinges>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ChargeOnColourGroups).HasMaxLength(50);

                entity.Property(e => e.CostPrice).HasColumnName("Cost Price");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Pcode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PricingMethodId).HasColumnName("PricingMethodID");

                entity.Property(e => e.RoutingCode).HasMaxLength(20);

                entity.Property(e => e.SellPrice).HasColumnName("Sell Price");

                entity.Property(e => e.ShortCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.ProductCat)
                    .WithMany(p => p.Hinges)
                    .HasForeignKey(d => d.ProductCatId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Hinges_ProductCategories");

                entity.HasOne(d => d.ProductSubCat)
                    .WithMany(p => p.Hinges)
                    .HasForeignKey(d => d.ProductSubCatId)
                    .HasConstraintName("FK_Hinges_ProductSubcategories");

                entity.HasOne(d => d.SelectionLink)
                    .WithMany(p => p.Hinges)
                    .HasForeignKey(d => d.SelectionLinkId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Hinges_SelectionLinks");
            });

            modelBuilder.Entity<Jobtypes>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.JobType).HasMaxLength(100);
            });

            modelBuilder.Entity<LinkedProducts>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BasePcode).HasMaxLength(30);

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.LinkedPcode).HasMaxLength(30);

                entity.Property(e => e.LinkedPcodeStockCode).HasMaxLength(30);
            });

            modelBuilder.Entity<Locks>(entity =>
            {
                entity.HasKey(e => e.Pcode);

                entity.Property(e => e.Pcode)
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.CostUomId).HasColumnName("CostUomID");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.RoutingCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.StockCode).HasMaxLength(50);

                entity.HasOne(d => d.ProductCat)
                    .WithMany(p => p.Locks)
                    .HasForeignKey(d => d.ProductCatId)
                    .HasConstraintName("FK_Locks_ProductCategories");

                entity.HasOne(d => d.ProductSubCat)
                    .WithMany(p => p.Locks)
                    .HasForeignKey(d => d.ProductSubCatId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Locks_ProductSubcategories");

                entity.HasOne(d => d.SelectionLink)
                    .WithMany(p => p.Locks)
                    .HasForeignKey(d => d.SelectionLinkId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Locks_SelectionLinks1");
            });

            modelBuilder.Entity<MasterProductTypes>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Enabled)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Producttype)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<NightVents>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.MinPieceLength).HasColumnName("MinPiece Length");

                entity.Property(e => e.Pcode).HasMaxLength(20);

                entity.Property(e => e.RoutingCode).HasMaxLength(30);

                entity.Property(e => e.StockCode1).HasMaxLength(20);

                entity.Property(e => e.StockCode2).HasMaxLength(20);

                entity.HasOne(d => d.ProductCat)
                    .WithMany(p => p.NightVents)
                    .HasForeignKey(d => d.ProductCatId)
                    .HasConstraintName("FK_TrickleVents_ProductCategories");
            });

            modelBuilder.Entity<OpenDirection>(entity =>
            {
                entity.HasKey(e => e.OpenDirection1);

                entity.Property(e => e.OpenDirection1)
                    .HasColumnName("OpenDirection")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .ValueGeneratedNever();
            });

            modelBuilder.Entity<OuterFrames>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Pcode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ReversePcode).HasMaxLength(50);

                entity.Property(e => e.StockCode).HasMaxLength(50);

                entity.Property(e => e.SystemTypesExcludePricing).HasMaxLength(50);

                entity.HasOne(d => d.Selection)
                    .WithMany(p => p.OuterFrames)
                    .HasForeignKey(d => d.Selectionid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OuterFrames_SelectionLinks");
            });

            modelBuilder.Entity<PanelColourgroups>(entity =>
            {
                entity.Property(e => e.Colourgroupdesc)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PanelColours>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Colourdesc)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PanelFurniture>(entity =>
            {
                entity.HasKey(e => e.Pcode);

                entity.Property(e => e.Pcode)
                    .HasMaxLength(20)
                    .ValueGeneratedNever();

                entity.Property(e => e.FurnitureDesc)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.SellLooseCode).HasMaxLength(20);

                entity.Property(e => e.StockCode).HasMaxLength(20);
            });

            modelBuilder.Entity<PanelFurnitureTypes>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.PanelTypeAvailableOn).HasMaxLength(50);

                entity.Property(e => e.TypeDesc)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<PanelGlassbordercols>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Colourdesc).HasMaxLength(50);

                entity.Property(e => e.Default).HasColumnName("default");
            });

            modelBuilder.Entity<PanelGlasscodes>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Alsoallowedongroupid).HasMaxLength(50);

                entity.Property(e => e.GlassbordersavailId)
                    .HasColumnName("GlassbordersavailID")
                    .HasMaxLength(50);

                entity.Property(e => e.Glasscode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PanelglassShapeid).HasMaxLength(50);

                entity.Property(e => e.Sellprice24and28mm).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.Sellprice40mm).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.TileColoursAvailable).HasMaxLength(50);
            });

            modelBuilder.Entity<PanelGlassTileColours>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.TileColour)
                    .HasColumnName("Tile Colour")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<PanelGlassTypes>(entity =>
            {
                entity.HasKey(e => e.SubtypeCode);

                entity.Property(e => e.SubtypeCode)
                    .HasMaxLength(10)
                    .ValueGeneratedNever();

                entity.Property(e => e.Decoglasstype)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Mastersubtype).HasMaxLength(3);
            });

            modelBuilder.Entity<Panelmastergroups>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Panelgroupdesc)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<PanelThickness>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Pcode)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.ThicknessDesc)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<PanelTypes>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.PanelType)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<Parameters>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");
            });

            modelBuilder.Entity<PartTranslations>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Basepcode).HasMaxLength(20);

                entity.Property(e => e.BayPartsIds).HasMaxLength(20);

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.Pcode).HasMaxLength(20);

                entity.Property(e => e.Stockcode).HasMaxLength(20);
            });

            modelBuilder.Entity<PricingMethod>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.PricingMethod1)
                    .HasColumnName("PricingMethod")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ProductCategories>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Table)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.TableName).HasMaxLength(100);
            });

            modelBuilder.Entity<ProductSubCatDefaults>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ColourCatId).HasMaxLength(50);

                entity.Property(e => e.DefaultSelectionLinkId).HasMaxLength(50);

                entity.Property(e => e.SystemTypeId).HasMaxLength(50);
            });

            modelBuilder.Entity<ProductSubCatDefaultsExploded>(entity =>
            {
                entity.HasKey(e => e.Ident);

                entity.ToTable("ProductSubCatDefaults_Exploded");

                entity.Property(e => e.DefaultSelectionLinkId).HasColumnName("DefaultSelectionLinkID");

                entity.Property(e => e.ProductCategoryId).HasColumnName("ProductCategoryID");

                entity.Property(e => e.ProductSubCategoryId).HasColumnName("ProductSubCategoryID");

                entity.Property(e => e.SystemTypeId).HasColumnName("SystemTypeID");

                entity.HasOne(d => d.ColourGroupNavigation)
                    .WithMany(p => p.ProductSubCatDefaultsExploded)
                    .HasForeignKey(d => d.ColourGroup)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductSubCatDefaults_Exploded_ColourGroups");

                entity.HasOne(d => d.ProductCategory)
                    .WithMany(p => p.ProductSubCatDefaultsExploded)
                    .HasForeignKey(d => d.ProductCategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductSubCatDefaults_Exploded_ProductCategories");

                entity.HasOne(d => d.ProductSubCategory)
                    .WithMany(p => p.ProductSubCatDefaultsExploded)
                    .HasForeignKey(d => d.ProductSubCategoryId)
                    .HasConstraintName("FK_ProductSubCatDefaults_Exploded_ProductSubcategories");
            });

            modelBuilder.Entity<ProductSubcategories>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.SubCatDesc).HasMaxLength(50);
            });

            modelBuilder.Entity<ReinforcingGroups>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ReoGroupName).HasMaxLength(50);
            });

            modelBuilder.Entity<ReinforcingParts>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CostUomId).HasColumnName("CostUomID");

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.LabelCharacter).HasMaxLength(10);

                entity.Property(e => e.ProductCatId).HasMaxLength(10);

                entity.Property(e => e.ProductCode).HasMaxLength(10);
            });

            modelBuilder.Entity<ReinforcingRules>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ColourReoId)
                    .HasColumnName("ColourReoID")
                    .HasMaxLength(50);

                entity.Property(e => e.MasterProductType).HasMaxLength(50);
            });

            modelBuilder.Entity<Sashes>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Pcode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ReinforcingGroupId).HasColumnName("ReinforcingGroupID");

                entity.Property(e => e.ShortCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.ProductCat)
                    .WithMany(p => p.Sashes)
                    .HasForeignKey(d => d.ProductCatId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Sashes_ProductCategories");

                entity.HasOne(d => d.SelectionLink)
                    .WithMany(p => p.Sashes)
                    .HasForeignKey(d => d.SelectionLinkid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Sashes_SelectionLinks");
            });

            modelBuilder.Entity<SashExtras>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CostUomId).HasColumnName("CostUomID");

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.Pcode).HasMaxLength(20);

                entity.Property(e => e.PricingMethodId).HasColumnName("PricingMethodID");

                entity.Property(e => e.ProductSubCatId).HasColumnName("ProductSubCatID");

                entity.Property(e => e.RoutingCode).HasMaxLength(20);

                entity.Property(e => e.StockCode).HasMaxLength(20);
            });

            modelBuilder.Entity<SashTypes>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DefaultOpenDirection)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DoubleEspagPosition).HasMaxLength(50);

                entity.Property(e => e.SashType).HasMaxLength(50);

                entity.Property(e => e.StdLockPosition).HasMaxLength(50);

                entity.Property(e => e.TripleEspagPosition).HasMaxLength(50);

                entity.HasOne(d => d.DefaultOpenDirectionNavigation)
                    .WithMany(p => p.SashTypes)
                    .HasForeignKey(d => d.DefaultOpenDirection)
                    .HasConstraintName("FK_SashTypes_OpenDirection");
            });

            modelBuilder.Entity<SelectionColourLinks>(entity =>
            {
                entity.HasKey(e => new { e.SelectionLinkId, e.ColourGroupId });

                entity.Property(e => e.SelectionLinkId).HasColumnName("SelectionLinkID");

                entity.Property(e => e.ColourGroupId).HasColumnName("ColourGroupID");

                entity.HasOne(d => d.ColourGroup)
                    .WithMany(p => p.SelectionColourLinks)
                    .HasForeignKey(d => d.ColourGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Selection_ColourGroups_ColourGroups");

                entity.HasOne(d => d.SelectionLink)
                    .WithMany(p => p.SelectionColourLinks)
                    .HasForeignKey(d => d.SelectionLinkId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Selection_ColourGroups_SelectionLinks");
            });

            modelBuilder.Entity<SelectionLinks>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ColourGroupAvailOnId).HasMaxLength(50);

                entity.Property(e => e.ColourReinforcingIdAvail).HasMaxLength(50);

                entity.Property(e => e.ProductSubCatId).HasColumnName("ProductSubCatID");

                entity.Property(e => e.SashTypeAvailOnId).HasMaxLength(50);

                entity.Property(e => e.SelectionLinkDesc).HasMaxLength(200);

                entity.Property(e => e.SellPrice).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.SystemTypeAvailOnId).HasMaxLength(50);

                entity.Property(e => e._2ndtablelink)
                    .HasColumnName("2ndtablelink")
                    .HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<SelectionOuterPriceExclude>(entity =>
            {
                entity.HasKey(e => new { e.OuterId, e.SystemId });
            });

            modelBuilder.Entity<SelectionPanelGlassCodeBorders>(entity =>
            {
                entity.HasKey(e => new { e.GlassCodeId, e.BorderColId });
            });

            modelBuilder.Entity<SelectionPanelGlassCodeTiles>(entity =>
            {
                entity.HasKey(e => new { e.GlassCodeId, e.TileColurId });
            });

            modelBuilder.Entity<SelectionPanelMasterGlassCodes>(entity =>
            {
                entity.HasKey(e => new { e.GlassCodeId, e.MasterGroupId });
            });

            modelBuilder.Entity<SelectionProdCatLinks>(entity =>
            {
                entity.HasKey(e => new { e.SystemLinkId, e.ProductCategoryLinkId });
            });

            modelBuilder.Entity<SelectionSashTypeLinks>(entity =>
            {
                entity.HasKey(e => new { e.SelectionLinkId, e.SashTypeId });

                entity.Property(e => e.SelectionLinkId).HasColumnName("SelectionLinkID");

                entity.Property(e => e.SashTypeId).HasColumnName("SashTypeID");

                entity.HasOne(d => d.SashType)
                    .WithMany(p => p.SelectionSashTypeLinks)
                    .HasForeignKey(d => d.SashTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Selection_SashTypes_SashTypes");

                entity.HasOne(d => d.SelectionLink)
                    .WithMany(p => p.SelectionSashTypeLinks)
                    .HasForeignKey(d => d.SelectionLinkId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Selection_SashTypes_SelectionLinks");
            });

            modelBuilder.Entity<SelectionSystemTypeColours>(entity =>
            {
                entity.HasKey(e => new { e.ColourId, e.SystemId });

                entity.Property(e => e.ColourId).HasColumnName("ColourID");

                entity.Property(e => e.SystemId).HasColumnName("SystemID");

                entity.HasOne(d => d.Colour)
                    .WithMany(p => p.SelectionSystemTypeColours)
                    .HasForeignKey(d => d.ColourId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Selection_SystemColours_Colours");

                entity.HasOne(d => d.System)
                    .WithMany(p => p.SelectionSystemTypeColours)
                    .HasForeignKey(d => d.SystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Selection_SystemColours_SystemTypes");
            });

            modelBuilder.Entity<SelectionSystemTypesLinks>(entity =>
            {
                entity.HasKey(e => new { e.SelectionLinkId, e.SystemTypeId });

                entity.Property(e => e.SelectionLinkId).HasColumnName("SelectionLinkID");

                entity.Property(e => e.SystemTypeId).HasColumnName("SystemTypeID");

                entity.HasOne(d => d.SelectionLink)
                    .WithMany(p => p.SelectionSystemTypesLinks)
                    .HasForeignKey(d => d.SelectionLinkId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Selection_SystemTypes_SelectionLinks");
            });

            modelBuilder.Entity<SelectPanelMasterFurnitureLinks>(entity =>
            {
                entity.HasKey(e => new { e.PanelMasterId, e.PanelFurnitureId });
            });

            modelBuilder.Entity<SpacerBar>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.Pcode).HasMaxLength(50);
            });

            modelBuilder.Entity<SpacerBarMeasurements>(entity =>
            {
                entity.HasKey(e => new { e.SpacerBarId, e.Measurement });
            });

            modelBuilder.Entity<SpecialsBreakdown>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Available)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Cost).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.CostUom).HasColumnName("CostUOM");

                entity.Property(e => e.ProductCode).HasMaxLength(50);

                entity.Property(e => e.SellPriceEuro).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.SellPriceStg)
                    .HasColumnName("SellPriceSTG")
                    .HasColumnType("numeric(18, 2)");

                entity.Property(e => e.ShapedFrameType)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Subtype)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SprayingOptions>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.PricingMethodId).HasColumnName("PricingMethodID");

                entity.Property(e => e.Spraytype).HasMaxLength(20);
            });

            modelBuilder.Entity<Styles>(entity =>
            {
                entity.HasKey(e => new { e.StyleType, e.StyleId });

                entity.Property(e => e.StyleType)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.StyleId).HasColumnName("StyleID");

                entity.Property(e => e.AliAltWeldingOpt)
                    .HasColumnName("AliAltWeldingOPT")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AltHangingOpt)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Coord1)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Coord2)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.IsGlassFitted).HasColumnName("isGlassFitted");

                entity.Property(e => e.MasterProductTypeId).HasColumnName("MasterProductTypeID");

                entity.Property(e => e.NoHwelds).HasColumnName("NoHWelds");

                entity.Property(e => e.SashHanding)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.StyleDesc)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WeldingOpt)
                    .HasColumnName("WeldingOPT")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.MasterProductType)
                    .WithMany(p => p.Styles)
                    .HasForeignKey(d => d.MasterProductTypeId)
                    .HasConstraintName("FK_Styles_MasterProductTypes");
            });

            modelBuilder.Entity<SubFrameJointTypes>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.JointType).HasMaxLength(100);
            });

            modelBuilder.Entity<SubFrameParts>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Pcode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.StockCode).HasMaxLength(50);

                entity.HasOne(d => d.ProductCat)
                    .WithMany(p => p.SubFrameParts)
                    .HasForeignKey(d => d.ProductCatId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SubFrameParts_ProductCategories");
            });

            modelBuilder.Entity<SubFrames>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.PcodeBottom)
                    .HasColumnName("PCodeBottom")
                    .HasMaxLength(20);

                entity.Property(e => e.PcodeLeft)
                    .HasColumnName("PCode Left")
                    .HasMaxLength(20);

                entity.Property(e => e.PcodeRight)
                    .HasColumnName("PCode Right")
                    .HasMaxLength(20);

                entity.Property(e => e.PcodeTop)
                    .HasColumnName("PCodeTop")
                    .HasMaxLength(20);

                entity.HasOne(d => d.ProductCat)
                    .WithMany(p => p.SubFrames)
                    .HasForeignKey(d => d.ProductCatId)
                    .HasConstraintName("FK_SubFrames_ProductCategories");
            });

            modelBuilder.Entity<SystemTypes>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CategoriesAllowedDefault).HasMaxLength(50);

                entity.Property(e => e.DefaultOpenDirection)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('O')");

                entity.Property(e => e.NominalCode).HasMaxLength(50);

                entity.Property(e => e.SubType).HasMaxLength(50);

                entity.Property(e => e.Systemtype)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.DefaultOpenDirectionNavigation)
                    .WithMany(p => p.SystemTypes)
                    .HasForeignKey(d => d.DefaultOpenDirection)
                    .HasConstraintName("FK_SystemTypes_OpenDirection");

                entity.HasOne(d => d.DefaultOuterFrame)
                    .WithMany(p => p.SystemTypes)
                    .HasForeignKey(d => d.DefaultOuterFrameId)
                    .HasConstraintName("FK_SystemTypes_OuterFrames");

                entity.HasOne(d => d.DefaultSash)
                    .WithMany(p => p.SystemTypes)
                    .HasForeignKey(d => d.DefaultSashId)
                    .HasConstraintName("FK_SystemTypes_Sashes");

                entity.HasOne(d => d.DefaultTransom)
                    .WithMany(p => p.SystemTypes)
                    .HasForeignKey(d => d.DefaultTransomId)
                    .HasConstraintName("FK_SystemTypes_TransomsAndMullions");

                entity.HasOne(d => d.Masterproductlink)
                    .WithMany(p => p.SystemTypes)
                    .HasForeignKey(d => d.Masterproductlinkid)
                    .HasConstraintName("FK_SystemTypes_MasterProductTypes");
            });

            modelBuilder.Entity<ThresholdTypes>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description).HasMaxLength(50);
            });

            modelBuilder.Entity<TransomsAndMullions>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Pcode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ReinforcingTypeId).HasColumnName("ReinforcingTypeID");

                entity.Property(e => e.ShortCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.ProductCat)
                    .WithMany(p => p.TransomsAndMullions)
                    .HasForeignKey(d => d.ProductCatId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TransomsAndMullions_ProductCategories");
            });
        }
    }
}
