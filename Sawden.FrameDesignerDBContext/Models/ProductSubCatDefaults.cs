﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class ProductSubCatDefaults
    {
        public int Id { get; set; }
        public string SystemTypeId { get; set; }
        public int? ProductCategoryId { get; set; }
        public int? ProductsubCategoryId { get; set; }
        public string DefaultSelectionLinkId { get; set; }
        public string ColourCatId { get; set; }
    }
}
