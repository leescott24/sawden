﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class BayPoleParts
    {
        public int Id { get; set; }
        public int? ColourId { get; set; }
        public string Pcode { get; set; }
        public string StockCode { get; set; }
        public string Description { get; set; }
        public int? PricingMethodId { get; set; }
        public int? SellPrice { get; set; }
        public bool? Available { get; set; }
        public int? JackDeduction { get; set; }
        public int? Length { get; set; }
        public bool? AvailableAsExtra { get; set; }
    }
}
