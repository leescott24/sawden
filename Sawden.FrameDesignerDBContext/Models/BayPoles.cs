﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class BayPoles
    {
        public int Id { get; set; }
        public int? ProductCatId { get; set; }
        public string BasePcode { get; set; }
        public string Description { get; set; }
        public int? Length { get; set; }
        public int? Height { get; set; }
        public int? Width { get; set; }
        public int? OuterFrameDepth { get; set; }
        public int? PoleHeightDeduction { get; set; }
        public string SystemTypesAvailOn { get; set; }

        public ProductCategories ProductCat { get; set; }
    }
}
