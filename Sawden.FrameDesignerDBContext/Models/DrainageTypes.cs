﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class DrainageTypes
    {
        public int Id { get; set; }
        public string DraingeType { get; set; }
        public int? SelectionLinkId { get; set; }
        public string RoutingCode { get; set; }
    }
}
