﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class PanelGlasscodes
    {
        public int Id { get; set; }
        public string Glasscode { get; set; }
        public int Defaultpanelgroupid { get; set; }
        public string Alsoallowedongroupid { get; set; }
        public string GlassbordersavailId { get; set; }
        public string TileColoursAvailable { get; set; }
        public decimal? Sellprice24and28mm { get; set; }
        public decimal? Sellprice40mm { get; set; }
        public bool? Cansandblast { get; set; }
        public bool? Canbesidelight { get; set; }
        public int? Panelglasstype { get; set; }
        public string PanelglassShapeid { get; set; }
    }
}
