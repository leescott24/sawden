﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class PartTranslations
    {
        public int Id { get; set; }
        public string Basepcode { get; set; }
        public int? Colourid { get; set; }
        public string Description { get; set; }
        public bool? IsAvailable { get; set; }
        public string Pcode { get; set; }
        public string Stockcode { get; set; }
        public int? PricingMethodId { get; set; }
        public int? Sellprice { get; set; }
        public int? Cost { get; set; }
        public int? CostUomId { get; set; }
        public int? ProductCategoryId { get; set; }
        public int? ProductSubCategoryId { get; set; }
        public bool? AvailableAsExtra { get; set; }
        public bool? AvailableAsCut { get; set; }
        public string BayPartsIds { get; set; }
    }
}
