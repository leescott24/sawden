﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class Handles
    {
        public int Id { get; set; }
        public string Pcode { get; set; }
        public string ShortCode { get; set; }
        public string Description { get; set; }
        public int? Qty { get; set; }
        public int SelectionLinkId { get; set; }
        public int? ProductCatId { get; set; }
        public int? ProductSubCatId { get; set; }
        public string ChargeOnColourGroups { get; set; }
        public int? PricingMethodId { get; set; }
        public int? CostOumId { get; set; }
        public decimal? Cost { get; set; }
        public decimal? SellPriceLoose { get; set; }
        public decimal? SellPrice { get; set; }
        public bool? ShowInExtras { get; set; }
        public bool? Available { get; set; }

        public ProductCategories ProductCat { get; set; }
        public SelectionLinks SelectionLink { get; set; }
    }
}
