﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SystemTypes
    {
        public SystemTypes()
        {
            SelectionSystemTypeColours = new HashSet<SelectionSystemTypeColours>();
        }

        public int Id { get; set; }
        public string Systemtype { get; set; }
        public int? Masterproductlinkid { get; set; }
        public string SubType { get; set; }
        public string NominalCode { get; set; }
        public string ColoursIdsAvailable { get; set; }
        public int? DefaultOuterFrameId { get; set; }
        public int? DefaultTransomId { get; set; }
        public int? DefaultSashId { get; set; }
        public string DefaultOpenDirection { get; set; }
        public bool AllowOppositeOpen { get; set; }
        public string CategoriesAllowedDefault { get; set; }

        public OpenDirection DefaultOpenDirectionNavigation { get; set; }
        public OuterFrames DefaultOuterFrame { get; set; }
        public Sashes DefaultSash { get; set; }
        public TransomsAndMullions DefaultTransom { get; set; }
        public MasterProductTypes Masterproductlink { get; set; }
        public ICollection<SelectionSystemTypeColours> SelectionSystemTypeColours { get; set; }
    }
}
