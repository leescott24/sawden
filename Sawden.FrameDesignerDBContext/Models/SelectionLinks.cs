﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SelectionLinks
    {
        public SelectionLinks()
        {
            Beads = new HashSet<Beads>();
            Cills = new HashSet<Cills>();
            Handles = new HashSet<Handles>();
            Hinges = new HashSet<Hinges>();
            Locks = new HashSet<Locks>();
            OuterFrames = new HashSet<OuterFrames>();
            Sashes = new HashSet<Sashes>();
            SelectionColourLinks = new HashSet<SelectionColourLinks>();
            SelectionSashTypeLinks = new HashSet<SelectionSashTypeLinks>();
            SelectionSystemTypesLinks = new HashSet<SelectionSystemTypesLinks>();
        }

        public int Id { get; set; }
        public string SelectionLinkDesc { get; set; }
        public bool? _2ndtablelink { get; set; }
        public int? ProductcategoryId { get; set; }
        public int? ProductSubCatId { get; set; }
        public int? ProductSubCatSeq { get; set; }
        public bool? Available { get; set; }
        public string SystemTypeAvailOnId { get; set; }
        public string SashTypeAvailOnId { get; set; }
        public string ColourGroupAvailOnId { get; set; }
        public string ColourReinforcingIdAvail { get; set; }
        public int? PricingMethodId { get; set; }
        public decimal? SellPrice { get; set; }

        public ICollection<Beads> Beads { get; set; }
        public ICollection<Cills> Cills { get; set; }
        public ICollection<Handles> Handles { get; set; }
        public ICollection<Hinges> Hinges { get; set; }
        public ICollection<Locks> Locks { get; set; }
        public ICollection<OuterFrames> OuterFrames { get; set; }
        public ICollection<Sashes> Sashes { get; set; }
        public ICollection<SelectionColourLinks> SelectionColourLinks { get; set; }
        public ICollection<SelectionSashTypeLinks> SelectionSashTypeLinks { get; set; }
        public ICollection<SelectionSystemTypesLinks> SelectionSystemTypesLinks { get; set; }
    }
}
