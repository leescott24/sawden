﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class NightVents
    {
        public int Id { get; set; }
        public string Pcode { get; set; }
        public string StockCode1 { get; set; }
        public string StockCode2 { get; set; }
        public string Description { get; set; }
        public int? SelectionLinkId { get; set; }
        public int? ProductCatId { get; set; }
        public int? ProductSubCatid { get; set; }
        public int? ProductSubSeq { get; set; }
        public int? PriceMethodId { get; set; }
        public decimal? SellPrice { get; set; }
        public int? CostUomId { get; set; }
        public decimal? Cost { get; set; }
        public string RoutingCode { get; set; }
        public bool? Available { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }
        public int? Gap { get; set; }
        public int? MinPieceLength { get; set; }
        public int? MinPieceHeight { get; set; }

        public ProductCategories ProductCat { get; set; }
    }
}
