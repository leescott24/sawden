﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class GeoDuplexLead
    {
        public int Id { get; set; }
        public string Pcode { get; set; }
        public string Description { get; set; }
        public string StockCode { get; set; }
        public int? GeobarType { get; set; }
        public int? Thickness { get; set; }
        public int? PricingMethodId { get; set; }
        public decimal? SellPriceStg { get; set; }
        public decimal? SellPriceEuro { get; set; }
        public int? CostUom { get; set; }
        public decimal? Cost { get; set; }
        public int? DisplaySequence { get; set; }
        public string ColourId { get; set; }
        public string ColoursAvailbeOn { get; set; }
        public bool? IsAvailable { get; set; }
    }
}
