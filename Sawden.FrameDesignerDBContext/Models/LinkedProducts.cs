﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class LinkedProducts
    {
        public int Id { get; set; }
        public string BasePcode { get; set; }
        public string LinkedPcode { get; set; }
        public string LinkedPcodeStockCode { get; set; }
        public string Description { get; set; }
        public int? Qty { get; set; }
        public int? Length { get; set; }
        public int? PricingMethodId { get; set; }
        public decimal? SelPrice { get; set; }
        public int? CostUomId { get; set; }
        public decimal? CostPrice { get; set; }
        public int? SelectionLinkIdLink { get; set; }
        public int? ProductSubCatLink { get; set; }
    }
}
