﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class OpenDirection
    {
        public OpenDirection()
        {
            SashTypes = new HashSet<SashTypes>();
            SystemTypes = new HashSet<SystemTypes>();
        }

        public string OpenDirection1 { get; set; }

        public ICollection<SashTypes> SashTypes { get; set; }
        public ICollection<SystemTypes> SystemTypes { get; set; }
    }
}
