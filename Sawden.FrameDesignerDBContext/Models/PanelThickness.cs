﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class PanelThickness
    {
        public int Id { get; set; }
        public string Pcode { get; set; }
        public string ThicknessDesc { get; set; }
        public int? Fullpanelprice { get; set; }
        public int? Halfpanelprice { get; set; }
        public bool? Default { get; set; }
        public int Displayseq { get; set; }
    }
}
