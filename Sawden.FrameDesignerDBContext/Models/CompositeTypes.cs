﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class CompositeTypes
    {
        public int Id { get; set; }
        public string Compositetype { get; set; }
    }
}
