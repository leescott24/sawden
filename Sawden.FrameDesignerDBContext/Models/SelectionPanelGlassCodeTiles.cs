﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SelectionPanelGlassCodeTiles
    {
        public int GlassCodeId { get; set; }
        public int TileColurId { get; set; }
    }
}
