﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SpacerBar
    {
        public int Id { get; set; }
        public string Pcode { get; set; }
        public string Description { get; set; }
        public string SizesAvailable { get; set; }
    }
}
