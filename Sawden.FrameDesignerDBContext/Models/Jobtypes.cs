﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class Jobtypes
    {
        public int Id { get; set; }
        public string JobType { get; set; }
        public int? Jobextbreakdownid { get; set; }
        public int? DisplaySequence { get; set; }
        public bool? ShowCustomer { get; set; }
    }
}
