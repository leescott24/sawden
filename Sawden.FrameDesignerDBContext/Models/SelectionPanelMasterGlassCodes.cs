﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SelectionPanelMasterGlassCodes
    {
        public int GlassCodeId { get; set; }
        public int MasterGroupId { get; set; }
    }
}
