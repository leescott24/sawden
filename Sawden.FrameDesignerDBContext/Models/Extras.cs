﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class Extras
    {
        public int Id { get; set; }
        public int ProductCatId { get; set; }
        public int? ExtrasCatId { get; set; }
        public int? Qty { get; set; }
        public string Pcode { get; set; }
        public string ShortCode { get; set; }
        public string Description { get; set; }
        public int? Lenght { get; set; }
        public int? PricingMethodId { get; set; }
        public decimal? SellPrice { get; set; }
        public int? CostUomId { get; set; }
        public decimal? CostPrice { get; set; }
    }
}
