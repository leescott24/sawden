﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class OuterFrames
    {
        public OuterFrames()
        {
            SystemTypes = new HashSet<SystemTypes>();
        }

        public int Id { get; set; }
        public int ProductCatId { get; set; }
        public string Pcode { get; set; }
        public string StockCode { get; set; }
        public string Description { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int RebateExt { get; set; }
        public int RebatInt { get; set; }
        public int Glass { get; set; }
        public int Sash { get; set; }
        public bool IsOddLeg { get; set; }
        public int Selectionid { get; set; }
        public string ReversePcode { get; set; }
        public int? ReinforcingGroupId { get; set; }
        public int? ReinforcingLenghtDeduction { get; set; }
        public int? DoorThresholdType { get; set; }
        public int? PricingMethodId { get; set; }
        public decimal? SellPrice { get; set; }
        public int? CostUomId { get; set; }
        public decimal? Cost { get; set; }
        public string SystemTypesExcludePricing { get; set; }

        public SelectionLinks Selection { get; set; }
        public ICollection<SystemTypes> SystemTypes { get; set; }
    }
}
