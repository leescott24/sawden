﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class Hinges
    {
        public int Id { get; set; }
        public int ProductCatId { get; set; }
        public int? ProductSubCatId { get; set; }
        public bool? Available { get; set; }
        public string Pcode { get; set; }
        public string ShortCode { get; set; }
        public string Description { get; set; }
        public int Qty { get; set; }
        public int? CostUomId { get; set; }
        public int? CostPrice { get; set; }
        public int? PricingMethodId { get; set; }
        public string ChargeOnColourGroups { get; set; }
        public decimal? SellPrice { get; set; }
        public int SelectionLinkId { get; set; }
        public string RoutingCode { get; set; }
        public int? SizeRangeFrom { get; set; }
        public int? SizeRangeTo { get; set; }

        public ProductCategories ProductCat { get; set; }
        public ProductSubcategories ProductSubCat { get; set; }
        public SelectionLinks SelectionLink { get; set; }
    }
}
