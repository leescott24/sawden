﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SpacerBarMeasurements
    {
        public int SpacerBarId { get; set; }
        public int Measurement { get; set; }
    }
}
