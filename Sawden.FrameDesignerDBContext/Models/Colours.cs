﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class Colours
    {
        public Colours()
        {
            SelectionSystemTypeColours = new HashSet<SelectionSystemTypeColours>();
        }

        public int Id { get; set; }
        public string Colour { get; set; }
        public bool? CamdenFoil { get; set; }
        public int? ColourGrp { get; set; }
        public int? ColourSeq { get; set; }
        public string DefaultBeadColourId { get; set; }
        public int? ReinforcingColourTypeId { get; set; }
        public bool Geopanel { get; set; }
        public byte ColourOnColour { get; set; }
        public byte ColourSmallFace { get; set; }
        public byte ColourLargeFace { get; set; }
        public string VisualColour { get; set; }
        public string ForeColour { get; set; }
        public bool RequiresTouchUp { get; set; }
        public int? ColourIdOutside { get; set; }
        public int? ColourIdInside { get; set; }
        public int? DiscountGroup { get; set; }

        public ColourGroups ColourGrpNavigation { get; set; }
        public ICollection<SelectionSystemTypeColours> SelectionSystemTypeColours { get; set; }
    }
}
