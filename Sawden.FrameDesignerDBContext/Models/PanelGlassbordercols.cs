﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class PanelGlassbordercols
    {
        public int Id { get; set; }
        public string Colourdesc { get; set; }
        public int Sequence { get; set; }
        public bool? Default { get; set; }
    }
}
