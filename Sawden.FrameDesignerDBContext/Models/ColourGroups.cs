﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class ColourGroups
    {
        public ColourGroups()
        {
            Colours = new HashSet<Colours>();
            ProductSubCatDefaultsExploded = new HashSet<ProductSubCatDefaultsExploded>();
            SelectionColourLinks = new HashSet<SelectionColourLinks>();
        }

        public int Id { get; set; }
        public string Colourgroupdesc { get; set; }

        public ICollection<Colours> Colours { get; set; }
        public ICollection<ProductSubCatDefaultsExploded> ProductSubCatDefaultsExploded { get; set; }
        public ICollection<SelectionColourLinks> SelectionColourLinks { get; set; }
    }
}
