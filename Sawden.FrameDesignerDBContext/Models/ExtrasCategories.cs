﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class ExtrasCategories
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int? DisplaySequence { get; set; }
    }
}
