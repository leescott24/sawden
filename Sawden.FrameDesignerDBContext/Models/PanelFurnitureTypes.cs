﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class PanelFurnitureTypes
    {
        public int Id { get; set; }
        public string TypeDesc { get; set; }
        public string PanelTypeAvailableOn { get; set; }
    }
}
