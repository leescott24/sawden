﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SubFrameParts
    {
        public int Id { get; set; }
        public int ProductCatId { get; set; }
        public string Pcode { get; set; }
        public string StockCode { get; set; }
        public string Description { get; set; }
        public int PriceMethodId { get; set; }
        public decimal SellPrice { get; set; }
        public int CostUomId { get; set; }
        public decimal? Cost { get; set; }
        public int? Length { get; set; }
        public int? Qty { get; set; }
        public int? OuterFrameDed { get; set; }
        public int? Deduction { get; set; }
        public int? Addition { get; set; }
        public bool? SupplyLoose { get; set; }
        public bool? AllowedAsExtra { get; set; }

        public ProductCategories ProductCat { get; set; }
    }
}
