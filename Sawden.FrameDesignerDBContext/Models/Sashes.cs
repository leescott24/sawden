﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class Sashes
    {
        public Sashes()
        {
            SystemTypes = new HashSet<SystemTypes>();
        }

        public int Id { get; set; }
        public int ProductCatId { get; set; }
        public string Pcode { get; set; }
        public string ShortCode { get; set; }
        public string Description { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int RebateExt { get; set; }
        public int RebateInt { get; set; }
        public int OverLap { get; set; }
        public bool IsOpenIn { get; set; }
        public int SelectionLinkid { get; set; }
        public int? ReinforcingGroupId { get; set; }
        public bool? FitDripRail { get; set; }

        public ProductCategories ProductCat { get; set; }
        public SelectionLinks SelectionLink { get; set; }
        public ICollection<SystemTypes> SystemTypes { get; set; }
    }
}
