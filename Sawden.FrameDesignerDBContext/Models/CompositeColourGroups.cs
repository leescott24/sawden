﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class CompositeColourGroups
    {
        public int Id { get; set; }
        public string CompoColourGroup { get; set; }
    }
}
