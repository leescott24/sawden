﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class ReinforcingRules
    {
        public int Id { get; set; }
        public string MasterProductType { get; set; }
        public string ColourReoId { get; set; }
        public int? ReinforcingSelectionId { get; set; }
        public int? PieceLengthOuterLeftRight { get; set; }
        public int? PieceLenghtTopBottom { get; set; }
        public int? PieceLengthSash { get; set; }
        public int? PieceLenghtTransom { get; set; }
    }
}
