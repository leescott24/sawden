﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class Panelmastergroups
    {
        public int Id { get; set; }
        public string Panelgroupdesc { get; set; }
        public string Code { get; set; }
        public decimal Baseprice { get; set; }
        public bool? IaAvailable { get; set; }
        public int? DisplaySequence { get; set; }
        public int? PanelType { get; set; }
        public int? Minwidth { get; set; }
        public int? Maxwidth { get; set; }
        public int? MinHeight { get; set; }
        public int? MaxHeight { get; set; }
        public int? SelectionLinkid { get; set; }
    }
}
