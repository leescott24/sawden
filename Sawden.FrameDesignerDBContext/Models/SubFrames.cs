﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.FrameDesignerDBContext.Models
{
    public partial class SubFrames
    {
        public int Id { get; set; }
        public int? ProductCatId { get; set; }
        public string Description { get; set; }
        public string PcodeLeft { get; set; }
        public string PcodeRight { get; set; }
        public string PcodeTop { get; set; }
        public string PcodeBottom { get; set; }
        public int? JointTypeId { get; set; }
        public int? SelectionLinkId { get; set; }
        public bool? RequiresThickness { get; set; }

        public ProductCategories ProductCat { get; set; }
    }
}
