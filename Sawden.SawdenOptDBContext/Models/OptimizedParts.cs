﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.SawdenOptDBContext.Models
{
    public partial class OptimizedParts
    {
        public int PartsitemId { get; set; }
        public string SawType { get; set; }
        public string JobNumber { get; set; }
        public byte FrameNumber { get; set; }
        public byte SashNumber { get; set; }
        public string PartPosition { get; set; }
        public string ProfileCode { get; set; }
        public byte ColourCode { get; set; }
        public string ShortCode { get; set; }
        public double PartLength { get; set; }
        public string PartCutType { get; set; }
        public string PartCutInfo { get; set; }
        public string PartRoutings { get; set; }
        public bool Reinforced { get; set; }
        public string ReinforcementChar { get; set; }
        public byte NoOfPanes { get; set; }
        public byte NoOfSashes { get; set; }
        public byte Printed { get; set; }
        public bool Ready { get; set; }
        public int? RequestId { get; set; }
        public short? Bin { get; set; }
        public short? DeleteTrolley { get; set; }
        public int? OrigRequestId { get; set; }
        public string OrigPartRoutings { get; set; }
        public DateTime TimeStamp { get; set; }
        public string DeleteLockInfo { get; set; }
        public int HeaderId { get; set; }
        public short? CutCount { get; set; }
        public DateTime? LastCutTime { get; set; }
        public byte[] RowVer { get; set; }
        public bool? ReinforcedTest { get; set; }
    }
}
