﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Sawden.Persistance.SawdenOptDBContext.Models
{
    public partial class SawdenOptContext : DbContext
    {
        public SawdenOptContext()
        {
        }

        public SawdenOptContext(DbContextOptions<SawdenOptContext> options)
            : base(options)
        {
        }

        public virtual DbSet<OptimizedParts> OptimizedParts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=SawdenOpt;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OptimizedParts>(entity =>
            {
                entity.HasKey(e => new { e.PartsitemId, e.SawType, e.JobNumber, e.FrameNumber, e.SashNumber });

                entity.Property(e => e.PartsitemId).HasColumnName("PARTSITEM_ID");

                entity.Property(e => e.SawType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.JobNumber)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.CutCount).HasDefaultValueSql("((0))");

                entity.Property(e => e.DeleteLockInfo)
                    .HasColumnName("DELETE_LockInfo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeleteTrolley).HasColumnName("DELETE_Trolley");

                entity.Property(e => e.HeaderId).HasColumnName("HEADER_ID");

                entity.Property(e => e.LastCutTime).HasColumnType("datetime");

                entity.Property(e => e.OrigPartRoutings)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.OrigRequestId).HasColumnName("OrigRequestID");

                entity.Property(e => e.PartCutInfo)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PartCutType)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.PartPosition)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PartRoutings)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.ProfileCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ReinforcementChar)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.RequestId).HasColumnName("RequestID");

                entity.Property(e => e.RowVer)
                    .IsRequired()
                    .IsRowVersion();

                entity.Property(e => e.ShortCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.TimeStamp)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });
        }
    }
}
