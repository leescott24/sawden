import { Component, OnInit } from '@angular/core'
import { PCodeDataService } from '../Data/pcode-data.service';
import { PCodeMaster, PCode, PCodeGroup } from '../Data/Models/PCode.model';
import { Router } from '@angular/router';

@Component({
        templateUrl: './pcode-info.component.html'
})
export class PCodeInfoComponent implements OnInit
{        
        public get pcodemaster() : PCodeMaster {
                return this.pCodeData.pcodeMaster;
        } 
        
        
        customModalDisplay : boolean = false;
        confirmTitle : string = "";
        confirmBody : string = "";
        confirmType : string = "";
        confirmData : any;
        
        constructor(private pCodeData:PCodeDataService, 
                private router: Router){}

        ngOnInit()
        {                
                if (!this.pCodeData.pcodeMaster)
                {
                        this.pCodeData.getPcodes().subscribe(response =>
                                {
                                        this.pCodeData.refreshPcodes(response);
                                }) 
                }             
        }

        PCodeAdd()
        {
                this.router.navigate(['/pcode', 0]);
        }

        PCodeEdit(id: number)
        {
                this.router.navigate(['/pcode', id]);
        }

        PCodeDelete(id: number)
        {
                let pcode : PCode = this.pCodeData.getPcodeById(id);
                
                this.confirmTitle = "Pcode Deletion";
                this.confirmBody = `Are you sure you wish to delete pcode: ${pcode.pcode}?`
                this.confirmType = "pcode";
                this.confirmData = pcode;
                this.customModalDisplay = true;                
        }

        GroupAdd()
        {
                this.router.navigate(['/pcode/group', 0]);
        }

        GroupEdit(id : number)
        {
                this.router.navigate(['/pcode/group', id]);
        }

        GroupDelete(id: number)
        {
                let group : PCodeGroup = this.pCodeData.getPcodeGroupById(id);

                console.log(group);
                
                this.confirmTitle = "Pcode Deletion";
                this.confirmBody = `Are you sure you wish to delete pcode group: ${group.group}?`
                this.confirmType = "pcodegroup";
                this.confirmData = group;
                this.customModalDisplay = true;
        }

        getGroupPcodes(id: number) : string
        {
                if (id)
                {
                        return "number 1";
                }
                return "test";
        }

        getType(id: number) : string
        {                
                return this.pcodemaster.pCodeTypes.filter(f => f.id == id)[0].type;
        }

        handleCustomConfirm(data)
        {                
                
                if(data.response == "confirm")
                {                                            
                        if (this.confirmType == "pcode")
                        {
                                this.pCodeData.deletePcode(data.data);
                        }

                        if (this.confirmType == "pcodegroup")
                        {
                                this.pCodeData.deletePcodeGroup(data.data);
                        }
                }


                this.resetCustomConfirm();
        }

        resetCustomConfirm()
        {
                this.customModalDisplay = false;
                this.confirmTitle = "";
                this.confirmBody = "";
                this.confirmType = "";
        }
}

