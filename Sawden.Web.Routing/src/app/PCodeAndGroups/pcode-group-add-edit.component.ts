import { Component, OnInit } from '@angular/core';
import { PCodeDataService } from '../Data/pcode-data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PCodeMaster, PCodeGroup } from '../Data/Models/PCode.model';
import { stringToTitle } from '../Shared/shared-methods.method';


@Component({
    templateUrl: './pcode-group-add-edit.component.html'
})
export class PCodeGroupAddEditComponent implements OnInit
{
    public get pcodemaster() : PCodeMaster {
            return this.pCodeData.pcodeMaster;
    } 

    pcodeGroupId: number;

    pcodeGroupOrignal: PCodeGroup;
    pcodeGroupCurent: PCodeGroup = new PCodeGroup();

    groupPcodesDisplay : boolean = true;

    isSaveDisabled : boolean = true;
    title: string;
    
    constructor(private pCodeData:PCodeDataService,
        private route: ActivatedRoute,
        private router: Router) {}
        
    ngOnInit()
    {
        this.route.paramMap.subscribe(params =>
            {
                this.pcodeGroupId = +params.get("id");
                
                if (!this.pCodeData.pcodeMaster)
                {
                    this.pCodeData.getPcodes().subscribe(response =>
                        {
                                this.pCodeData.refreshPcodes(response);
                                this.getPcodeGroup();
                        })
                }
                else
                {
                    this.getPcodeGroup();
                }  
            
            });
    }

    getPcodeGroup()
    {        
        if (this.pcodeGroupId == 0)
        {
            this.title = "Add New PCode Group (Lookup)";
            this.pcodeGroupOrignal = new PCodeGroup();
            this.pcodeGroupOrignal.pcodes = [];     
        }
        else
        {
            this.title = "Edit PCode Group (Lookup)";
            this.pcodeGroupOrignal = this.pCodeData.getPcodeGroupById(this.pcodeGroupId);
        }

        this.pcodeGroupCurent = JSON.parse(JSON.stringify(this.pcodeGroupOrignal));
    }

    togglePcodeInGroup(id: number)
    {            
        if (this.pcodeGroupCurent.pcodes.filter(f => f.id == id)[0] != null)
        {
            this.pcodeGroupCurent.pcodes = this.pcodeGroupCurent.pcodes.filter(f => f.id != id);
        }
        else
        {
            let pcode = this.pCodeData.getPcodeById(id);
            this.pcodeGroupCurent.pcodes.push(pcode);
        }
    }

    pcodeInGroup(id: number) : boolean
    {
        return this.pcodeGroupCurent.pcodes.filter(f => f.id == id)[0] != null;
    }

    pcodeGroupChanged(event: string)
    {
        this.pcodeGroupCurent.group = stringToTitle(event);
        this.compare();
    }

    descriptionChanged(event: string)
    {
        this.pcodeGroupCurent.groupDescription = stringToTitle(event);
        this.compare();
    }

    compare()
    {
        if (this.isPcodeGroupValid() && (JSON.stringify(this.pcodeGroupCurent) != JSON.stringify(this.pcodeGroupOrignal)))
        {
            this.isSaveDisabled = false;
        }
        else
        {
            this.isSaveDisabled = true;
        }
    }

    isPcodeGroupValid() : boolean
    {              
        if (!this.pcodeGroupCurent.group || this.pcodeGroupCurent.group == "" 
        || !this.pcodeGroupCurent.groupDescription || this.pcodeGroupCurent.groupDescription == ""
        || !this.pcodeGroupCurent.pcodes || this.pcodeGroupCurent.pcodes.length == 0)
        {
            return false;
        }


        if (this.pcodeGroupId == 0 && this.pcodemaster.pCodeGroups.filter(m => m.group == this.pcodeGroupCurent.group)[0] != null)
        {
            return false;
        }

        return true;
    }

    groupAddPcodesToggle()
    {
        this.groupPcodesDisplay = false;
    }

    groupPcodesAdd()
    {
        this.groupPcodesDisplay = true;
        this.compare();
    }

    groupPcodesCancel()
    {
        this.pcodeGroupCurent.pcodes = JSON.parse(JSON.stringify(this.pcodeGroupOrignal.pcodes));
        this.groupPcodesDisplay = true;
        this.compare();
    }

    cancel()
    {
        this.router.navigate(['/pcode']);
    }

    save()
    {
        if (this.pcodeGroupId == 0)
        {
            this.pCodeData.addPcodeGroup(this.pcodeGroupCurent);
        }
        else
        {
            this.pCodeData.updatePcodeGroup(this.pcodeGroupCurent);
        }

        this.router.navigate(['/pcode']);
    }
}