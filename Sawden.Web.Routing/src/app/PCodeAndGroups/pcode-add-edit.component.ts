import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router';
import { PCodeDataService } from '../Data/pcode-data.service';
import { PCode, PCodeType, PCodeMaster } from '../Data/Models/PCode.model';
import { TitleCasePipe } from '@angular/common';
import { stringToTitle } from '../Shared/shared-methods.method';

@Component({
    templateUrl: './pcode-add-edit.component.html'
})
export class PCodeAddEditComponent implements OnInit
{
    public get pcodemaster() : PCodeMaster {
            return this.pCodeData.pcodeMaster;
    } 

    pcodeId: number;

    pcodeOrignal: PCode;
    pcodeCurent: PCode = new PCode();

    pcodeTypeDisplay : boolean = true;
    pcodeTypeDefault : string = "";

    isSaveDisabled : boolean = true;
    title: string;

    constructor(private pCodeData:PCodeDataService,
    private route: ActivatedRoute,
    private router: Router) {}

    ngOnInit()
        {
            this.route.paramMap.subscribe(params =>
                {
                    this.pcodeId = +params.get("id");
                    
                    if (!this.pCodeData.pcodeMaster)
                    {
                        this.pCodeData.getPcodes().subscribe(response =>
                            {
                                    this.pCodeData.refreshPcodes(response);
                                    this.getPcode();
                            })
                    }
                    else
                    {
                        this.getPcode();
                    }               
                });
        }

    getPcode()
    {
        if (this.pcodeId == 0)
        {
            this.title = "Add New PCode";
            this.pcodeOrignal = new PCode();     
        }
        else
        {
            this.title = "Edit PCode";
            this.pcodeOrignal = this.pCodeData.getPcodeById(this.pcodeId);
        }

        this.pcodeCurent = JSON.parse(JSON.stringify(this.pcodeOrignal));
    } 

    pcodeChanged(event: string)
    {
        this.pcodeCurent.pcode = stringToTitle(event);
        this.compare();
    }

    descriptionChanged(event: string)
    {
        this.pcodeCurent.description = stringToTitle(event);        
        this.compare();
    }

    pcodeTypeIdChanged(event: number)
    {        
        this.pcodeCurent.pcodeTypeId = +event;
        this.compare();

        console.log(event);
    }

    compare()
    {
        if (this.isPcodeValid() && (JSON.stringify(this.pcodeCurent) != JSON.stringify(this.pcodeOrignal)))
        {
            this.isSaveDisabled = false;
        }
        else
        {
            this.isSaveDisabled = true;
        }
    }

    isPcodeValid() : boolean
    {              
        if (!this.pcodeCurent.pcode || this.pcodeCurent.pcode == "" 
        || !this.pcodeCurent.description || this.pcodeCurent.description == ""
        || !this.pcodeCurent.pcodeTypeId || this.pcodeCurent.pcodeTypeId == 0)
        {
            return false;
        }


        if (this.pcodeId == 0 && this.pcodemaster.pCodes.filter(m => m.pcode == this.pcodeCurent.pcode)[0] != null)
        {
            return false;
        }

        return true;
    }

    pcodeTypeToggle()
    {
        this.pcodeTypeDisplay = false;
    }

    pcodeTypeAdd()
    {
        let type = new PCodeType();
        type.type = stringToTitle(this.pcodeTypeDefault);    
        
        this.pCodeData.addPcodeType(type).subscribe(response =>
            {
                this.pcodeCurent.pcodeTypeId = response;
                this.pcodeTypeDefault = "";
                this.pcodeTypeDisplay = true;
                this.compare();
            });     
    }

    pcodeTypeCancel()
    {
        this.pcodeTypeDefault = "";
        this.pcodeTypeDisplay = true;
    }

    cancel()
    {
        this.router.navigate(['/pcode']);
    }

    save()
    {        
        if (this.pcodeId == 0)
        {
            this.pCodeData.addPcode(this.pcodeCurent);
        }
        else
        {
            this.pCodeData.updatePcode(this.pcodeCurent);
        }

        this.router.navigate(['/pcode']);
    }    
}

