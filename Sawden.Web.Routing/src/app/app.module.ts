import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms'
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

//nav
import { NavComponent } from './Nav/nav.component';

//services
import { SawdenRoutingConfig } from './Config/Sawden-Routing-Config.service';
import { PCodeDataService } from './Data/pcode-data.service';
import { ProfileDataService } from './Data/profile-data.service';

//shared
import { TableWrapperComponent } from './Shared/table-wrapper.component';
import { CustomConfirmComponent } from './Shared/custom-confirm.component';

//profile
import { ProfileInfoComponent } from './ProfileAndGroups/profile-info.component';
import { ProfileGroupAddEditComponent } from './ProfileAndGroups/profile-group-add-edit.component';

//pcodes
import { PCodeInfoComponent } from './PCodeAndGroups/pcode-info.component';
import { PCodeAddEditComponent } from './PCodeAndGroups/pcode-add-edit.component';
import { PCodeGroupAddEditComponent } from './PCodeAndGroups/pcode-group-add-edit.component';

//drainage
import { DrainageInfoComponent } from './Drainage/drainage-info.component';

//routing
import { RouteringInfoComponent } from './Routering/routering-info.component';


//testing
import { TestingInfoComponent } from './Testing/testing-info.component';

//pipes
import { GroupPcodeFilter } from './Pipes/group-pcode-filter.pipe';
import { ProfileOrder } from './Pipes/profile-order.pipe';
import { PcodeOrder } from './Pipes/pcode-order.pipe';


@NgModule({
  declarations: [
    AppComponent,

    NavComponent,
    TableWrapperComponent,    
    CustomConfirmComponent,

    PCodeInfoComponent,
    PCodeAddEditComponent,
    PCodeGroupAddEditComponent,

    ProfileInfoComponent,
    ProfileGroupAddEditComponent,
    DrainageInfoComponent,

    RouteringInfoComponent,

    TestingInfoComponent,   

    GroupPcodeFilter,
    ProfileOrder,
    PcodeOrder
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      preventDuplicates: true
    })  
  ],
  providers: [
    SawdenRoutingConfig,
    PCodeDataService,
    ProfileDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
