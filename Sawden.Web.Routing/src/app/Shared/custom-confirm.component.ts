import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'custom-confirm',
    templateUrl: './custom-confirm.component.html',
    styleUrls: ['./custom-confirm.component.css']
})
export class CustomConfirmComponent
{
    @Input() title : string;
    @Input() body : string;
    @Input() data : any;

    @Output() customConfirmEvent = new EventEmitter();
    
    constructor() {}

    confirm()
    {
        this.customConfirmEvent.emit({
            response: "confirm",
            data: this.data
        });
    }

    cancel()
    {
        this.customConfirmEvent.emit({
            response: "cancel"
        });
    }
}