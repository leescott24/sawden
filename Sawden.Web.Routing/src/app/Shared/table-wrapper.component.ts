import { Component } from '@angular/core';

@Component({
    selector: 'table-wrapper',
    templateUrl: './table-wrapper.component.html'
})
export class TableWrapperComponent
{
    display: string = "Hide";

    displayToggle()
        {
                if (this.display == "Hide")
                {
                        this.display = "Show";
                }
                else
                {
                        this.display = "Hide";
                }        
        }
}