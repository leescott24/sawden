export function stringToTitle(str : string) : string
{
    str = str.trim();

    if (str != "")
    {
        return str.split(' ')
            .map(m => m[0].toUpperCase() + m.substr(1).toLowerCase())
            .join(' ');
    }
    else
    {
        return "";
    }             
}