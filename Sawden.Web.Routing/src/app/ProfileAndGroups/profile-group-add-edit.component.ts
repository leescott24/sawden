import { Component, OnInit } from '@angular/core';
import { ProfileDataService } from '../Data/profile-data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileMaster, ProfileGroup } from '../Data/Models/Profile.model';
import { stringToTitle } from '../Shared/shared-methods.method';




@Component({
    templateUrl: './profile-group-add-edit.component.html'
})
export class ProfileGroupAddEditComponent implements OnInit
{
    public get profilemaster() : ProfileMaster {
        return this.profileData.profileMaster;
    }

    profileGroupId: number;

    profileGroupOrignal: ProfileGroup;
    profileGroupCurent: ProfileGroup = new ProfileGroup();

    groupPcodesDisplay : boolean = true;

    isSaveDisabled : boolean = true;
    title: string;

    constructor(private profileData:ProfileDataService,
        private route: ActivatedRoute,
        private router: Router) {}
    
    ngOnInit()
    {
        this.route.paramMap.subscribe(params =>
            {
                this.profileGroupId = +params.get("id");
                
                if (!this.profileData.profileMaster)
                {
                    this.profileData.getProfiles().subscribe(response =>
                        {
                                this.profileData.refreshProfiles(response);
                                this.getPcodeGroup();
                        })
                }
                else
                {
                    this.getPcodeGroup();
                }  
            
            });
    }

    getPcodeGroup()
    {
        if (this.profileGroupId == 0)
        {
            this.title = "Add New Profile Group (Lookup)";
            this.profileGroupOrignal = new ProfileGroup();
            this.profileGroupOrignal.profiles = [];     
        }
        else
        {
            this.title = "Edit Profile Group (Lookup)";
            this.profileGroupOrignal = this.profileData.getProfileGroupById(this.profileGroupId);
        }

        this.profileGroupCurent = JSON.parse(JSON.stringify(this.profileGroupOrignal));
    }

    profileGroupChanged(event: string)
    {
        this.profileGroupCurent.group = stringToTitle(event);
        this.compare();
    }

    descriptionChanged(event: string)
    {
        this.profileGroupCurent.groupDescription = stringToTitle(event);
        this.compare();
    }

    compare()
    {
        if (this.isProfileGroupValid() && (JSON.stringify(this.profileGroupCurent) != JSON.stringify(this.profileGroupOrignal)))
        {
            this.isSaveDisabled = false;
        }
        else
        {
            this.isSaveDisabled = true;
        }
    }

    isProfileGroupValid() : boolean
    { 
        return true;
    }

    
}