import { Component } from '@angular/core'
import { ProfileDataService } from '../Data/profile-data.service';
import { Router } from '@angular/router';
import { ProfileMaster } from '../Data/Models/Profile.model';


@Component({
    templateUrl: './profile-info.component.html'
})
export class ProfileInfoComponent
{
    public get profilemaster() : ProfileMaster {
        return this.profileData.profileMaster;
    }

    customModalDisplay : boolean = false;
    confirmTitle : string = "";
    confirmBody : string = "";
    confirmType : string = "";
    confirmData : any;

    constructor(private profileData:ProfileDataService, 
        private router: Router){}


    ngOnInit()
    {                
            if (!this.profileData.profileMaster)
            {
                    this.profileData.getProfiles().subscribe(response =>
                            {
                                    this.profileData.refreshProfiles(response);
                            }) 
            }             
    }


    getType(id: number) : string
    {                
            return this.profilemaster.profileTypes.filter(f => f.id == id)[0].type;
    }

    GroupAdd()
    {
        this.router.navigate(['/profile/group', 0]);
    }
    
    GroupEdit(id: number)
    {
        this.router.navigate(['/profile/group', id]);
    }

    GroupDelete(id: number)
    {

    }
}