import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PCodeMaster, PCode, PCodeType, PCodeGroup } from './Models/PCode.model';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

import { map } from 'rxjs/operators';
import { SawdenRoutingConfig } from '../Config/Sawden-Routing-Config.service';

@Injectable()
export class PCodeDataService
{     
    pcodeMaster: PCodeMaster;
    options : any = { headers: new HttpHeaders({'Content-Type': 'application/json'})};

    public get urlStart() : string 
    {
        return this.config.urlStart + "PCodeRouting/";
    }

    constructor(private http: HttpClient,
        private toastr: ToastrService,
        private config: SawdenRoutingConfig){}

    getPcodes() : Observable<PCodeMaster>
    {        
        return this.http.get<PCodeMaster>(this.urlStart + 'GetPcodeMaster');              
    }

    refreshPcodes(response: PCodeMaster)
    {
        this.toastr.success("PCodes have been successfully loaded from Sawden DB");
        this.pcodeMaster = response;                                
    }

    addPcode(data: PCode)
    {        
        this.http.post(this.urlStart + 'AddPcode', data, this.options).subscribe(
            (response) =>
            {
                data.id = +response;
                this.pcodeMaster.pCodes.push(data);

                this.toastr.success(`Pcode: "${data.pcode}" has been successfully added to Sawden DB`);                                
            },
            error =>
            {
                this.toastr.error(`Pcode: "${data.pcode}" failed to add to Sawden DB`);
            }
        )
    }

    updatePcode(data: PCode)
    {    
        this.http.post(this.urlStart + 'UpdatePcode', data, this.options).subscribe(
            response =>
            {
                let item = this.pcodeMaster.pCodes.find(this.findIndexToUpdate, data.id);
                let index = this.pcodeMaster.pCodes.indexOf(item);       
                this.pcodeMaster.pCodes[index] = data;

                for (var i = 0; i < this.pcodeMaster.pCodeGroups.length; i++)
                {
                    for(var j = 0; j < this.pcodeMaster.pCodeGroups[i].pcodes.length; j++)
                    {
                        if (this.pcodeMaster.pCodeGroups[i].pcodes[j].id == data.id)
                        {
                            this.pcodeMaster.pCodeGroups[i].pcodes[j].pcode = data.pcode;
                            this.pcodeMaster.pCodeGroups[i].pcodes[j].description = data.description;
                            this.pcodeMaster.pCodeGroups[i].pcodes[j].pcodeTypeId = data.pcodeTypeId;
                        }
                    }
                }

                this.toastr.success(`Pcode: "${data.pcode}" has been successfully updated in Sawden DB`);
            },
            error =>
            {
                this.toastr.error(`Pcode: "${data.pcode}" failed to update in Sawden DB`);
            }
        )
    }

    deletePcode(data: PCode)
    {             
        this.http.post(this.urlStart + 'DeletePcode', data.id, this.options).subscribe(
            response =>
            {
                this.pcodeMaster.pCodes = this.pcodeMaster.pCodes.filter(function(value, index, arr)
                {
                    return value.id != data.id;
                });

                this.toastr.success(`Pcode: "${data.pcode}" has been successfully removed from Sawden DB`);
            },
            error =>
            {
                this.toastr.error(`Pcode: "${data.pcode}" failed to delete from Sawden DB`);
            }
        )
    }

    findIndexToUpdate(newItem) { 
        return newItem.id === this;
    }

    addPcodeType(data: PCodeType) : Observable<number>
    {
        return this.http.post<number>(this.urlStart + 'AddPcodeType', data, this.options).pipe(
            map((response) =>
            {
                data.id = +response;
                this.pcodeMaster.pCodeTypes.push(data);
                
                return +response;                
            },
            error =>
            {
                console.log(error);
            }));            
    }

    getPcodeById(id : number) : PCode
    {
        return this.pcodeMaster.pCodes.filter(f => f.id == id)[0];
    }

    getPcodeGroupById(id : number) : PCodeGroup
    {
        return this.pcodeMaster.pCodeGroups.filter(f => f.id == id)[0];
    }

    addPcodeGroup(data: PCodeGroup)
    {
        this.http.post(this.urlStart + "AddPcodeGroup", data, this.options)
            .subscribe(response =>
                {
                    data.id = +response;
                    this.pcodeMaster.pCodeGroups.push(data);

                    this.toastr.success(`Pcode Group (Lookup): "${data.group}" has been successfully added to Sawden DB`);
                }, error =>
                {
                    this.toastr.success(`Pcode Group (Lookup): "${data.group}" failed to add to Sawden DB`);
                });
    }

    updatePcodeGroup(data: PCodeGroup)
    {
        this.http.post(this.urlStart + 'UpdatePcodeGroup', data, this.options)
            .subscribe(response =>
                {                    
                    let item = this.pcodeMaster.pCodeGroups.find(this.findIndexToUpdate, data.id);
                    let index = this.pcodeMaster.pCodeGroups.indexOf(item);       
                    this.pcodeMaster.pCodeGroups[index] = data;

                    this.toastr.success(`Pcode Group (Lookup): "${data.group}" has been successfully updated in Sawden DB`);
                }, error =>
                {
                    this.toastr.success(`Pcode Group (Lookup): "${data.group}" failed to updated in Sawden DB`);
                }
                );
    }

    deletePcodeGroup(data: PCodeGroup)
    {
        this.http.post(this.urlStart + 'DeletePcodeGroup', data.id, this.options)
            .subscribe(response =>
                {
                    this.pcodeMaster.pCodeGroups = this.pcodeMaster.pCodeGroups.filter(function(value, index, arr)
                    {
                        return value.id != data.id;
                    });

                    this.toastr.success(`Pcode Group (Lookup): "${data.group}" has been successfully removed from Sawden DB`);
                }, error =>
                {
                    this.toastr.error(`Pcode Group (Lookup): "${data.group}" failed to delete from Sawden DB`);
                }
                );
    }

}


