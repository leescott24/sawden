import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { SawdenRoutingConfig } from '../Config/Sawden-Routing-Config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ProfileMaster, ProfileGroup } from './Models/Profile.model';
import { Observable } from 'rxjs';



@Injectable()
export class ProfileDataService
{ 
    profileMaster: ProfileMaster;
    options : any = { headers: new HttpHeaders({'Content-Type': 'application/json'})};

    public get urlStart() : string 
    {
        return this.config.urlStart + "ProfileRouting/";
    }

    constructor(private http: HttpClient,
        private toastr: ToastrService,
        private config: SawdenRoutingConfig){}


    getProfiles() : Observable<ProfileMaster>
    {
        console.log(this.urlStart);
        return this.http.get<ProfileMaster>(this.urlStart + 'GetProfileMaster');              
    }

    refreshProfiles(response: ProfileMaster)
    {
        this.toastr.success("Profiles have been successfully loaded from Sawden DB");
        this.profileMaster = response;                                
    }

    getProfileGroupById(id: number) : ProfileGroup
    {
        return this.profileMaster.profileGroups.filter(f => f.id == id)[0];
    }

}