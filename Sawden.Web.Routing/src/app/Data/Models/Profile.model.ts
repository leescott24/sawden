export class ProfileMaster
{
    profiles : Profile[];
    profileTypes : ProfileType[];
    profileGroups : ProfileGroup[];
}

export class Profile
{
    id : number;
    profilePcode : string;
    description : string;
    height : number;
    width : number;
    profileType : number;
    transomType? : number;
}

export class ProfileType
{
    id: number;
    type: string;
}

export class ProfileGroup
{
    id: number;
    group: string;
    groupDescription: string;
    profiles : Profile[];
}