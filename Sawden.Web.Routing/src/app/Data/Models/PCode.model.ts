export class PCodeMaster
{
    pCodes: PCode[];
    pCodeTypes: PCodeType[];
    pCodeGroups: PCodeGroup[];
}

export class PCode
{
    id: number;
    pcode: string;
    description : string;
    pcodeTypeId : number;
}

export class PCodeType
{
    id: number;
    type: string;
}

export class PCodeGroup
{
    id: number;
    group: string;
    groupDescription: string;
    pcodes : PCode[];
}