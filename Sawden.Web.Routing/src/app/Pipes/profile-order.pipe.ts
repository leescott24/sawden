import { Pipe, PipeTransform } from '@angular/core';
import { Profile } from '../Data/Models/Profile.model';

@Pipe({
    name: 'profileOrder'
})
export class ProfileOrder implements PipeTransform {
    transform(items: Profile[], args: string): any {
        
        if (!items)
            return items;

        items.sort((a : Profile, b : Profile) =>
        {
            if (a.profileType < b.profileType)
            {
                return -1;
            } 
            else if (a.profileType > b.profileType)
            {
                return 1;
            }
            else
            {
                if (a.profilePcode < b.profilePcode)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
        });

        return items;
    }
}