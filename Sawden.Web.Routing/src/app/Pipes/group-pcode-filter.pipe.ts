import { Pipe, PipeTransform } from '@angular/core';
import { PCode } from '../Data/Models/PCode.model';

@Pipe({
    name: 'groupPcodesFilter'
})
export class GroupPcodeFilter implements PipeTransform {
    transform(items: PCode[], filter: number): any {
        if (!items || !filter) {
            return items;
        }

        return items.filter(item => item.pcodeTypeId == filter);
    }
}