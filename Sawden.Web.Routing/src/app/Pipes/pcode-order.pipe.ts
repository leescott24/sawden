import { Pipe, PipeTransform } from '@angular/core';
import { PCode } from '../Data/Models/PCode.model';

@Pipe({
    name: 'pcodeOrder'
})
export class PcodeOrder implements PipeTransform {
    transform(items: PCode[], args: string): any {
        
        if (!items)
            return items;

        items.sort((a : PCode, b : PCode) =>
        {
            if (a.pcodeTypeId < b.pcodeTypeId)
            {
                return -1;
            } 
            else if (a.pcodeTypeId > b.pcodeTypeId)
            {
                return 1;
            }
            else
            {
                if (a.pcode < b.pcode)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
        });

        return items;
    }
}