import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileInfoComponent } from './ProfileAndGroups/profile-info.component';
import { ProfileGroupAddEditComponent } from './ProfileAndGroups/profile-group-add-edit.component';

import { PCodeInfoComponent } from './PCodeAndGroups/pcode-info.component';
import { PCodeAddEditComponent } from './PCodeAndGroups/pcode-add-edit.component';
import { PCodeGroupAddEditComponent } from './PCodeAndGroups/pcode-group-add-edit.component';

import { DrainageInfoComponent } from './Drainage/drainage-info.component';

import { RouteringInfoComponent } from './Routering/routering-info.component';

import { TestingInfoComponent } from './Testing/testing-info.component';



const routes: Routes = [
  { path: 'profile', component: ProfileInfoComponent},
  { path: 'profile/group/:id', component: ProfileGroupAddEditComponent},
  { path: 'pcode', component: PCodeInfoComponent},
  { path: 'pcode/:id', component: PCodeAddEditComponent},
  { path: 'pcode/group/:id', component: PCodeGroupAddEditComponent},  
  { path: 'drainage', component: DrainageInfoComponent},
  { path: 'routering', component: RouteringInfoComponent},
  { path: 'testing', component: TestingInfoComponent},
  { path: '', redirectTo: '/profile', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
