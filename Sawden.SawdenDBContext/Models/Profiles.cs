﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.SawdenDBContext.Models
{
    public partial class Profiles
    {
        public int Id { get; set; }
        public string ProfilePcode { get; set; }
        public string Description { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int ProfileType { get; set; }
        public int? TransomType { get; set; }
        public bool CutOnSaw { get; set; }
    }
}
