﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.SawdenDBContext.Models
{
    public partial class OrderLog
    {
        public int OrderNumber { get; set; }
        public int FrameCount { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
