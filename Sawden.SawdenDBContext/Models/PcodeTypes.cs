﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.SawdenDBContext.Models
{
    public partial class PcodeTypes
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
