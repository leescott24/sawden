﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.SawdenDBContext.Models
{
    public partial class AllowedSystemTypes
    {
        public int SystemTypeId { get; set; }
    }
}
