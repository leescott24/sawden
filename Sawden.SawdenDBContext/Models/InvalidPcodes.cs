﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.SawdenDBContext.Models
{
    public partial class InvalidPcodes
    {
        public int Id { get; set; }
        public string Pcode { get; set; }
        public bool IsError { get; set; }
        public bool IsIgnore { get; set; }
    }
}
