﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.SawdenDBContext.Models
{
    public partial class PcodeGroupLookup
    {
        public int PcodeId { get; set; }
        public int PcodeGroupId { get; set; }
    }
}
