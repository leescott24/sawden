﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.SawdenDBContext.Models
{
    public partial class ProfileTypes
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
