﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Sawden.Persistance.SawdenDBContext.Models
{
    public partial class SawdenContext : DbContext
    {
        public SawdenContext()
        {
        }

        public SawdenContext(DbContextOptions<SawdenContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AllowedMasterProductTypes> AllowedMasterProductTypes { get; set; }
        public virtual DbSet<AllowedSystemTypes> AllowedSystemTypes { get; set; }
        public virtual DbSet<DefaultOffsets> DefaultOffsets { get; set; }
        public virtual DbSet<InvalidPcodes> InvalidPcodes { get; set; }
        public virtual DbSet<OrderLog> OrderLog { get; set; }
        public virtual DbSet<PcodeGroupLookup> PcodeGroupLookup { get; set; }
        public virtual DbSet<PcodeGroups> PcodeGroups { get; set; }
        public virtual DbSet<Pcodes> Pcodes { get; set; }
        public virtual DbSet<PcodeTypes> PcodeTypes { get; set; }
        public virtual DbSet<ProfileGroupLookup> ProfileGroupLookup { get; set; }
        public virtual DbSet<ProfileGroups> ProfileGroups { get; set; }
        public virtual DbSet<Profiles> Profiles { get; set; }
        public virtual DbSet<ProfileTypes> ProfileTypes { get; set; }
        public virtual DbSet<SashTypePartPosition> SashTypePartPosition { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=Sawden;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AllowedMasterProductTypes>(entity =>
            {
                entity.HasKey(e => e.MasterProductId);

                entity.Property(e => e.MasterProductId).ValueGeneratedNever();
            });

            modelBuilder.Entity<AllowedSystemTypes>(entity =>
            {
                entity.HasKey(e => e.SystemTypeId);

                entity.Property(e => e.SystemTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<DefaultOffsets>(entity =>
            {
                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<InvalidPcodes>(entity =>
            {
                entity.Property(e => e.Pcode)
                    .IsRequired()
                    .HasColumnName("PCode")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<OrderLog>(entity =>
            {
                entity.HasKey(e => new { e.OrderNumber, e.Timestamp });

                entity.Property(e => e.Timestamp).HasColumnType("datetime");
            });

            modelBuilder.Entity<PcodeGroupLookup>(entity =>
            {
                entity.HasKey(e => new { e.PcodeId, e.PcodeGroupId });

                entity.ToTable("PCodeGroupLookup", "routing");

                entity.Property(e => e.PcodeId).HasColumnName("PCodeId");

                entity.Property(e => e.PcodeGroupId).HasColumnName("PCodeGroupId");
            });

            modelBuilder.Entity<PcodeGroups>(entity =>
            {
                entity.ToTable("PCodeGroups", "routing");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Group)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.GroupDescription)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Pcodes>(entity =>
            {
                entity.ToTable("PCodes", "routing");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Pcode)
                    .IsRequired()
                    .HasColumnName("PCode")
                    .HasMaxLength(50);

                entity.Property(e => e.PcodeTypeId).HasColumnName("PCodeTypeId");
            });

            modelBuilder.Entity<PcodeTypes>(entity =>
            {
                entity.ToTable("PCodeTypes", "routing");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ProfileGroupLookup>(entity =>
            {
                entity.HasKey(e => new { e.ProfileId, e.ProfileGroupId });

                entity.ToTable("ProfileGroupLookup", "routing");
            });

            modelBuilder.Entity<ProfileGroups>(entity =>
            {
                entity.ToTable("ProfileGroups", "routing");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Group)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.GroupDescription)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Profiles>(entity =>
            {
                entity.ToTable("Profiles", "routing");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ProfilePcode)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ProfileTypes>(entity =>
            {
                entity.ToTable("ProfileTypes", "routing");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SashTypePartPosition>(entity =>
            {
                entity.HasKey(e => e.SashTypeId);

                entity.Property(e => e.SashTypeId).ValueGeneratedNever();

                entity.Property(e => e.PartPosition).HasMaxLength(50);
            });
        }
    }
}
