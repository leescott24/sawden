﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.SawdenDBContext.Models
{
    public partial class Pcodes
    {
        public int Id { get; set; }
        public string Pcode { get; set; }
        public string Description { get; set; }
        public int? PcodeTypeId { get; set; }
    }
}
