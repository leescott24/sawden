﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.SawdenDBContext.Models
{
    public partial class ProfileGroupLookup
    {
        public int ProfileId { get; set; }
        public int ProfileGroupId { get; set; }
    }
}
