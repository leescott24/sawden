﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.SawdenDBContext.Models
{
    public partial class DefaultOffsets
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public double Value { get; set; }
    }
}
