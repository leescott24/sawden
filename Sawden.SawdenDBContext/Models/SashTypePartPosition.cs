﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.SawdenDBContext.Models
{
    public partial class SashTypePartPosition
    {
        public int SashTypeId { get; set; }
        public string PartPosition { get; set; }
    }
}
