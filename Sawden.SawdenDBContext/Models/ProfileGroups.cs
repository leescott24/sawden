﻿using System;
using System.Collections.Generic;

namespace Sawden.Persistance.SawdenDBContext.Models
{
    public partial class ProfileGroups
    {
        public int Id { get; set; }
        public string Group { get; set; }
        public string GroupDescription { get; set; }
    }
}
