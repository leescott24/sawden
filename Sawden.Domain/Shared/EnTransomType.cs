﻿namespace Sawden.Domain.Shared
{
    public enum EnTransomType
    {
         NonTransom = 0,
         Transom1 = 1,
         Transom2 = 2,
         Transom3 = 3, 
         Dummy = 4
    }
}
