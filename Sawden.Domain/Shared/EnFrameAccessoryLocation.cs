﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Shared
{
    public enum EnFrameAccessoryLocation
    {
        Top = 0,
        Bottom = 1,
        Left = 2,
        Right = 3
    }
}
