﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Shared
{
    public interface IColour
    {
        int? ColourIdOutside { get; set; }

        int? ColourIdInside { get; set; }
    }
}
