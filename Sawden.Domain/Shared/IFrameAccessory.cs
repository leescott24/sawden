﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Shared
{
    public interface IFrameAccessory
    {
        int? FrameAccessoryId { get; set; }

        EnFrameAccessoryLocation? Location { get; set; }
    }
}
