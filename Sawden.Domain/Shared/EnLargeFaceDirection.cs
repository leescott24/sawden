﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Shared
{
    public enum EnLargeFaceDirection
    {
        None = 0,
        Outside = 1,
        Inside = 2
    }
}
