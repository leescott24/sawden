﻿namespace Sawden.Domain.Shared
{
    public interface IFrame
    {
        int? FrameNumber { get; set; }

        double? TotalWidth { get; set; }

        double? TotalHeight { get; set; }

        double? FrameWidth { get; set; }

        double? FrameHeight { get; set; }

        int? MasterProductTypeId { get; set; }

        int? SystemTypeId { get; set; }
        
        int? DrainageTypeId { get; set; }

        bool? IsCrucifix { get; set; }
    }
}
