﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Shared
{
    public enum EnGlassType
    {
        Unglazed = 0,
        Glazed = 1,
        Panel = 2
    }
}
