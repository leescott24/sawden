﻿using Sawden.Domain.Input.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Shared
{
    public interface ISash
    {
        int? SashNumber { get; set; }

        int? SashTypeId { get; set; }

        bool? OpenIn { get; set; }

        bool? IsCrucifix { get; set; }
    }
}
