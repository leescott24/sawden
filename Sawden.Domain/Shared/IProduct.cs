﻿namespace Sawden.Domain.Shared
{
    public interface IProduct
    {
        string Pcode { get; set; }

        int? SelectionLinkId { get; set; }

        int? ProductCategoryId { get; set; }

        int? ProductSubCategoryId { get; set; }
    }
}
