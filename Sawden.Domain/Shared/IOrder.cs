﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sawden.Domain.Shared
{
    public interface IOrder
    {
        int? OrderNumber { get; set; }
    }
}
