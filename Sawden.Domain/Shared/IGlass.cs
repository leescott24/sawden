﻿namespace Sawden.Domain.Shared
{
    public interface IGlass
    {
        bool? IsInternallyGlazed { get; set; }

        EnGlassType? GlassType { get; set; }
    }
}
