﻿namespace Sawden.Domain.Shared
{
    public interface ISplit
    {
        int? Id { get; set; }

        double? Value { get; set; }
    }
}
