﻿using System.Collections.Generic;

namespace Sawden.Domain.Shared
{
    public interface IPosition
    {
        int? RowId { get; set; }

        int? RowSpan { get; set; }

        int? ColId { get; set; }

        int? ColSpan { get; set; }        
    }
}
