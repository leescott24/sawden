﻿namespace Sawden.Domain.Shared
{
    public interface IProfile
    {
        string Pcode { get; set; }

        EnTransomType? TransomType { get; set; }

        EnLargeFaceDirection LargeFaceDirection { get; set; }     
    }
}
