﻿using Newtonsoft.Json;
using Sawden.Domain.CustomValidation;
using Sawden.Domain.Shared;
using System;
using System.ComponentModel.DataAnnotations;

namespace Sawden.Domain.Input.Models
{
    public class ClsInputProfile : ClsInputPosition, IProfile
    {
        [CustomProfilePCodeValidation]
        public string Pcode { get; set; }

        [Required]
        public EnTransomType? TransomType { get; set; }

        [JsonIgnore]
        public EnLargeFaceDirection LargeFaceDirection { get; set; }
    }
}
