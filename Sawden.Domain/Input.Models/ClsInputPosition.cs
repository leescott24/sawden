﻿using Newtonsoft.Json;
using Sawden.Domain.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sawden.Domain.Input.Models
{
    public class ClsInputPosition : IPosition
    {
        [Required]
        public int? RowId { get; set; }

        [Required]
        public int? RowSpan { get; set; }

        [Required]
        public int? ColId { get; set; }

        [Required]
        public int? ColSpan { get; set; }              
    }
}
