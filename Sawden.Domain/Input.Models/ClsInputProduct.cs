﻿using Sawden.Domain.CustomValidation;
using Sawden.Domain.Shared;
using System.ComponentModel.DataAnnotations;

namespace Sawden.Domain.Input.Models
{
    public class ClsInputProduct : IProduct
    {
        [CustomStringValidate]
        public string Pcode { get; set; }

        [Required]
        public int? SelectionLinkId { get; set; }

        [Required]
        public int? ProductCategoryId { get; set; }

        [Required]
        public int? ProductSubCategoryId { get; set; }
    }
}
