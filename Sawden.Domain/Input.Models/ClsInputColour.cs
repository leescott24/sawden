﻿using Sawden.Domain.Shared;
using System.ComponentModel.DataAnnotations;

namespace Sawden.Domain.Input.Models
{
    public class ClsInputColour : IColour
    {
        [Required]
        public int? ColourIdOutside { get; set; }

        [Required]
        public int? ColourIdInside { get; set; }
    }
}
