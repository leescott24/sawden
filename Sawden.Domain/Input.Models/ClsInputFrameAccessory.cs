﻿using Sawden.Domain.Shared;
using System.ComponentModel.DataAnnotations;

namespace Sawden.Domain.Input.Models
{
    public class ClsInputFrameAccessory : IFrameAccessory
    {
        [Required]
        public int? FrameAccessoryId { get; set; }

        [Required]
        public EnFrameAccessoryLocation? Location { get; set; }
    }
}
