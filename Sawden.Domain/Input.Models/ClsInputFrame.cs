﻿using Sawden.Domain.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Sawden.Domain.Input.Models
{
    public class ClsInputFrame : IFrame
    {
        [Required]
        [Range(1, 99, ErrorMessage = "Frame Number must be between 1 and 99")]
        public int? FrameNumber { get; set; }

        [Required]
        [Range(175, 6000, ErrorMessage = "Total Width must have dimensions between 175 and 6000")]
        public double? TotalWidth { get; set; }

        [Required]
        [Range(175, 6000, ErrorMessage = "Total Height must have dimensions between 175 and 6000")]
        public double? TotalHeight { get; set; }

        [Required]
        [Range(175, 6000, ErrorMessage = "Frame Width must have dimensions between 175 and 6000")]
        public double? FrameWidth { get; set; }

        [Required]
        [Range(175, 6000, ErrorMessage = "Frame Height must have dimensions between 175 and 6000")]
        public double? FrameHeight { get; set; }

        [Required]
        public int? MasterProductTypeId { get; set; }

        [Required]
        public int? SystemTypeId { get; set; }

        [Required]
        public int? DrainageTypeId { get; set; }

        [Required]
        public bool? IsCrucifix { get; set; }

        [Required]
        public ClsInputColour Colour { get; set; }

        public ClsInputCill Cill { get; set; }

        [Required]
        public List<ClsInputSplit> LstRow { get; set; }

        [Required]
        public List<ClsInputSplit> LstCol { get; set; }

        [Required]
        public List<ClsInputProfile> LstProfile { get; set; }

        public List<ClsInputSash> LstSash { get; set; }

        public List<ClsInputGlass> LstGlass { get; set; }

        public List<ClsInputProduct> LstExtra { get; set; }

        public List<ClsInputFrameAccessory> LstFrameAccessory { get; set; }
    }
}
