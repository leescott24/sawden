﻿using Sawden.Domain.Shared;
using System.ComponentModel.DataAnnotations;

namespace Sawden.Domain.Input.Models
{
    public class ClsInputCill : ICill
    {
        [Required]
        public int? CillId { get; set; }
    }
}
