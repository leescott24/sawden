﻿using Sawden.Domain.CustomValidation;
using Sawden.Domain.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Sawden.Domain.Input.Models
{
    public class ClsInputSashCeption : ClsInputPosition, ISash
    {
        [Required]
        public int? SashNumber { get; set; }

        [Required]
        [CustomSashCeptionValidate]
        public int? SashTypeId { get; set; }

        [Required]
        public bool? OpenIn { get; set; }

        [Required]
        public bool? IsCrucifix { get; set; }

        [Required]
        public ClsInputColour Colour { get; set; }

        [CustomSashProfileValidate]
        public List<ClsInputProfile> LstProfile { get; set; }

        public List<ClsInputProduct> LstExtra { get; set; }
    }
}
