﻿using Sawden.Domain.Shared;
using System.ComponentModel.DataAnnotations;

namespace Sawden.Domain.Input.Models
{
    public class ClsInputGlass : ClsInputPosition, IGlass
    {
        [Required]
        public bool? IsInternallyGlazed { get; set; }

        [Required]
        public EnGlassType? GlassType { get; set; }
    }
}
