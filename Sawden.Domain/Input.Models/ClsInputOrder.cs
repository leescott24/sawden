﻿using Newtonsoft.Json;
using Sawden.Domain.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sawden.Domain.Input.Models
{
    public class ClsInputOrder : IOrder
    {
        [Required]
        public int? OrderNumber { get; set; }

        [Required]
        public List<ClsInputFrame> LstFrame { get; set; }
    }
}
