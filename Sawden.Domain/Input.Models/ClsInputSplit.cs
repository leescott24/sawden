﻿using Sawden.Domain.Shared;
using System.ComponentModel.DataAnnotations;

namespace Sawden.Domain.Input.Models
{
    public class ClsInputSplit : ISplit
    {
        [Required]
        public int? Id { get; set; }

        [Required]
        public double? Value { get; set; }
    }
}
