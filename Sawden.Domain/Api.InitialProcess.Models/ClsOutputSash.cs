﻿using Sawden.Domain.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Api.InitialProcess.Models
{
    public class ClsOutputSash : IPosition
    {
        public int SashNumber { get; set; }

        public int SashTypeId { get; set; }

        public double Width { get; set; }

        public double Height { get; set; }

        public bool? OpenIn { get; set; }

        public int? RowId { get; set; }

        public int? RowSpan { get; set; }

        public int? ColId { get; set; }

        public int? ColSpan { get; set; }
    }
}
