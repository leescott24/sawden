﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Api.InitialProcess.Models
{
    public class ClsOutputBar
    {
        public string Pcode { get; set; }

        public double Length { get; set; }

        public int ColourIdLargeFace { get; set; }

        public int ColourIdSmallFace { get; set; }
    }
}
