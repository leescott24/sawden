﻿using Sawden.Domain.Main.Models.Main;
using Sawden.Domain.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Api.InitialProcess.Models
{
    public class ClsOutputGlass : ClsPosition, IGlass
    {
        public double Width { get; set; }

        public double Height { get; set; }

        public bool? IsInternallyGlazed { get; set; }

        public EnGlassType? GlassType { get; set; }
    }
}
