﻿using System.Collections.Generic;

namespace Sawden.Domain.Api.InitialProcess.Models
{
    public class ClsOutputOrder
    {
        public int OrderNumber{ get; set; }

        public List<ClsOutputFrame> LstFrame { get; set; }

        public List<ClsOutputBar> LstBars { get; set; }
    }
}
