﻿using Sawden.Domain.Main.Models.Abstract;
using Sawden.Domain.Main.Models.Main;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Api.InitialProcess.Models
{
    public class ClsOutputFrame
    {
        public int FrameNumber { get; set; }

        public List<ClsOutputProfile> LstProfile { get; set; }

        public List<ClsOutputSash> LstSash { get; set; }

        public List<ClsOutputGlass> LstGlass { get; set; }

        public List<ClsOutputProduct> LstExtras { get; set; }
    }
}
