﻿using Newtonsoft.Json;
using Sawden.Domain.Main.Models.Main;
using Sawden.Domain.Shared;
using System.Collections.Generic;

namespace Sawden.Domain.Main.Models.Abstract
{
    public abstract class ACFrame : IFrame
    {
        public int? FrameNumber { get; set; }

        public double? TotalWidth { get; set; }

        public double? TotalHeight { get; set; }

        public double? FrameWidth { get; set; }

        public double? FrameHeight { get; set; }

        public int? MasterProductTypeId { get; set; }

        public int? SystemTypeId { get; set; }

        public int? DrainageTypeId { get; set; }

        public bool? IsCrucifix { get; set; }

        public ClsColour Colour { get; set; }

        public ClsCill Cill { get; set; }

        public List<ClsSplit> LstRow { get; set; }

        public List<ClsSplit> LstCol { get; set; }

        public List<ACProfile> LstProfile { get; set; }

        public List<ACSash> LstSash { get; set; }

        public List<ClsGlass> LstGlass { get; set; }

        public List<ACProduct> LstExtra { get; set; }

        public List<ACFrameAccessory> LstFrameAccessory { get; set; }        
    }
}
