﻿using Sawden.Domain.Shared;

namespace Sawden.Domain.Main.Models.Abstract
{
    public abstract class ACProduct : IProduct
    {
        public string Pcode { get; set; }

        public int? SelectionLinkId { get; set; }

        public int? ProductCategoryId { get; set; }

        public int? ProductSubCategoryId { get; set; }
    }
}
