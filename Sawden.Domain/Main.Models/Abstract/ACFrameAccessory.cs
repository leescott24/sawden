﻿using Sawden.Domain.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Main.Models.Abstract
{
    public abstract class ACFrameAccessory : IFrameAccessory
    {
        public int? FrameAccessoryId { get; set; }

        public EnFrameAccessoryLocation? Location { get; set; }
    }
}
