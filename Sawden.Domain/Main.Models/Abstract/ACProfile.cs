﻿using Sawden.Domain.Main.Models.Main;
using Sawden.Domain.Shared;
using System;
using System.Collections.Generic;

namespace Sawden.Domain.Main.Models.Abstract
{
    public abstract class ACProfile : ClsPosition, IProfile
    {
        public double Length { get; set; }

        public double OffsetTL { get; set; }

        public double OffsetBR { get; set; }

        public int ProfileHeight { get; set; }

        public int ProfileWidth { get; set; }

        public int RebateInternal { get; set; }

        public int RebateExternal { get; set; }

        public string Pcode { get; set; }

        public string PartPosition { get; set; }

        public int ProductCategoryId { get; set; }

        public int SashOffSet { get; set; }

        public bool IsOddLeg { get; set; }

        public int ColourIdLargeFace { get; set; }

        public int ColourIdSmallFace { get; set; }

        public ClsEndPrep TLEndPrep { get; set; }

        public ClsEndPrep BREndPrep { get; set; }

        public EnTransomType? TransomType { get; set; }

        public EnLargeFaceDirection LargeFaceDirection { get; set; }

        public List<ClsVNotch> LstVNotch { get; set; }
    }
}
