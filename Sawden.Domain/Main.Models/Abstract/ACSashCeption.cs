﻿using Sawden.Domain.Main.Models.Main;
using Sawden.Domain.Shared;
using System.Collections.Generic;

namespace Sawden.Domain.Main.Models.Abstract
{
    public class ACSashCeption : ClsPosition, ISash
    {
        public int? SashNumber { get; set; }

        public int? SashTypeId { get; set; }

        public double OffsetTop { get; set; }

        public double OffsetBot { get; set; }

        public double OffsetLeft { get; set; }

        public double OffsetRight { get; set; }

        public double Width { get; set; }

        public double Height { get; set; }

        public bool? OpenIn { get; set; }

        public bool? IsCrucifix { get; set; }        

        public ClsColour Colour { get; set; }

        public List<ACProfile> LstProfile { get; set; }

        public List<ACProduct> LstExtra { get; set; }
    }
}
