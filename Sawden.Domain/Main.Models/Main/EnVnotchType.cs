﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Main.Models.Main
{
    public enum EnVnotchType
    {
        Small = 0,
        Medium = 1,
        Large = 2
    }
}
