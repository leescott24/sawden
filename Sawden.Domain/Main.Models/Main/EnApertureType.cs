﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Main.Models.Main
{
    public enum EnApertureType
    {
        None = 0,
        Internal = 1,
        External = 2
    }
}
