﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Main.Models.Main
{
    public enum EnEndPrep
    {
        Acute = 0,
        Flat = 1,
        Diamond = 2,
        Chamfer = 3,
        DoubleChamfer = 4
    }
}
