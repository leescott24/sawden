﻿using Sawden.Domain.Main.Models.Abstract;
using Sawden.Domain.Shared;

namespace Sawden.Domain.Main.Models.Main
{
    public class ClsCill : ICill
    {
        public int? CillId { get; set; }
    }
}
