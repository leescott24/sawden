﻿using Sawden.Domain.Main.Models.Abstract;

namespace Sawden.Domain.Main.Models.Main
{
    public class ClsAperture : ClsPosition
    {
        public ClsAperture()
        {
            ApertureType = EnApertureType.None;
        }

        public EnApertureType ApertureType { get; set; }
    }
}
