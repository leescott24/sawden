﻿using Sawden.Domain.Main.Models.Abstract;
using Sawden.Domain.Shared;

namespace Sawden.Domain.Main.Models.Main
{
    public class ClsPosition : IPosition
    {
        public int? RowId { get; set; }

        public int? RowSpan { get; set; }

        public int? ColId { get; set; }

        public int? ColSpan { get; set; }
    }
}
