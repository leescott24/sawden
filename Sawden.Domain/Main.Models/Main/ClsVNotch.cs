﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Main.Models.Main
{
    public class ClsVNotch
    {
        public bool IsTL { get; set; } //top or left of profile

        public int RowId { get; set; }

        public int ColId { get; set; }

        public double Position { get; set; }

        public EnVnotchType Type { get; set; }
    }
}
