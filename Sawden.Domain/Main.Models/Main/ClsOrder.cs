﻿using Sawden.Domain.Main.Models.Abstract;
using System.Collections.Generic;

namespace Sawden.Domain.Main.Models.Main
{
    public class ClsOrder
    {
        public int? OrderNumber { get; set; }

        public List<ACFrame> LstFrame { get; set; }
    }
}
