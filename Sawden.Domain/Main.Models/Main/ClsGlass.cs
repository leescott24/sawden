﻿using Sawden.Domain.Main.Models.Abstract;
using Sawden.Domain.Shared;

namespace Sawden.Domain.Main.Models.Main
{
    public class ClsGlass : ClsPosition, IGlass
    {
        public double Width { get; set; }

        public double Height { get; set; }

        public bool? IsInternallyGlazed { get; set; }

        public EnGlassType? GlassType { get; set; }
    }
}
