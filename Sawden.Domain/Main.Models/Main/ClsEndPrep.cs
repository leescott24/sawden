﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Main.Models.Main
{
    public class ClsEndPrep
    {
        public EnEndPrep EndPrep { get; set; }

        public double Chamfer { get; set; }

        public double MulTraChamferTL { get; set; }

        public double MulTraChamferBR { get; set; }
    }
}
