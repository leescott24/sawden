﻿using Sawden.Domain.Main.Models.Abstract;
using Sawden.Domain.Shared;

namespace Sawden.Domain.Main.Models.Main
{
    public class ClsColour : IColour
    {
        public int? ColourIdOutside { get; set; }

        public int? ColourIdInside { get; set; }
    }
}
