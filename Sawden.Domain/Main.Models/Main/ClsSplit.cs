﻿using Sawden.Domain.Main.Models.Abstract;
using Sawden.Domain.Shared;

namespace Sawden.Domain.Main.Models.Main
{
    public class ClsSplit : ISplit
    {
        public int? Id { get; set; }

        public double? Value { get; set; }
    }
}
