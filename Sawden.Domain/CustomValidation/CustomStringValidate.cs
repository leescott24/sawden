﻿using System.ComponentModel.DataAnnotations;

namespace Sawden.Domain.CustomValidation
{
    internal class CustomStringValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var str = value as string;

            if (str == "" || str.ToLower() == "string" || str == null)
            {
                return new ValidationResult("String must not be blank or 'string'");
            }

            return ValidationResult.Success;
        }
    }
}
