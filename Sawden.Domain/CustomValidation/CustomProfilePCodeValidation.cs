﻿using Sawden.Domain.Input.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Sawden.Domain.CustomValidation
{
    public class CustomProfilePCodeValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var profile = validationContext.ObjectInstance as ClsInputProfile;

            if (profile.TransomType == Shared.EnTransomType.NonTransom)
            {
                if (profile.Pcode == null || profile.Pcode == "" || profile.Pcode.ToLower() == "string")
                {
                    return new ValidationResult("Profile PCode of TransomType.NonTransom must not be blank or 'string'");
                }

                return ValidationResult.Success;
            }
            else
            {
                if (profile.Pcode == null || profile.Pcode == "" || profile.Pcode.ToLower() == "string")
                {
                    return ValidationResult.Success;
                }

                return new ValidationResult("Profile PCode of TransomType != Nontransom must be blank or 'string'");
            }
        }
    }
}
