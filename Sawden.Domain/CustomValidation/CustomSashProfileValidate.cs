﻿using Sawden.Domain.Input.Models;
using System.ComponentModel.DataAnnotations;

namespace Sawden.Domain.CustomValidation
{
    internal class CustomSashProfileValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var sash = validationContext.ObjectInstance as ClsInputSash;
            var sashCeption = validationContext.ObjectInstance as ClsInputSashCeption;

            var sashTypeId = sash == null ? sashCeption.SashTypeId : sash.SashTypeId;
            var lstProfile = sash == null ? sashCeption.LstProfile : sash.LstProfile;

            //profile checks
            if (sashTypeId == 19 ||
                sashTypeId == 20 ||
                sashTypeId == 22 ||
                sashTypeId == 23
            )
            {
                if (lstProfile != null)
                {
                    return new ValidationResult("Composite door Sash can not contain List of Profile aswell");
                }
            }
            else
            {
                if (lstProfile == null)
                {
                    return new ValidationResult("All Sashes (except Composite) require List of Profile");
                }
            }

            return ValidationResult.Success;
        }
    }
}
