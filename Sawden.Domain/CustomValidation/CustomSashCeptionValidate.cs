﻿using Sawden.Domain.Input.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Sawden.Domain.CustomValidation
{
    public class CustomSashCeptionValidate : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var sashCeption = validationContext.ObjectInstance as ClsInputSashCeption;

            var lstAllowedSashTypes = new List<int>() { 1, 2, 3, 15, 16, 18, 21 };

            if (!lstAllowedSashTypes.Contains(Convert.ToInt32(sashCeption.SashTypeId)))
            {
                return new ValidationResult("Sash ception not of an Allowed SashType");
            }

            return ValidationResult.Success;
        }
    }
}
