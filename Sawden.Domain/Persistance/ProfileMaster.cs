﻿using Sawden.Persistance.SawdenDBContext.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Persistance
{
    public class ProfileMaster
    {
        public List<Profiles> Profiles { get; set; }

        public List<ProfileTypes> ProfileTypes { get; set; }

        public List<ProfileGroupsWithProfiles> ProfileGroups { get; set; }
    }
}
