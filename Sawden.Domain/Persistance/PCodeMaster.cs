﻿using Sawden.Persistance.SawdenDBContext.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Persistance
{
    public class PCodeMaster
    {
        public List<Pcodes> PCodes { get; set; }

        public List<PcodeTypes> PCodeTypes { get; set; }

        public List<PcodeGroupsWithPcodes> PCodeGroups { get; set; }
    }
}
