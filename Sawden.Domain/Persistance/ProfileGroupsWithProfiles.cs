﻿using Sawden.Persistance.SawdenDBContext.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Persistance
{
    public class ProfileGroupsWithProfiles : ProfileGroups
    {
        public List<Profiles> Profiles { get; set; }
    }
}
