﻿using Sawden.Persistance.SawdenDBContext.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Domain.Persistance
{
    public class PcodeGroupsWithPcodes : PcodeGroups
    {
        public List<Pcodes> Pcodes { get; set; }
    }
}
