﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Shared.Interfaces
{
    public interface IResponseData
    {       
        EnOutcome Outcome { get; set; }

        bool Continue { get; }

        List<ResponseNote> Notes { get; set; }

        dynamic Data { get; set; }
    }
}
