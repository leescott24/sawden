﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Shared.Interfaces
{
    public interface ISawdenSettings
    {
        List<int> AllowedMasterProductTypes { get; set; }

        List<int> AllowedSystemTypes { get; set; }

        List<ClsInvalidPcode> InvalidPcodes { get; set; }

        double GlassOffset { get; set; }

        double SashOverlap { get; set; }

        double OddLegOverLap { get; set; }

        double DummyMullionProfileOffset { get; set; }

        double DummyMullionAliOffset { get; set; }

        double Comp44mmOffset { get; set; }

        double Comp70mmOverLap { get; set; }
    }
}
