﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Shared
{
    public enum EnResponseNote
    {
        Info = 0,
        SawdenWarning = 1,
        SawdenError = 2,
        CamdenWarning = 3,
        CamdenError = 4
    }
}
