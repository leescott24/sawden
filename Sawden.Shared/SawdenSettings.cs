﻿using Sawden.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Shared
{
    public class SawdenSettings : ISawdenSettings
    {
        public List<int> AllowedMasterProductTypes { get; set; }

        public List<int> AllowedSystemTypes { get; set; }

        public List<ClsInvalidPcode> InvalidPcodes { get; set; }

        public double GlassOffset { get; set; }

        public double SashOverlap { get; set; }

        public double OddLegOverLap { get; set; }

        public double DummyMullionProfileOffset { get; set; }

        public double DummyMullionAliOffset { get; set; }

        public double Comp44mmOffset { get; set; }

        public double Comp70mmOverLap { get; set; }
    }
}
