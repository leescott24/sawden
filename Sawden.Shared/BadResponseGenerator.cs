﻿using Sawden.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;

namespace Sawden.Shared
{
    public static class BadResponseGenerator
    {
        public static void Exception(IResponseData response, Exception ex = null, EnOutcome outcome = EnOutcome.Failure, ResponseNote note = null, List<ResponseNote> additionalNotes = null)
        {
            var callingMethod = new StackFrame(1).GetMethod();
            var exLocation = callingMethod.DeclaringType.FullName + "." + callingMethod.Name;

            response.Notes.Add(new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Failed In: {exLocation}"
                }
            );

            response.Outcome = outcome;

            if (ex != null)
            {
                response.Notes.Add(new ResponseNote()
                {
                    NoteType = EnResponseNote.SawdenError,
                    NoteMessage = $"Exception Message: {ex.Message}"
                });
            }

            if (note != null)
            {
                response.Notes.Add(note);
            }

            if (additionalNotes != null)
            {
                additionalNotes.ForEach(f =>
                {
                    response.Notes.Add(f);
                });
            }

            response.Data = null;
        }

        public static void Add(IResponseData response, EnOutcome outcome = EnOutcome.Failure, ResponseNote note = null,  List<ResponseNote> additionalNotes = null)
        {
            response.Outcome = outcome;

            if (note != null)
            {
                response.Notes.Add(note);
            }

            if (additionalNotes != null)
            {
                additionalNotes.ForEach(f =>
                {
                    response.Notes.Add(f);
                });
            }

            response.Data = null;
        }
    }
}
