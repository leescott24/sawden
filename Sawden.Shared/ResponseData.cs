﻿using Sawden.Shared.Interfaces;
using System.Collections.Generic;

namespace Sawden.Shared
{
    public class ResponseData : IResponseData
    {
        public ResponseData()
        {
            _notes = new List<ResponseNote>();
            Outcome = EnOutcome.Success;
        }

        public EnOutcome Outcome { get; set; }

        public bool Continue
        {
            get
            {
                return (Outcome == EnOutcome.Success
                    || Outcome == EnOutcome.CompletedWithWarnings
                    || Outcome == EnOutcome.CompletedWithWarnings);
            }
        }

        private List<ResponseNote> _notes;         

        public List<ResponseNote> Notes
        {
            get
            {
                if (_notes == null)
                {
                    _notes = new List<ResponseNote>();
                }
                return _notes;
            }
            set
            {
                _notes = value;
            }
        }

        public dynamic Data { get; set; }
    } 
}
