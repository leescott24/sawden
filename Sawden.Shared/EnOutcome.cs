﻿using System.ComponentModel;

namespace Sawden.Shared
{   
    public enum EnOutcome
    {        
        Success = 1,
        Failure = 2,
        NoneFound = 3,
        InsufficientRights = 4,
        CompletedWithWarnings = 5,
        CompletedWithErrors = 6
    }
}
