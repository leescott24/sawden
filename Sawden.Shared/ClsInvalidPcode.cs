﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Shared
{
    public class ClsInvalidPcode
    {
        public string PCode { get; set; }

        public bool IsError { get; set; }

        public bool IsIgnore { get; set; }
    }
}
