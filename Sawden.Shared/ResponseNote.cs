﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sawden.Shared
{
    public class ResponseNote
    {
        public EnResponseNote NoteType { get; set; }

        public string NoteMessage { get; set; }
    }
}
