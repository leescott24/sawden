﻿using AutoMapper;
using Sawden.Domain.Input.Models;
using Sawden.Domain.Main.Models.Main;

namespace Sawden.Shared
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ClsInputCill, ClsCill>();
            CreateMap<ClsInputColour, ClsColour>();
            CreateMap<ClsInputGlass, ClsGlass>();
            CreateMap<ClsInputPosition, ClsPosition>();
            CreateMap<ClsInputSplit, ClsSplit>();
        }
    }
}
